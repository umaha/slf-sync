<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Feeder Webservice versi 2.2, mengikuti panduan
 */
class Feederws
{
	private $url;
	private $token;

	function __construct($params)
	{
		$this->url = $params['url'];
		$this->token = $params['token'] ?? null;
	}

	/**
	 * @param $data mixed JSON Object dari fungsi json_encode
	 * @return string Hasil dari eksekusi webservice
	 */
	public function runWS($data)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		$headers[] = 'Content-Type: application/json';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_URL, $this->url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$result = curl_exec($ch);
		if (curl_errno($ch)) { 
			print curl_error($ch); 
		 } 
		curl_close($ch);
		return $result;
	}

	private function array_return($result)
	{
		$decoded = json_decode($result, true);

		// 100 : Invalid Token
		if ($decoded['error_code'] == 100) {
			redirect('/');
		}

		return $decoded;
	}

	public function GetToken($username, $password)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetToken',
			'username' => $username,
			'password' => $password
		]));

		return json_decode($result, true);
	}

	public function GetProfilPT()
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetProfilPT',
			'token' => $this->token,
			'limit' => 1
		]));

		return json_decode($result, true);
	}

	public function GetJenjangPendidikan()
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetJenjangPendidikan',
			'token' => $this->token
		]));

		return json_decode($result, true);
	}

	public function GetAllProdi($kode_pt)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetAllProdi',
			'token' => $this->token,
			'filter' => "kode_perguruan_tinggi='{$kode_pt}'",
		]));

		return json_decode($result, true);
	}

	// return example
	// {
	// 	"tgl_create": "13-02-2023",
	// 	"id_kurikulum": "1121d3d7-0f33-4685-8c43-eeef27b16172",
	// 	"nama_kurikulum": "Kurikulum Merdeka Belajar Kampus Merdeka (MBKM)",
	// 	"id_matkul": "7a147763-073d-468f-97b0-269fdfaeb682",
	// 	"kode_mata_kuliah": "311211001",
	// 	"nama_mata_kuliah": "I S B D",
	// 	"id_prodi": "96f4e4f6-adba-41b6-8d92-92369808ae44",
	// 	"nama_program_studi": "S1 Ilmu Hukum",
	// 	"semester": "1",
	// 	"id_semester": "20211",
	// 	"semester_mulai_berlaku": "2021/2022 Ganjil",
	// 	"sks_mata_kuliah": "3.00",
	// 	"sks_tatap_muka": "3.00",
	// 	"sks_praktek": "0.00",
	// 	"sks_praktek_lapangan": "0.00",
	// 	"sks_simulasi": "0.00",
	// 	"apakah_wajib": "0",
	// 	"status_sync": "sudah sync"
	//   }
	public function GetListMatkulKurikulumFilterBy($filter)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetMatkulKurikulum',
			'token' => $this->token,
			'filter' => $filter,
		]));

		return json_decode($result, true);
	}

	public function GetMatkulKurikulum($id_matkul, $id_kurikulum)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetMatkulKurikulum',
			'token' => $this->token,
			'filter' => "id_matkul='{$id_matkul}' AND id_kurikulum='{$id_kurikulum}'",
			'limit' => 5
		]));

		return json_decode($result, true);
	}

	public function GetListMataKuliah($id_matkul)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetListMataKuliah',
			'token' => $this->token,
			'filter' => "id_matkul='{$id_matkul}'",
			'limit' => 5
		]));

		return json_decode($result, true);
	}

	// return example
	// {
	// 	"tgl_create": "13-02-2023",
	// 	"id_kurikulum": "1121d3d7-0f33-4685-8c43-eeef27b16172",
	// 	"nama_kurikulum": "Kurikulum Merdeka Belajar Kampus Merdeka (MBKM)",
	// 	"id_matkul": "7a147763-073d-468f-97b0-269fdfaeb682",
	// 	"kode_mata_kuliah": "311211001",
	// 	"nama_mata_kuliah": "I S B D",
	// 	"id_prodi": "96f4e4f6-adba-41b6-8d92-92369808ae44",
	// 	"nama_program_studi": "S1 Ilmu Hukum",
	// 	"semester": "1",
	// 	"id_semester": "20211",
	// 	"semester_mulai_berlaku": "2021/2022 Ganjil",
	// 	"sks_mata_kuliah": "3.00",
	// 	"sks_tatap_muka": "3.00",
	// 	"sks_praktek": "0.00",
	// 	"sks_praktek_lapangan": "0.00",
	// 	"sks_simulasi": "0.00",
	// 	"apakah_wajib": "0",
	// 	"status_sync": "sudah sync"
	//   }
	public function GetListMataKuliahFilterBy($filter)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetListMataKuliah',
			'token' => $this->token,
			'filter' => $filter,
		]));

		return json_decode($result, true);
	}

	public function GetListAktivitasMahasiswa()
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetListAktivitasMahasiswa',
			'token' => $this->token,
			'limit' => 5
		]));

		return json_decode($result, true);
	}

	public function GetJenisAktivitasMahasiswa()
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetJenisAktivitasMahasiswa',
			'token' => $this->token,
		]));

		return json_decode($result, true);
	}

	public function GetListMahasiswa($nim)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetListMahasiswa',
			'token' => $this->token,
			'filter' => "nim='{$nim}'",
			'limit' => 1
		]));

		return json_decode($result, true);
	}

	public function GetBiodataMahasiswaByNik($nik)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetBiodataMahasiswa',
			'token' => $this->token,
			'filter' => "nik='{$nik}'",
			'limit' => 1
		]));

		return json_decode($result, true);
	}

	public function GetBiodataMahasiswa($nama_mahasiswa, $nama_ibu, $tanggal_lahir)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetBiodataMahasiswa',
			'token' => $this->token,
			'filter' => "nama_mahasiswa='{$nama_mahasiswa}' AND
						LOWER(nama_ibu_kandung)=LOWER('{$nama_ibu}')",
			'limit' => 1
		]));

		return json_decode($result, true);
	}


	public function InsertBiodataMahasiswa($data)
	{
		$result = $this->runWS(json_encode([
			'act' => 'InsertBiodataMahasiswa',
			'token' => $this->token,
			'record' => $data
		]));

		return json_decode($result, true);
	}

	public function UpdateBiodataMahasiswa($key, $data)
	{
		$result = $this->runWS(json_encode([
			'act' => 'UpdateBiodataMahasiswa',
			'token' => $this->token,
			'key' => $key,
			'record' => $data
		]));

		return json_decode($result, true);
	}

	public function DeleteBiodataMahasiswa($id_mahasiswa)
	{
		$result = $this->runWS(json_encode([
			'act' => 'DeleteBiodataMahasiswa',
			'token' => $this->token,
			'key' => [
				'id_mahasiswa' => $id_mahasiswa
			]
		]));

		return json_decode($result, true);
	}

	public function InsertRiwayatPendidikanMahasiswa($data)
	{
		$result = $this->runWS(json_encode([
			'act' => 'InsertRiwayatPendidikanMahasiswa',
			'token' => $this->token,
			'record' => $data
		]));

		return json_decode($result, true);
	}

	public function UpdateRiwayatPendidikanMahasiswa($key, $data)
	{
		$result = $this->runWS(json_encode([
			'act' => 'UpdateRiwayatPendidikanMahasiswa',
			'token' => $this->token,
			'key' => $key,
			'record' => $data
		]));

		return json_decode($result, true);
	}

	public function GetListRiwayatPendidikanMahasiswa($id_reg_pd)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetListRiwayatPendidikanMahasiswa',
			'token' => $this->token,
			'filter' => "id_registrasi_mahasiswa = '{$id_reg_pd}'"
		]));

		return json_decode($result, true);
	}

	public function GetListRiwayatPendidikanMahasiswaByNim($arrayNim)
	{
		$nim = implode("','", $arrayNim);

		
		$result = $this->runWS(json_encode([
			'act' => 'GetListRiwayatPendidikanMahasiswa',
			'token' => $this->token,
			'filter' => "nim in ('{$nim}')"
		]));

		return json_decode($result, true);
	}

	public function GetListRiwayatPendidikanMahasiswaProdi($filter)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetListRiwayatPendidikanMahasiswa',
			'token' => $this->token,
			'filter' => $filter
		]));

		return json_decode($result, true);
	}

	public function DeleteRiwayatPendidikanMahasiswa($key)
	{
		$result = $this->runWS(json_encode([
			'act' => 'DeleteRiwayatPendidikanMahasiswa',
			'token' => $this->token,
			'key' => $key
		]));

		return json_decode($result, true);
	}

	public function InsertMataKuliah($data)
	{
		$result = $this->runWS(json_encode([
			'act' => 'InsertMataKuliah',
			'token' => $this->token,
			'record' => $data
		]));

		return json_decode($result, true);
	}

	public function UpdateMataKuliah($key, $data)
	{
		$result = $this->runWS(json_encode([
			'act' => 'UpdateMataKuliah',
			'token' => $this->token,
			'key' => $key,
			'record' => $data
		]));

		return json_decode($result, true);
	}

	// return example
	// {
	// 	"id_jenj_didik": "30",
	// 	"jml_sem_normal": "0",
	// 	"id_kurikulum": "adf85a4d-8223-4fdc-953b-2aab7b72c343",
	// 	"nama_kurikulum": "Kurikulum 2013 S1 Hukum",
	// 	"id_prodi": "96f4e4f6-adba-41b6-8d92-92369808ae44",
	// 	"nama_program_studi": "S1 Ilmu Hukum",
	// 	"id_semester": "20151",
	// 	"semester_mulai_berlaku": "2015/2016 Ganjil",
	// 	"jumlah_sks_lulus": "0",
	// 	"jumlah_sks_wajib": "0",
	// 	"jumlah_sks_pilihan": "0",
	// 	"jumlah_sks_mata_kuliah_wajib": "70.00",
	// 	"jumlah_sks_mata_kuliah_pilihan": "0",
	// 	"status_sync": "sudah sync"
	//   }
	public function GetListKurikulumFilterBy($filter)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetListKurikulum',
			'token' => $this->token,
			'filter' => $filter,
		]));

		return json_decode($result, true);
	}

	public function InsertKurikulum($data)
	{
		$result = $this->runWS(json_encode([
			'act' => 'InsertKurikulum',
			'token' => $this->token,
			'record' => $data
		]));

		return json_decode($result, true);
	}

	public function UpdateKurikulum($key, $data)
	{
		$result = $this->runWS(json_encode([
			'act' => 'UpdateKurikulum',
			'token' => $this->token,
			'key' => $key,
			'record' => $data
		]));

		return json_decode($result, true);
	}

	public function GetDetailMahasiswaLulusDO($filter)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetDetailMahasiswaLulusDO',
			'token' => $this->token,
			'filter' => $filter,
		]));

		return json_decode($result, true);
	}

	public function GetListMahasiswaLulusDO($filter)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetListMahasiswaLulusDO',
			'token' => $this->token,
			'filter' => $filter,
		]));

		return json_decode($result, true);
	}

	public function InsertMahasiswaLulusDO($data)
	{
		$result = $this->runWS(json_encode([
			'act' => 'InsertMahasiswaLulusDO',
			'token' => $this->token,
			'record' => $data
		]));

		return json_decode($result, true);
	}

	public function UpdateMahasiswaLulusDO($key, $data)
	{
		$result = $this->runWS(json_encode([
			'act' => 'UpdateMahasiswaLulusDO',
			'token' => $this->token,
			'key' => $key,
			'record' => $data
		]));

		return json_decode($result, true);
	}

	public function InsertMatkulKurikulum($data)
	{
		$result = $this->runWS(json_encode([
			'act' => 'InsertMatkulKurikulum',
			'token' => $this->token,
			'record' => $data
		]));

		return json_decode($result, true);
	}

	// return example
	// {
	// 	"id_kelas_kuliah": "00f8b6e2-f561-4f52-beb7-d566b0dfcc66",
	// 	"id_prodi": "96f4e4f6-adba-41b6-8d92-92369808ae44",
	// 	"nama_program_studi": "S1 Ilmu Hukum",
	// 	"id_semester": "20201",
	// 	"nama_semester": "2020/2021 Ganjil",
	// 	"id_matkul": "d86b7e1c-899f-4bb0-99dc-b8b687571c1b",
	// 	"kode_mata_kuliah": "31.4.15.7.05",
	// 	"nama_mata_kuliah": "KKN",
	// 	"nama_kelas_kuliah": "MLM-A",
	// 	"sks": "2.00",
	// 	"id_dosen": null,
	// 	"nama_dosen": null,
	// 	"jumlah_mahasiswa": "0",
	// 	"apa_untuk_pditt": "0"
	//   }
	public function GetListKelasKuliah($filter)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetListKelasKuliah',
			'token' => $this->token,
			'filter' => $filter
		]));

		return json_decode($result, true);
	}

	public function InsertKelasKuliah($data)
	{
		$result = $this->runWS(json_encode([
			'act' => 'InsertKelasKuliah',
			'token' => $this->token,
			'record' => $data
		]));

		return json_decode($result, true);
	}

	public function UpdateKelasKuliah($key, $data)
	{
		$result = $this->runWS(json_encode([
			'act' => 'UpdateKelasKuliah',
			'token' => $this->token,
			'key' => $key,
			'record' => $data
		]));

		return json_decode($result, true);
	}

	public function DeleteKelasKuliah($key)
	{
		$result = $this->runWS(json_encode([
			'act' => 'DeleteKelasKuliah',
			'token' => $this->token,
			'key' => $key
		]));

		return json_decode($result, true);
	}

	// return example
	// {
	// 	"id_kelas_kuliah": "0a5f08fc-0f7b-4394-9ff7-21a394d9c678",
	// 	"nama_kelas_kuliah": "MLM-A",
	// 	"id_registrasi_mahasiswa": "24b5b923-ea76-4855-9a7c-039a95e5a7d0",
	// 	"id_mahasiswa": "f5cd531c-5567-4aa2-80c8-cb08f74712ed",
	// 	"nim": "341213505",
	// 	"nama_mahasiswa": "PRANELIANI PUTRANTI M",
	// 	"id_matkul": "85f0e0a4-016f-4efd-8578-86a6b3689206",
	// 	"kode_mata_kuliah": "31215519",
	// 	"nama_mata_kuliah": "Hukum Pidana Khusus",
	// 	"id_prodi": "96f4e4f6-adba-41b6-8d92-92369808ae44",
	// 	"nama_program_studi": "S1 Ilmu Hukum",
	// 	"angkatan": "2014",
	// 	"status_sync": "sudah sync"
	//   }
	public function GetPesertaKelasKuliah($filter)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetPesertaKelasKuliah',
			'token' => $this->token,
			'filter' => $filter,
			'limit' => 5
		]));

		return json_decode($result, true);
	}

	public function InsertPesertaKelasKuliah($data)
	{
		$result = $this->runWS(json_encode([
			'act' => 'InsertPesertaKelasKuliah',
			'token' => $this->token,
			'record' => $data
		]));

		return json_decode($result, true);
	}

	public function DeletePesertaKelasKuliah($key)
	{
		$result = $this->runWS(json_encode([
			'act' => 'DeletePesertaKelasKuliah',
			'token' => $this->token,
			'key' => $key
		]));

		return json_decode($result, true);
	}

	// return example
	

	public function GetListNilaiPerkuliahanKelas($filter)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetListNilaiPerkuliahanKelas',
			'token' => $this->token,
			'filter' => $filter,
			'limit' => 5
		]));

		return json_decode($result, true);
	}

	// return example
	// {
	// 	"id_prodi": "944fc32c-18f3-47f9-bc19-6e711480c858",
	// 	"nama_program_studi": "S1 Teknik Mesin",
	// 	"id_semester": "20201",
	// 	"nama_semester": "2020/2021 Ganjil",
	// 	"id_matkul": "f14b4730-1cee-43fe-ae87-571214cb2b0a",
	// 	"kode_mata_kuliah": "11218105",
	// 	"nama_mata_kuliah": "KIMIA DASAR",
	// 	"sks_mata_kuliah": "2.00",
	// 	"id_kelas_kuliah": "02a4e52a-3fe6-4d29-9a4f-65b67f49c66c",
	// 	"nama_kelas_kuliah": "A",
	// 	"id_registrasi_mahasiswa": "cbb12b15-aba8-4b30-8467-d45b0acf39f5",
	// 	"id_mahasiswa": "0fc7c672-f3b7-4936-9033-898d5d0845d4",
	// 	"nim": "141220006",
	// 	"nama_mahasiswa": "MUCHAMMAD THAARIQ BAGAS YUDHISTIRA",
	// 	"jurusan": "S1 Teknik Mesin",
	// 	"angkatan": "2020",
	// 	"nilai_angka": "85.0",
	// 	"nilai_indeks": "4.00",
	// 	"nilai_huruf": "A"
	//   }
	public function GetDetailNilaiPerkuliahanKelas($filter)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetDetailNilaiPerkuliahanKelas',
			'token' => $this->token,
			'filter' => $filter,
		]));

		return json_decode($result, true);
	}

	/**
	 * @param $key array id_registrasi_mahasiswa, id_kelas_kuliah
	 * @param $data
	 * @return mixed
	 */
	public function UpdateNilaiPerkuliahanKelas($key, $data)
	{
		$result = $this->runWS(json_encode([
			'act' => 'UpdateNilaiPerkuliahanKelas',
			'token' => $this->token,
			'key' => $key,
			'record' => $data
		]));

		return json_decode($result, true);
	}

	public function GetNilaiTransferPendidikanMahasiswa($filter)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetNilaiTransferPendidikanMahasiswa',
			'token' => $this->token,
			'filter' => $filter
		]));

		return json_decode($result, true);
	}

	public function InsertNilaiTransferPendidikanMahasiswa($data)
	{
		$result = $this->runWS(json_encode([
			'act' => 'InsertNilaiTransferPendidikanMahasiswa',
			'token' => $this->token,
			'record' => $data
		]));

		return json_decode($result, true);
	}

	public function UpdateNilaiTransferPendidikanMahasiswa($key, $data)
	{
		$result = $this->runWS(json_encode([
			'act' => 'UpdateNilaiTransferPendidikanMahasiswa',
			'token' => $this->token,
			'key' => $key,
			'record' => $data
		]));

		return json_decode($result, true);
	}

	public function DeleteNilaiTransferPendidikanMahasiswa($key)
	{
		$result = $this->runWS(json_encode([
			'act' => 'DeleteNilaiTransferPendidikanMahasiswa',
			'token' => $this->token,
			'key' => $key
		]));

		return json_decode($result, true);
	}

	public function GetDosenPengajarKelasKuliah($filter)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetDosenPengajarKelasKuliah',
			'token' => $this->token,
			'filter' => $filter
		]));

		return json_decode($result, true);
	}

	public function InsertDosenPengajarKelasKuliah($data)
	{
		$result = $this->runWS(json_encode([
			'act' => 'InsertDosenPengajarKelasKuliah',
			'token' => $this->token,
			'record' => $data
		]));

		return json_decode($result, true);
	}

	public function UpdateDosenPengajarKelasKuliah($key, $data)
	{
		$result = $this->runWS(json_encode([
			'act' => 'UpdateDosenPengajarKelasKuliah',
			'token' => $this->token,
			'key' => $key,
			'record' => $data
		]));

		return json_decode($result, true);
	}

	public function DeleteDosenPengajarKelasKuliah($key)
	{
		$result = $this->runWS(json_encode([
			'act' => 'DeleteDosenPengajarKelasKuliah',
			'token' => $this->token,
			'key' => $key
		]));

		return json_decode($result, true);
	}

	// return example
	// {
	// 	"id_registrasi_mahasiswa": "5de91768-e061-49e7-b2cd-d1899c346178",
	// 	"nim": "341220076",
	// 	"nama_mahasiswa": "OKY PERMANA",
	// 	"id_prodi": "96f4e4f6-adba-41b6-8d92-92369808ae44",
	// 	"nama_program_studi": "S1 Ilmu Hukum",
	// 	"angkatan": "2020",
	// 	"id_periode_masuk": "20201",
	// 	"id_semester": "20201",
	// 	"nama_semester": "2020/2021 Ganjil",
	// 	"id_status_mahasiswa": "A",
	// 	"nama_status_mahasiswa": "Aktif",
	// 	"ips": "3.67",
	// 	"ipk": "3.67",
	// 	"sks_semester": "18",
	// 	"sks_total": "18",
	// 	"biaya_kuliah_smt": "6600000.00",
	// 	"id_pembiayaan": null,
	// 	"status_sync": "sudah sync"
	//   }
	public function GetListPerkuliahanMahasiswa($filter)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetListPerkuliahanMahasiswa',
			'token' => $this->token,
			'filter' => $filter
		]));

		return json_decode($result, true);
	}

	public function InsertPerkuliahanMahasiswa($data)
	{
		$result = $this->runWS(json_encode([
			'act' => 'InsertPerkuliahanMahasiswa',
			'token' => $this->token,
			'record' => $data
		]));

		return json_decode($result, true);
	}

	public function UpdatePerkuliahanMahasiswa($key, $data)
	{
		$result = $this->runWS(json_encode([
			'act' => 'UpdatePerkuliahanMahasiswa',
			'token' => $this->token,
			'key' => $key,
			'record' => $data
		]));

		return json_decode($result, true);
	}

	public function DeletePerkuliahanMahasiswa($key)
	{
		$result = $this->runWS(json_encode([
			'act' => 'DeletePerkuliahanMahasiswa',
			'token' => $this->token,
			'key' => $key,
		]));

		return json_decode($result, true);
	}

	public function GetListDosen($filter = null)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetListDosen',
			'token' => $this->token,
			'filter' => $filter
		]));

		return $this->array_return($result);
	}

	public function GetListPenugasanDosen($filter = null, $limit = null)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetListPenugasanDosen',
			'token' => $this->token,
			'filter' => $filter,
			'limit' => $limit
		]));

		return $this->array_return($result);
	}

	public function GetJenisPendaftaran($filter = null, $limit = null)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetJenisPendaftaran',
			'token' => $this->token
		]));

		return $this->array_return($result);
	}

	public function getJenisTinggal($filter = null, $limit = null)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetJenisTinggal',
			'token' => $this->token
		]));

		return $this->array_return($result);
	}

	public function GetPembiayaan($filter = null, $limit = null)
	{
		$result = $this->runWS(json_encode([
			'act' => 'GetPembiayaan',
			'token' => $this->token
		]));

		return $this->array_return($result);
	}
}
