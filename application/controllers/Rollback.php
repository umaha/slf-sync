<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property Remotedb $rdb Remote DB Sistem Langitan
 * @property string $token Token webservice
 * @property string $npsn Kode Perguruan Tinggi
 * @property array $satuan_pendidikan Row: satuan_pendidikan
 * @property Langitan_model $langitan_model
 * @property PerguruanTinggi_model $pt
 * @property Feederws $feederws
 */
class Rollback extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->check_credentials();

		// Inisialisasi Token dan Satuan Pendidikan
		$this->token = $this->session->userdata('token');
		$this->satuan_pendidikan = $this->session->userdata(FEEDER_SATUAN_PENDIDIKAN);

		// Inisialisasi URL Feeder
		$this->load->library('feeder', array('url' => $this->session->userdata('wsdl')));

		// Inisialisasi URL Feeder WS 2
		$this->load->library('feederws', [
			'url' => $this->session->userdata('ws2url'),
			'token' => $this->session->userdata('token')
		]);

		// Inisialisasi Library RemoteDB
		$this->load->library('remotedb', NULL, 'rdb');
		$this->rdb->set_url($this->session->userdata('langitan'));

		// Inisialisasi Langitan_model
		$this->load->model('langitan_model');

		// Inisialisasi Perguruan Tinggi
		$this->pt = $this->session->userdata('pt');
	}

    function riwayat_pendidikan_mahasiswa()
	{
		$jumlah = array();

		// Ambil jumlah mahasiswa di feeder (off 2022)
		$jumlah['feeder'] = '-';

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		// Ambil Semua angkatan yg ada :
		$sql_angkatan_mhs_raw = file_get_contents(APPPATH . 'models/sql/mahasiswa-angkatan.sql');
		$sql_angkatan_mhs = strtr($sql_angkatan_mhs_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$angkatan_set = $this->rdb->QueryToArray($sql_angkatan_mhs);
		$this->smarty->assign('angkatan_set', $angkatan_set);

		$this->smarty->assign('url_rollback', site_url('rollback/start/' . $this->uri->segment(2)));
		$this->smarty->display('rollback/' . $this->uri->segment(2) . '.tpl');
	}

	function riwayat_pendidikan_mahasiswa_data($id_prodi, $angkatan)
	{

        $filter = "id_prodi='{$id_prodi}' AND id_periode_masuk='{$angkatan}1'";
		$riwayatPendidikan = $this->feederws->GetListRiwayatPendidikanMahasiswaProdi($filter);

		$jumlah['feeder'] = count($riwayatPendidikan["data"]);

		echo json_encode($jumlah);
	}

	private function proses_riwayat_pendidikan_mahasiswa()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('rollback/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

        
        if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			$angkatan = $this->input->post('angkatan');
			$id_prodi = $this->input->post('kode_prodi');

            $filter = "id_prodi='{$id_prodi}' AND id_periode_masuk='{$angkatan}1'";
            $riwayat_pendidikan_set = $this->feederws->GetListRiwayatPendidikanMahasiswaProdi($filter);

			// simpan ke cache
			$this->session->set_userdata('riwayat_pendidikan_rollback_set', $riwayat_pendidikan_set["data"]);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Rollback. Jumlah data: ' . count($riwayat_pendidikan_set['data']);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		}
		// ----------------------------------------------
		// Proses Sinkronisasi dari data yg sudah diambil
		// ----------------------------------------------
		else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$riwayat_pendidikan_rollback_set = $this->session->userdata('riwayat_pendidikan_rollback_set');
			$jumlah_rollback = count($riwayat_pendidikan_rollback_set);

			if ($index_proses < $jumlah_rollback) {

                $riwayat_pendidikan_rollback = $riwayat_pendidikan_rollback_set[$index_proses];

				$id_reg_pd = $riwayat_pendidikan_rollback['id_registrasi_mahasiswa'];
				$nim_mhs = $riwayat_pendidikan_rollback['nim'];

				$rollback_result = $this->feederws->DeleteRiwayatPendidikanMahasiswa(
					['id_registrasi_mahasiswa' => $id_reg_pd]
				);

				if ($rollback_result['error_code'] == 0) {
                    // $rollback_result[];

                    $this->rdb->Query("UPDATE mahasiswa 
                        SET fd_id_reg_pd = null, 
                        fd_sync_on = null
                        WHERE fd_id_reg_pd = '{$id_reg_pd}'");

                    $result['message'] = ($index_proses + 1) . " Rollback {$nim_mhs} : Berhasil";
				} else {
					$result['message'] = ($index_proses + 1) . " Rollback {$nim_mhs} : Gagal. ";
					$result['message'] .= "({$rollback_result['error_code']}) {$rollback_result['error_desc']}";
				}

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Selesai
			// --------------------------------
			else {
				$result['message'] = "Selesai";
				$result['status'] = SYNC_STATUS_DONE;
			}
		}

		echo json_encode($result);
	}


	/**
	 * GET /rollback/mata_kuliah
	 */
	function mata_kuliah()
	{
		$jumlah = array();

		// Ambil jumlah mata_kuliah di feeder. Sudah obsolete
		$jumlah['feeder'] = '-';

		// Ambil data mata kuliah
		$sql_mk_raw = file_get_contents(APPPATH . 'models/sql/mata-kuliah.sql');
		$sql_mk = strtr($sql_mk_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$mk_set = $this->rdb->QueryToArray($sql_mk);

		$jumlah['langitan'] = $mk_set[0]['JUMLAH'];
		$jumlah['linked'] = $mk_set[1]['JUMLAH'];
		$jumlah['update'] = $mk_set[2]['JUMLAH'];
		$jumlah['insert'] = $mk_set[3]['JUMLAH'];

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		$this->smarty->assign('url_sync', site_url('rollback/start/' . $this->uri->segment(2)));
		$this->smarty->display('rollback/' . $this->uri->segment(2) . '.tpl');
	}

	/**
	 * Ajax-GET /rollback/mata_kuliah_data/
	 * @param string $kode_prodi Kode program studi versi Feeder
	 */
	function mata_kuliah_data($kode_prodi)
	{
		// Ambil jumlah mata kuliah di feeder. Sudah obsolete.
		$jumlah['feeder'] = '-';

		// Ambil data mata kuliah
		$sql_mk_raw = file_get_contents(APPPATH . 'models/sql/mata-kuliah-data.sql');
		$sql_mk = strtr($sql_mk_raw, array(
			'@npsn' => $this->satuan_pendidikan['npsn'],
			'@kode_prodi' => $kode_prodi
		));

		$mk_set = $this->rdb->QueryToArray($sql_mk);

		$jumlah['langitan'] = $mk_set[0]['JUMLAH'];
		$jumlah['linked'] = $mk_set[1]['JUMLAH'];
		$jumlah['update'] = $mk_set[2]['JUMLAH'];
		$jumlah['insert'] = $mk_set[3]['JUMLAH'];

		echo json_encode($jumlah);
	}

	private function proses_mata_kuliah()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('rollback/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		// -----------------------------------
		// Ambil data untuk Insert
		// -----------------------------------
		if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			// Filter Prodi & Angkatan
			$kode_prodi = $this->input->post('kode_prodi');

			// Mendapatkan id_sms
			// $response = $this->feeder->GetRecord($this->token, FEEDER_SMS, "id_sp = '{$this->satuan_pendidikan['id_sp']}' AND trim(kode_prodi) = '{$kode_prodi}'");
			// $sms = $response['result'];

			// Ambil data mata kuliah
			$sql_mk_raw = file_get_contents(APPPATH . 'models/sql/mata-kuliah-insert.sql');
			$sql_mk = strtr($sql_mk_raw, array(
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@kode_prodi' => $kode_prodi
			));
			$mk_set = $this->rdb->QueryToArray($sql_mk);

			for ($i = 0; $i < count($mk_set); $i++) {
				$mk_set[$i]["SKS_MATA_KULIAH"] = (int)$mk_set[$i]["SKS_MATA_KULIAH"];
				$mk_set[$i]["SKS_TATAP_MUKA"] = (int)$mk_set[$i]["SKS_TATAP_MUKA"];
				$mk_set[$i]["SKS_PRAKTEK"] = (int)$mk_set[$i]["SKS_PRAKTEK"];
				$mk_set[$i]["SKS_PRAKTEK_LAPANGAN"] = (int)$mk_set[$i]["SKS_PRAKTEK_LAPANGAN"];
				$mk_set[$i]["SKS_SIMULASI"] = (int)$mk_set[$i]["SKS_SIMULASI"];
				$mk_set[$i]["ADA_SAP"] = (int)$mk_set[$i]["ADA_SAP"];
				$mk_set[$i]["ADA_SILABUS"] = (int)$mk_set[$i]["ADA_SILABUS"];
				$mk_set[$i]["ADA_BAHAN_AJAR"] = (int)$mk_set[$i]["ADA_BAHAN_AJAR"];
				$mk_set[$i]["ADA_ACARA_PRAKTEK"] = (int)$mk_set[$i]["ADA_ACARA_PRAKTEK"];
				$mk_set[$i]["ADA_DIKTAT"] = (int)$mk_set[$i]["ADA_DIKTAT"];
			}
			$this->session->set_userdata('mata_kuliah_insert_set', $mk_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Entri. Jumlah data: ' . count($mk_set);
			$result['status'] = SYNC_STATUS_PROSES;
			// $result['status'] = SYNC_STATUS_DONE;

			// die();
			// ganti parameter
			$_POST['mode'] = MODE_AMBIL_DATA_LANGITAN_2;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Ambil data untuk Update
		// -----------------------------------
		else if ($mode == MODE_AMBIL_DATA_LANGITAN_2) {
			// Filter Prodi & Angkatan
			$kode_prodi = $this->input->post('kode_prodi');

			// Mendapatkan id_sms
			// $response = $this->feeder->GetRecord($this->token, FEEDER_SMS, "id_sp = '{$this->satuan_pendidikan['id_sp']}' AND trim(kode_prodi) = '{$kode_prodi}'");
			// $sms = $response['result'];

			// Ambil data mata kuliah
			$sql_mk_raw = file_get_contents(APPPATH . 'models/sql/mata-kuliah-update.sql');
			$sql_mk = strtr($sql_mk_raw, array(
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@kode_prodi' => $kode_prodi
			));

			$mk_set = $this->rdb->QueryToArray($sql_mk);

			for ($i = 0; $i < count($mk_set); $i++) {
				$mk_set[$i]["SKS_MATA_KULIAH"] = (int)$mk_set[$i]["SKS_MATA_KULIAH"];
				$mk_set[$i]["SKS_TATAP_MUKA"] = (int)$mk_set[$i]["SKS_TATAP_MUKA"];
				$mk_set[$i]["SKS_PRAKTEK"] = (int)$mk_set[$i]["SKS_PRAKTEK"];
				$mk_set[$i]["SKS_PRAKTEK_LAPANGAN"] = (int)$mk_set[$i]["SKS_PRAKTEK_LAPANGAN"];
				$mk_set[$i]["SKS_SIMULASI"] = (int)$mk_set[$i]["SKS_SIMULASI"];
				$mk_set[$i]["ADA_SAP"] = (int)$mk_set[$i]["ADA_SAP"];
				$mk_set[$i]["ADA_SILABUS"] = (int)$mk_set[$i]["ADA_SILABUS"];
				$mk_set[$i]["ADA_BAHAN_AJAR"] = (int)$mk_set[$i]["ADA_BAHAN_AJAR"];
				$mk_set[$i]["ADA_ACARA_PRAKTEK"] = (int)$mk_set[$i]["ADA_ACARA_PRAKTEK"];
				$mk_set[$i]["ADA_DIKTAT"] = (int)$mk_set[$i]["ADA_DIKTAT"];
			}
			$this->session->set_userdata('mata_kuliah_update_set', $mk_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Update. Jumlah data: ' . count($mk_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Proses Sinkronisasi dari data yg sudah diambil
		// -----------------------------------
		else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$mata_kuliah_insert_set = $this->session->userdata('mata_kuliah_insert_set');
			$jumlah_insert = count($mata_kuliah_insert_set);


			// Ambil dari cache
			$mata_kuliah_update_set = $this->session->userdata('mata_kuliah_update_set');
			$jumlah_update = count($mata_kuliah_update_set);

			// Waktu Sinkronisasi
			$time_sync = date('Y-m-d H:i:s');

			// --------------------------------
			// Proses Insert
			// --------------------------------
			if ($index_proses < $jumlah_insert) {
				// Proses dalam bentuk key lowercase
				$mata_kuliah_insert = array_change_key_case($mata_kuliah_insert_set[$index_proses], CASE_LOWER);

				// Simpan id_mata_kuliah untuk update data di langitan
				$id_mata_kuliah = $mata_kuliah_insert['id_mata_kuliah'];

				// Hilangkan id_mata_kuliah
				unset($mata_kuliah_insert['id_mata_kuliah']);

				// Entri ke Feeder Mata Kuliah
				$insert_result = $this->feederws->InsertMataKuliah($mata_kuliah_insert);

				// Jika berhasil insert, terdapat return id_mk
				if (isset($insert_result['data']['id_matkul'])) {
					// Pesan Insert, tampilkan kode mk dan nama mk
					$result['message'] = ($index_proses + 1) . " Insert {$mata_kuliah_insert['kode_mata_kuliah']} {$mata_kuliah_insert['nama_mata_kuliah']} : Berhasil";

					// status sandbox
					$is_sandbox = ($this->session->userdata('is_sandbox') == TRUE) ? '1' : '0';

					// Update mata_kuliah.fd_id_mk
					$this->rdb->Query("UPDATE mata_kuliah SET fd_id_mk = '{$insert_result['data']['id_matkul']}', fd_sync_on = sysdate WHERE id_mata_kuliah = {$id_mata_kuliah}");
				} else // saat insert mata_kuliah gagal
				{
					// Pesan Insert, kode mk  mengambil dari mata_kuliah
					$result['message'] = ($index_proses + 1) . " Insert {$mata_kuliah_insert['kode_mata_kuliah']} {$mata_kuliah_insert['nama_mata_kuliah']} : " . json_encode($insert_result);
				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Proses Update
			// --------------------------------
			else if ($index_proses < ($jumlah_insert + $jumlah_update)) {
				// index berjalan dikurangi jumlah data insert utk mendapatkan index update
				$index_proses -= $jumlah_insert;

				// Proses dalam bentuk key lowercase
				$mata_kuliah_update = array_change_key_case($mata_kuliah_update_set[$index_proses], CASE_LOWER);

				// Simpan id_mk dan id_mata_kuliah untuk update data di langitan
				$id_mk			= $mata_kuliah_update['id_mk'];
				$id_mata_kuliah	= $mata_kuliah_update['id_mata_kuliah'];
				$kode_mk		= $mata_kuliah_update['kode_mata_kuliah'];
				$nm_mk			= $mata_kuliah_update['nama_mata_kuliah'];

				// Hilangkan id_mk & id_mata_kuliah
				unset($mata_kuliah_update['id_mk']);
				unset($mata_kuliah_update['id_mata_kuliah']);
				// unset($mata_kuliah_update['kode_mata_kuliah']);
				// unset($mata_kuliah_update['nama_mata_kuliah']);

				// Build data format
				$data_update = array(
					'key'	=> array('id_matkul' => $id_mk),
					$mata_kuliah_update
				);

				// Update ke Feeder Mata Kuliah
				$update_result = $this->feederws->UpdateMataKuliah(['id_matkul' => $id_mk], $mata_kuliah_update);

				// Jika tidak ada masalah update
				if ($update_result['error_code'] == 0) {
					$result['message'] = ($index_proses + 1) . " Update {$kode_mk} {$nm_mk} : Berhasil";

					$this->rdb->Query("UPDATE mata_kuliah SET fd_sync_on = sysdate WHERE id_mata_kuliah = {$id_mata_kuliah}");
				} else {
					$result['message'] = ($index_proses + 1) . " Update {$kode_mk} : Gagal. ";
					$result['message'] .= "({$update_result['error_code']}) {$update_result['error_desc']}";
					$result['message'] .= "\n" . json_encode($data_update);
				}

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// meneruskan index proses ditambah lagi dengan jumlah data insert
				$index_proses += $jumlah_insert;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Selesai
			// --------------------------------
			else {
				$result['message'] = "Selesai";
				$result['status'] = SYNC_STATUS_DONE;
			}
		}

		echo json_encode($result);
	}

	/**
	 * GET /rollback/kurikulum/
	 */
	function kurikulum()
	{
		// Ambil jumlah kurikulum di feeder. Sudah obsolete.
		$jumlah['feeder'] = '-';

		// Ambil data kurikulum
		$sql_kurikulum_raw = file_get_contents(APPPATH . 'models/sql/kurikulum.sql');
		$sql_kurikulum = strtr($sql_kurikulum_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$kurikulum_set = $this->rdb->QueryToArray($sql_kurikulum);

		$jumlah['langitan'] = $kurikulum_set[0]['JUMLAH'];
		$jumlah['linked'] = $kurikulum_set[1]['JUMLAH'];
		$jumlah['update'] = $kurikulum_set[2]['JUMLAH'];
		$jumlah['insert'] = $kurikulum_set[3]['JUMLAH'];

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		$this->smarty->assign('url_sync', site_url('rollback/start/' . $this->uri->segment(2)));
		$this->smarty->display('rollback/' . $this->uri->segment(2) . '.tpl');
	}

	/**
	 * Ajax-GET /rollback/kurikulum_data/
	 * @param string $kode_prodi Kode program studi versi Feeder
	 */
	function kurikulum_data($kode_prodi)
	{
		// Ambil jumlah kurikulum di feeder. Sudah obsolete.
		$jumlah['feeder'] = '-';

		// Ambil data kurikulum
		$sql_kurikulum_raw = file_get_contents(APPPATH . 'models/sql/kurikulum-data.sql');
		$sql_kurikulum = strtr($sql_kurikulum_raw, array(
			'@npsn' => $this->satuan_pendidikan['npsn'],
			'@kode_prodi' => $kode_prodi
		));

		$kurikulum_set = $this->rdb->QueryToArray($sql_kurikulum);

		$jumlah['langitan'] = $kurikulum_set[0]['JUMLAH'];
		$jumlah['linked'] = $kurikulum_set[1]['JUMLAH'];
		$jumlah['update'] = $kurikulum_set[2]['JUMLAH'];
		$jumlah['insert'] = $kurikulum_set[3]['JUMLAH'];

		echo json_encode($jumlah);
	}

	private function proses_kurikulum()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('rollback/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		// -----------------------------------
		// Ambil data untuk Insert
		// -----------------------------------
		if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			// Filter Prodi
			$kode_prodi = $this->input->post('kode_prodi');

			// Ambil data kurikulum yang akan di insert
			$sql_kurikulum_raw = file_get_contents(APPPATH . 'models/sql/kurikulum-insert.sql');
			$sql_kurikulum = strtr($sql_kurikulum_raw, array(
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@kode_prodi' => $kode_prodi
			));

			$kurikulum_set = $this->rdb->QueryToArray($sql_kurikulum);

			$this->session->set_userdata('kurikulum_insert_set', $kurikulum_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Entri. Jumlah data: ' . count($kurikulum_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_AMBIL_DATA_LANGITAN_2;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Ambil data untuk Update
		// -----------------------------------
		else if ($mode == MODE_AMBIL_DATA_LANGITAN_2) {
			// Filter Prodi & Angkatan
			$kode_prodi = $this->input->post('kode_prodi');

			// Ambil data kurikulum yang akan di insert
			$sql_kurikulum_raw = file_get_contents(APPPATH . 'models/sql/kurikulum-update.sql');
			$sql_kurikulum = strtr($sql_kurikulum_raw, array(
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@kode_prodi' => $kode_prodi
			));

			$kurikulum_set = $this->rdb->QueryToArray($sql_kurikulum);

			$this->session->set_userdata('kurikulum_update_set', $kurikulum_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Update. Jumlah data: ' . count($kurikulum_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Proses Sinkronisasi dari data yg sudah diambil
		// -----------------------------------
		else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$kurikulum_insert_set = $this->session->userdata('kurikulum_insert_set');
			$jumlah_insert = count($kurikulum_insert_set);

			// Ambil dari cache
			$kurikulum_update_set = $this->session->userdata('kurikulum_update_set');
			$jumlah_update = count($kurikulum_update_set);

			// Waktu Sinkronisasi
			$time_sync = date('Y-m-d H:i:s');

			// --------------------------------
			// Proses Insert
			// --------------------------------
			if ($index_proses < $jumlah_insert) {
				// Proses dalam bentuk key lowercase
				$kurikulum_insert = array_change_key_case($kurikulum_insert_set[$index_proses], CASE_LOWER);

				// Simpan id_kurikulum untuk update data di langitan
				$id_kurikulum = $kurikulum_insert['id_kurikulum'];

				// Hilangkan id_kurikulum
				unset($kurikulum_insert['id_kurikulum']);

				// Entri ke Feeder Kurikulum
				$insert_result = $this->feederws->InsertKurikulum($kurikulum_insert);

				// Jika berhasil insert, terdapat return id_kurikulum_sp
				if (isset($insert_result['data']['id_kurikulum'])) {
					// Pesan Insert, tampilkan nama kurikulum
					$result['message'] = ($index_proses + 1) . " Insert {$kurikulum_insert['nama_kurikulum']} : Berhasil";

					// Update status sinkron fd_id_kurilum_sp 
					$fd_id_kurikulum_sp = $insert_result['data']['id_kurikulum'];
					$this->rdb->Query("UPDATE kurikulum SET fd_id_kurikulum_sp = '{$fd_id_kurikulum_sp}', fd_sync_on = sysdate WHERE id_kurikulum = {$id_kurikulum}");
				} else // saat insert kurikulum gagal
				{
					// Pesan insert jika gagal
					$result['message'] = ($index_proses + 1) . " Insert {$kurikulum_insert['nama_kurikulum']} : " .
						json_encode($insert_result) . "\n" .
						json_encode($kurikulum_insert);
				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Proses Update
			// --------------------------------
			else if ($index_proses < ($jumlah_insert + $jumlah_update)) {
				// index berjalan dikurangi jumlah data insert utk mendapatkan index update
				$index_proses -= $jumlah_insert;

				// Proses dalam bentuk key lowercase
				$kurikulum_update = array_change_key_case($kurikulum_update_set[$index_proses], CASE_LOWER);

				// Simpan id_kurikulum_sp dan kurikulum_prodi untuk update data di langitan
				$id_kurikulum_sp	= $kurikulum_update['id_kurikulum_sp'];
				$id_kurikulum		= $kurikulum_update['id_kurikulum'];

				// Hilangkan id_kurikulum_sp & id_kurikulum
				unset($kurikulum_update['id_kurikulum_sp']);
				unset($kurikulum_update['id_kurikulum']);

				// Update ke Feeder Kurikulum
				$update_result = $this->feederws->UpdateKurikulum(
					['id_kurikulum' => $id_kurikulum_sp],
					$kurikulum_update
				);

				// Jika tidak ada masalah update
				if ($update_result['error_code'] == 0) {
					$result['message'] = ($index_proses + 1) . " Update {$kurikulum_update['nama_kurikulum']} : Berhasil";

					// Update waktu sync
					$this->rdb->Query("UPDATE kurikulum SET fd_sync_on = sysdate WHERE id_kurikulum = {$id_kurikulum}");
				} else {
					$result['message'] = ($index_proses + 1) . " Update {$kurikulum_update['nama_kurikulum']} : Gagal. ";
					$result['message'] .= "({$update_result['error_code']}) {$update_result['error_desc']}";
					$result['message'] .= "\n" . json_encode($kurikulum_update);
				}

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// meneruskan index proses ditambah lagi dengan jumlah data insert
				$index_proses += $jumlah_insert;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Selesai
			// --------------------------------
			else {
				$result['message'] = "Selesai";
				$result['status'] = SYNC_STATUS_DONE;
			}
		}

		echo json_encode($result);
	}

	/**
	 * GET /rollback/mata_kuliah_kurikulum/
	 */
	function mata_kuliah_kurikulum()
	{
		// Ambil jumlah mk kurikulum di feeder
		// $response = $this->feeder->GetCountRecordset($this->token, FEEDER_MK_KURIKULUM, null);
		$jumlah['feeder'] = '-';

		// Ambil data mata kuliah kurikulum
		$sql_mk_kurikulum_raw = file_get_contents(APPPATH . 'models/sql/mata-kuliah-kurikulum.sql');
		$sql_mk_kurikulum = strtr($sql_mk_kurikulum_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$data_set = $this->rdb->QueryToArray($sql_mk_kurikulum);

		$jumlah['langitan'] = $data_set[0]['JUMLAH'];
		$jumlah['linked'] = $data_set[1]['JUMLAH'];
		$jumlah['update'] = $data_set[2]['JUMLAH'];
		$jumlah['insert'] = $data_set[3]['JUMLAH'];

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		$this->smarty->assign('url_sync', site_url('rollback/start/' . $this->uri->segment(2)));
		$this->smarty->display('rollback/' . $this->uri->segment(2) . '.tpl');
	}

	/**
	 * 
	 * @param string $kode_prodi
	 * @param string $id_kurikulum_prodi
	 */
	function mata_kuliah_kurikulum_data($kode_prodi, $id_kurikulum)
	{
		// Ambil id_sms dari kode prodi
		// $response = $this->feeder->GetRecord($this->token, FEEDER_SMS, "p.id_sp = '{$this->satuan_pendidikan['id_sp']}' and trim(p.kode_prodi) = '{$kode_prodi}'");
		// $id_sms = $response['result']['id_sms'];

		// Ambil id_kurikulum_sp dari id_kurikulum_prodi
		// $response = $this->rdb->QueryToArray("SELECT id_kurikulum_sp FROM feeder_kurikulum WHERE id_kurikulum_prodi = {$id_kurikulum_prodi}");
		// $id_kurikulum_sp = $response[0]['ID_KURIKULUM_SP'];

		// Ambil jumlah mk kurikulum di feeder
		// $response = $this->feeder->GetCountRecordset($this->token, FEEDER_MK_KURIKULUM, "p.id_kurikulum_sp = '{$id_kurikulum_sp}'");
		$jumlah['feeder'] = '-';

		// Query berbasis id_kurikulum_sp & id_mk

		$sql_mk_kurikulum_raw = file_get_contents(APPPATH . 'models/sql/mata-kuliah-kurikulum-data.sql');
		$sql_mk_kurikulum = strtr(
			$sql_mk_kurikulum_raw,
			[
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@id_kurikulum' => $id_kurikulum
			]
		);

		$data_set = $this->rdb->QueryToArray($sql_mk_kurikulum);

		$jumlah['langitan'] = $data_set[0]['JUMLAH'];
		$jumlah['linked'] = $data_set[1]['JUMLAH'];
		$jumlah['update'] = $data_set[2]['JUMLAH'];
		$jumlah['insert'] = $data_set[3]['JUMLAH'];

		echo json_encode($jumlah);
	}

	private function proses_mata_kuliah_kurikulum()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('rollback/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		// -----------------------------------
		// Ambil data untuk Insert
		// -----------------------------------
		if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			// Filter prodi & kurikulum_prodi
			$kode_prodi			= $this->input->post('kode_prodi');
			$id_kurikulum	= $this->input->post('id_kurikulum');


			// Ambil data kurikulum yang akan di insert
			$sql_mata_kuliah_kurikulum_raw = file_get_contents(APPPATH . 'models/sql/mata-kuliah-kurikulum-insert.sql');
			$sql_mata_kuliah_kurikulum = strtr($sql_mata_kuliah_kurikulum_raw, array(
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@id_kurikulum' => $id_kurikulum,
				'@kode_prodi' => $kode_prodi
			));

			$data_set = $this->rdb->QueryToArray($sql_mata_kuliah_kurikulum);

			$this->session->set_userdata('data_insert_set', $data_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Entri. Jumlah data: ' . count($data_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Proses Sinkronisasi dari data yg sudah diambil
		// -----------------------------------
		else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$data_insert_set = $this->session->userdata('data_insert_set');
			$jumlah_insert = count($data_insert_set);

			// Ambil dari cache
			// $data_update_set = $this->session->userdata('data_update_set');
			// $jumlah_update = count($data_update_set);

			// Waktu Sinkronisasi
			$time_sync = date('Y-m-d H:i:s');

			// --------------------------------
			// Proses Insert
			// --------------------------------
			if ($index_proses < $jumlah_insert) {
				// Proses dalam bentuk key lowercase
				$data_insert = array_change_key_case($data_insert_set[$index_proses], CASE_LOWER);

				// Simpan informasi mata kuliah
				$id_kurikulum_mk		= $data_insert['id_kurikulum_mk'];
				$nm_mata_kuliah		= $data_insert['nm_mata_kuliah'];

				// Hilangkan data tdk diperlukan untuk insert
				unset($data_insert['id_kurikulum_mk']);
				unset($data_insert['nm_mata_kuliah']);

				// Entri ke Feeder mata_kuliah_kurikulum
				$insert_result = $this->feederws->InsertMatkulKurikulum($data_insert);

				// Jika berhasil insert, terdapat return id_kurikulum_sp & id_mk
				if (isset($insert_result['data']['id_kurikulum']) && isset($insert_result['data']['id_matkul'])) {
					// Pesan Insert, tampilkan nama kelas
					$result['message'] = ($index_proses + 1) . " Insert id_kurikulum_mk {$id_kurikulum_mk} {$nm_mata_kuliah} : Berhasil";

					// status sandbox
					$is_sandbox = ($this->session->userdata('is_sandbox') == TRUE) ? '1' : '0';

					// Melakukan insert ke feeder_mk_kurikulum hasil insert
					// $this->rdb->Query(
					// 	"INSERT INTO feeder_mk_kurikulum (ID_KURIKULUM_SP, ID_MK, LAST_SYNC, LAST_UPDATE, IS_SANDBOX)
					// 		VALUES ('{$insert_result['result']['id_kurikulum_sp']}', '{$insert_result['result']['id_mk']}', to_date('{$time_sync}', 'YYYY-MM-DD HH24:MI:SS'), to_date('{$time_sync}', 'YYYY-MM-DD HH24:MI:SS'), {$is_sandbox})"
					// );
					$query = "UPDATE kurikulum_mk 
							SET fd_id_kurikulum_sp = '{$insert_result['data']['id_kurikulum']}',
							fd_id_mk = '{$insert_result['data']['id_matkul']}',
							fd_sync_on = sysdate 
							WHERE id_kurikulum_mk = {$id_kurikulum_mk}";
					$this->rdb->Query($query);
				} else if ($insert_result['error_code'] == 630) { //jika Data mata kuliah kurikulum ini sudah ada
					//get mk kurikulum
					$get_mk_kurikulum_result = $this->feederws
						->GetMatkulKurikulum(
							$data_insert["id_matkul"],
							$data_insert["id_kurikulum"],
						);
					if (
						$get_mk_kurikulum_result["error_code"] == 0 &&
						count($get_mk_kurikulum_result["data"]) > 0
					) {
						$query = "UPDATE kurikulum_mk 
							SET fd_id_kurikulum_sp = '{$get_mk_kurikulum_result['data'][0]['id_kurikulum']}',
							fd_id_mk = '{$get_mk_kurikulum_result['data'][0]['id_matkul']}',
							fd_sync_on = sysdate 
							WHERE id_kurikulum_mk = {$id_kurikulum_mk}";
						$this->rdb->Query($query);
						$result['message'] = ($index_proses + 1) . " Insert id_kurikulum_mk {$id_kurikulum_mk} {$nm_mata_kuliah} : Berhasil";
					}
				} else // saat insert mk kurikulum gagal
				{
					// Pesan insert jika gagal
					$result['message'] = ($index_proses + 1) . " Insert Gagal {$id_kurikulum_mk} {$nm_mata_kuliah} : " . json_encode($insert_result);
				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}

			// --------------------------------
			// Selesai
			// --------------------------------
			else {
				$result['message'] = "Selesai";
				$result['status'] = SYNC_STATUS_DONE;
			}
		}

		echo json_encode($result);
	}

	/**
	 * AJAX-GET /rollback/ambil_kurikulum/{$kode_prodi}
	 */
	function ambil_kurikulum($kode_prodi)
	{
		// Ambil data kurikulum yg sudah ada di feeder
		$sql =
			"SELECT
			k.id_kurikulum,
			nm_kurikulum AS nama_kurikulum
		FROM kurikulum k
		JOIN semester s ON s.id_semester = k.id_semester_mulai
		JOIN program_studi ps ON ps.id_program_studi = k.id_program_studi
		JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
		JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
		JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
		WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' 
		AND ps.kode_program_studi = '{$kode_prodi}' 
		AND ps.status_aktif_prodi = 1
		AND k.fd_id_kurikulum_sp IS NOT NULL";

		$kurikulum_set = $this->rdb->QueryToArray($sql);

		echo json_encode($kurikulum_set);
	}

	/**
	 * GET /rollback/kelas_kuliah/
	 */
	function kelas_kuliah()
	{
		// Ambil jumlah kelas kuliah di feeder. Sudah obsolete.
		$jumlah['feeder'] = '-';

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		$this->smarty->assign('url_rollback', site_url('rollback/start/' . $this->uri->segment(2)));
		$this->smarty->display('rollback/' . $this->uri->segment(2) . '.tpl');
	}

	/**
	 * Ajax-GET /rollback/kelas_kuliah_data/
	 * @param string $kode_prodi Kode program studi versi Feeder
	 * @param string $id_smt Kode semester, format 20151 / 20152
	 */
	function kelas_kuliah_data($id_prodi, $id_smt)
	{
		// Ambil jumlah kelas kuliah di feeder
		$jumlah['feeder'] = '-';

        $filter = "id_semester='{$id_smt}' AND id_prodi='{$id_prodi}'";
		$listKelas = $this->feederws->GetListKelasKuliah($filter);

		$jumlah['feeder'] = count($listKelas["data"]);

		echo json_encode($jumlah);
	}

	private function proses_kelas_kuliah()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('rollback/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

        
        if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			$id_smt = $this->input->post('semester');
			$id_prodi = $this->input->post('kode_prodi');

            $filter = "id_semester='{$id_smt}' AND id_prodi='{$id_prodi}'";
            $listKelas = $this->feederws->GetListKelasKuliah($filter);
			// simpan ke cache
			$this->session->set_userdata('kelas_rollback_set', $listKelas["data"]);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Rollback. Jumlah data: ' . count($listKelas['data']);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		}
		// ----------------------------------------------
		// Proses Sinkronisasi dari data yg sudah diambil
		// ----------------------------------------------
		else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$kelas_rollback_set = $this->session->userdata('kelas_rollback_set');
			$jumlah_rollback = count($kelas_rollback_set);

			if ($index_proses < $jumlah_rollback) {

                $kelas_rollback = $kelas_rollback_set[$index_proses];

				// Simpan id_reg_pd untuk keperluan update di langitan
				$id_kelas_kuliah = $kelas_rollback['id_kelas_kuliah'];
				$nama_kelas_kuliah = $kelas_rollback['nama_kelas_kuliah'];

				// Update ke Feeder Kuliah Mahasiswa
				$rollback_result = $this->feederws->DeleteKelasKuliah(
					['id_kelas_kuliah' => $id_kelas_kuliah]
				);

				// Jika tidak ada masalah update
				if ($rollback_result['error_code'] == 0) {
                    // $rollback_result[];
					$result['message'] = ($index_proses + 1) . "Rollback Nama Kelas {$nama_kelas_kuliah} : Berhasil";

                    $this->rdb->Query("UPDATE kelas_mk 
                        SET fd_id_kls = null, 
                        fd_sync_on = null 
                        WHERE fd_id_kls = '{$id_kelas_kuliah}'");

				} else {
					$result['message'] = ($index_proses + 1) . " Rollback kelas {$nama_kelas_kuliah} : Gagal. ";
					$result['message'] .= "({$rollback_result['error_code']}) {$rollback_result['error_desc']}";
				}

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Selesai
			// --------------------------------
			else {
				$result['message'] = "Selesai";
				$result['status'] = SYNC_STATUS_DONE;
			}
		}

		echo json_encode($result);
	}

	/**
	 * GET /rollback/ajar_dosen/
	 */
	function ajar_dosen()
	{
		// Ambil jumlah kelas kuliah di feeder. Sudah obsolete.
		$jumlah['feeder'] = '-';

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		$this->smarty->assign('url_rollback', site_url('rollback/start/' . $this->uri->segment(2)));
		$this->smarty->display('rollback/' . $this->uri->segment(2) . '.tpl');
	}

	/**
	 * Ajax-GET /rollback/ajar_dosen_data/
	 * @param string $kode_prodi Kode program studi versi Feeder
	 * @param string $id_smt Kode semester, format 20151 / 20152
	 */
	function ajar_dosen_data($id_prodi, $id_smt)
	{
		// Dosen ajar tidak bisa di query jumlah
		// $response = $this->feeder->GetCountRecordset($this->token, FEEDER_AJAR_DOSEN, "p.id_kls = '{$id_kls}'");
		$jumlah['feeder'] = '-';

        $filterDosenKelas = "id_prodi = '{$id_prodi}' AND id_semester = '{$id_smt}'";
        $dosenKelas = $this->feederws->GetDosenPengajarKelasKuliah($filterDosenKelas);
		$jumlah['feeder'] = count($dosenKelas['data']);

		echo json_encode($jumlah);
	}

	private function proses_ajar_dosen()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('rollback/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

        if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			$id_smt = $this->input->post('semester');
			$id_prodi = $this->input->post('kode_prodi');

            $filterDosenKelas = "id_prodi = '{$id_prodi}' AND id_semester = '{$id_smt}'";
            $dosenKelas = $this->feederws->GetDosenPengajarKelasKuliah($filterDosenKelas);

			// simpan ke cache
			$this->session->set_userdata('dosen_kelas_rollback_set', $dosenKelas['data']);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Rollback. Jumlah data: ' . count($dosenKelas['data']);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		}
		// ----------------------------------------------
		// Proses Sinkronisasi dari data yg sudah diambil
		// ----------------------------------------------
		else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$dosen_kelas_rollback_set = $this->session->userdata('dosen_kelas_rollback_set');
			$jumlah_rollback = count($dosen_kelas_rollback_set);

			if ($index_proses < $jumlah_rollback) {

                $dosen_kelas_rollback = $dosen_kelas_rollback_set[$index_proses];

				// Simpan  untuk keperluan update di langitan
				$nama_dosen = $dosen_kelas_rollback['nama_dosen'];
				$id_aktivitas_mengajar = $dosen_kelas_rollback['id_aktivitas_mengajar'];

				// Update ke Feeder Kuliah Mahasiswa
				$rollback_result = $this->feederws->DeleteDosenPengajarKelasKuliah(
					['id_aktivitas_mengajar' => $id_aktivitas_mengajar]
				);

				// Jika tidak ada masalah update
				if ($rollback_result['error_code'] == 0) {
                    // $rollback_result[];
					$result['message'] = ($index_proses + 1) . "Rollback Dosen {$nama_dosen} : Berhasil";

                    $this->rdb->Query("UPDATE pengampu_mk 
                        SET fd_id_ajar = null, 
                            fd_sync_on = null 
                        WHERE fd_id_ajar = '{$id_aktivitas_mengajar}'");

				} else {
					$result['message'] = ($index_proses + 1) . " Rollback Dosen {$nama_dosen} : Gagal. ";
					$result['message'] .= "({$rollback_result['error_code']}) {$rollback_result['error_desc']}";
				}

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Selesai
			// --------------------------------
			else {
				$result['message'] = "Selesai";
				$result['status'] = SYNC_STATUS_DONE;
			}
		}

		echo json_encode($result);
	}

    public function nilai_per_prodi()
	{
		// Ambil jumlah nilai di feeder. Sudah obsolete.
		$jumlah['feeder'] = '-';
		$jumlah['kelas'] = '-';

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		$this->smarty->assign('url_rollback', site_url('rollback/start/' . $this->uri->segment(2)));
		$this->smarty->display('rollback/' . $this->uri->segment(2) . '.tpl');
	}

	function nilai_per_prodi_data($id_prodi, $id_smt)
	{

		// Ambil peserta kelas di feeder. Sudah obsolete.
		$jumlah['feeder'] = '-';

        $jumlah_mhs = 0;

        $filter = "id_semester='{$id_smt}' AND id_prodi='{$id_prodi}'";
		$listKelas = $this->feederws->GetListKelasKuliah($filter);
        foreach ($listKelas['data'] as $kelas) {
            $filterPesertaKelas = "id_kelas_kuliah='{$kelas['id_kelas_kuliah']}'";
		    $pesertaKelas = $this->feederws->GetPesertaKelasKuliah($filterPesertaKelas);
            $jumlah_mhs += count($pesertaKelas['data']);
        }

		$jumlah['feeder'] = $jumlah_mhs;
		$jumlah['kelas'] = count($listKelas['data']);

		echo json_encode($jumlah);
	}

	private function proses_nilai_per_prodi()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('rollback/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

        if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			$id_smt = $this->input->post('semester');
			$id_prodi = $this->input->post('kode_prodi');

            $peserta_kelas_set = [];
            $filter = "id_semester='{$id_smt}' AND id_prodi='{$id_prodi}'";
            $listKelas = $this->feederws->GetListKelasKuliah($filter);
            foreach ($listKelas['data'] as $kelas) {
                $filterPesertaKelas = "id_kelas_kuliah='{$kelas['id_kelas_kuliah']}'";
                $pesertaKelas = $this->feederws->GetPesertaKelasKuliah($filterPesertaKelas);
                $peserta_kelas_set = array_merge($peserta_kelas_set, $pesertaKelas['data']);
            }

			// simpan ke cache
			$this->session->set_userdata('peserta_kelas_rollback_set', $peserta_kelas_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Rollback. Jumlah data: ' . count($peserta_kelas_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		}
		// ----------------------------------------------
		// Proses Sinkronisasi dari data yg sudah diambil
		// ----------------------------------------------
		else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$peserta_kelas_rollback_set = $this->session->userdata('peserta_kelas_rollback_set');
			$jumlah_rollback = count($peserta_kelas_rollback_set);

			if ($index_proses < $jumlah_rollback) {

                $peserta_kelas_rollback = $peserta_kelas_rollback_set[$index_proses];

				// Simpan id_reg_pd untuk keperluan update di langitan
				$nama_kelas_kuliah = $peserta_kelas_rollback['nama_kelas_kuliah'];
				$id_kelas_kuliah = $peserta_kelas_rollback['id_kelas_kuliah'];
				$id_reg_pd = $peserta_kelas_rollback['id_registrasi_mahasiswa'];
				$nim_mhs = $peserta_kelas_rollback['nim'];

				// Update ke Feeder Kuliah Mahasiswa
				$rollback_result = $this->feederws->DeletePesertaKelasKuliah(
					['id_registrasi_mahasiswa' => $id_reg_pd, 
                    'id_kelas_kuliah' => $id_kelas_kuliah]
				);

				// Jika tidak ada masalah update
				if ($rollback_result['error_code'] == 0) {
                    // $rollback_result[];
					$result['message'] = ($index_proses + 1) . "Rollback Nama Kelas {$nama_kelas_kuliah}, {$nim_mhs} : Berhasil";

                    // Update status sync
					$this->rdb->Query("UPDATE pengambilan_mk 
					SET fd_id_kls = null, 
						fd_id_reg_pd = null, 
						fd_sync_on = null 
					WHERE fd_id_reg_pd = '{$id_reg_pd}' AND fd_id_kls = '{$id_kelas_kuliah}'");

				} else {
					$result['message'] = ($index_proses + 1) . " Rollback {$nim_mhs} : Gagal. ";
					$result['message'] .= "({$rollback_result['error_code']}) {$rollback_result['error_desc']}";
				}

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Selesai
			// --------------------------------
			else {
				$result['message'] = "Selesai";
				$result['status'] = SYNC_STATUS_DONE;
			}
		}

		echo json_encode($result);
	}

	/**
	 * Ajax-GET /rollback/ambil_kelas/ Ambil kelas untuk nilai perkuliahan
	 * @param string $kode_prodi Kode program studi versi Feeder
	 * @param string $id_smt Kode semester versi Feeder
	 */
	function ambil_kelas($kode_prodi, $id_smt)
	{
		// Konversi format semester ke id_semester
		$sql_semester_raw = file_get_contents(APPPATH . 'models/sql/semester-konversi.sql');
		$sql_semester = strtr($sql_semester_raw, array('@npsn' => $this->satuan_pendidikan['npsn'], '@id_smt' => $id_smt));
		$semester_langitan = $this->rdb->QueryToArray($sql_semester);
		$id_semester = $semester_langitan[0]['ID_SEMESTER'];

		// Ambil data kelas yang ada di langitan yg sudah ter-sync (sudah ada fd_id_kls di kelas_mk)
		$sql_ambil_kelas_raw = file_get_contents(APPPATH . 'models/sql/ambil-kelas-kuliah.sql');
		$sql_ambil_kelas = strtr($sql_ambil_kelas_raw, array('@npsn' => $this->satuan_pendidikan['npsn'], '@kode_prodi' => $kode_prodi, '@smt' => $id_semester));
		$data_set = $this->rdb->QueryToArray($sql_ambil_kelas);

		echo json_encode($data_set);
	}


	/**
	 * GET /rollback/kuliah_mahasiswa/
	 */
	function kuliah_mahasiswa()
	{
		$jumlah = array();

		// Ambil jumlah kuliah mahasiswa di feeder. Sudah obsolete.
		$jumlah['feeder'] = '-';

		$this->smarty->assign('jumlah', $jumlah);
		// Data program studi
		$this->smarty->assign('program_studi_set', $this->langitan_model->list_program_studi($this->satuan_pendidikan['npsn']));

		$this->smarty->assign('url_rollback', site_url('rollback/start/' . $this->uri->segment(2)));
		$this->smarty->display('rollback/' . $this->uri->segment(2) . '.tpl');
	}

	/**
	 * Ajax-GET /rollback/kuliah_mahasiswa_data/
	 */
	function kuliah_mahasiswa_data($id_prodi, $id_smt)
	{
		// Ambil jumlah kuliah mahasiswa di feeder
		$jumlah['feeder'] = '-';

        $filter = "id_semester='{$id_smt}' AND id_prodi='{$id_prodi}'";
		$kuliah_mahasiswa_set = $this->feederws->GetListPerkuliahanMahasiswa($filter);

		$jumlah['feeder'] = count($kuliah_mahasiswa_set['data']);
		$jumlah['data'] = $kuliah_mahasiswa_set['data'];

		echo json_encode($jumlah);
	}

	private function proses_kuliah_mahasiswa()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('rollback/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

        if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			$id_smt = $this->input->post('semester');
			$id_prodi = $this->input->post('kode_prodi');

            $filter = "id_semester='{$id_smt}' AND id_prodi='{$id_prodi}'";
            $kuliah_mahasiswa_set = $this->feederws->GetListPerkuliahanMahasiswa($filter);

			// simpan ke cache
			$this->session->set_userdata('kuliah_mahasiswa_rollback_set', $kuliah_mahasiswa_set["data"]);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Rollback. Jumlah data: ' . count($kuliah_mahasiswa_set['data']);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		}
		// ----------------------------------------------
		// Proses Sinkronisasi dari data yg sudah diambil
		// ----------------------------------------------
		else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$kuliah_mahasiswa_rollback_set = $this->session->userdata('kuliah_mahasiswa_rollback_set');
			$jumlah_rollback = count($kuliah_mahasiswa_rollback_set);

			if ($index_proses < $jumlah_rollback) {

                $kuliah_mahasiswa_rollback = $kuliah_mahasiswa_rollback_set[$index_proses];

				// Simpan id_reg_pd untuk keperluan update di langitan
				$id_smt = $kuliah_mahasiswa_rollback['id_semester'];
				$id_reg_pd = $kuliah_mahasiswa_rollback['id_registrasi_mahasiswa'];
				$nim_mhs = $kuliah_mahasiswa_rollback['nim'];

				// Update ke Feeder Kuliah Mahasiswa
				$rollback_result = $this->feederws->DeletePerkuliahanMahasiswa(
					['id_registrasi_mahasiswa' => $id_reg_pd, 
                    'id_semester' => $id_smt]
				);

				// Jika tidak ada masalah update
				if ($rollback_result['error_code'] == 0) {
                    // $rollback_result[];

					$this->rdb->Query("UPDATE mahasiswa_status 
                        SET fd_sync_on = null, fd_id_smt = null, fd_id_reg_pd = null 
                        WHERE fd_id_reg_pd = '{$id_reg_pd}' 
                        AND fd_id_smt = '{$id_smt}'");
                    $result['message'] = ($index_proses + 1) . "Semester {$id_smt}, Rollback {$nim_mhs} : Berhasil";
				} else {
					$result['message'] = ($index_proses + 1) . " Rollback {$nim_mhs} : Gagal. ";
					$result['message'] .= "({$rollback_result['error_code']}) {$rollback_result['error_desc']}";
				}

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Selesai
			// --------------------------------
			else {
				$result['message'] = "Selesai";
				$result['status'] = SYNC_STATUS_DONE;
			}
		}

		echo json_encode($result);
	}

    public function nilai_transfer()
	{
		$jumlah['feeder'] = '-';

		$this->smarty->assign('jumlah', $jumlah);
        // Data program studi
        $this->smarty->assign('program_studi_set', $this->langitan_model->list_program_studi($this->satuan_pendidikan['npsn']));
		$this->smarty->assign('url_rollback', site_url('rollback/start/' . $this->uri->segment(2)));
		$this->smarty->display('rollback/' . $this->uri->segment(2) . '.tpl');
	}

    function nilai_transfer_data($id_prodi, $id_smt)
	{

		// Ambil peserta kelas di feeder. Sudah obsolete.
		$jumlah['feeder'] = '-';

        $filter = "id_semester='{$id_smt}' AND id_prodi='{$id_prodi}'";
		$nilai_transfer_set = $this->feederws->GetNilaiTransferPendidikanMahasiswa($filter);

		$jumlah['feeder'] = count($nilai_transfer_set['data']);
		echo json_encode($jumlah);
	}

    private function proses_nilai_transfer()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

        
        if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			$id_smt = $this->input->post('semester');
			$id_prodi = $this->input->post('kode_prodi');

            $filter = "id_semester='{$id_smt}' AND id_prodi='{$id_prodi}'";
            $kuliah_mahasiswa_set = $this->feederws->GetListPerkuliahanMahasiswa($filter);

			// simpan ke cache
			$this->session->set_userdata('kuliah_mahasiswa_rollback_set', $kuliah_mahasiswa_set["data"]);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Rollback. Jumlah data: ' . count($kuliah_mahasiswa_set['data']);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		}
		// ----------------------------------------------
		// Proses Sinkronisasi dari data yg sudah diambil
		// ----------------------------------------------
		else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$kuliah_mahasiswa_rollback_set = $this->session->userdata('kuliah_mahasiswa_rollback_set');
			$jumlah_rollback = count($kuliah_mahasiswa_rollback_set);

			if ($index_proses < $jumlah_rollback) {

                $kuliah_mahasiswa_rollback = $kuliah_mahasiswa_rollback_set[$index_proses];

				// Simpan id_reg_pd untuk keperluan update di langitan
				$id_smt = $kuliah_mahasiswa_rollback['id_semester'];
				$id_reg_pd = $kuliah_mahasiswa_rollback['id_registrasi_mahasiswa'];
				$nim_mhs = $kuliah_mahasiswa_rollback['nim'];

				// Update ke Feeder Kuliah Mahasiswa
				$rollback_result = $this->feederws->DeletePerkuliahanMahasiswa(
					['id_registrasi_mahasiswa' => $id_reg_pd, 
                    'id_semester' => $id_smt]
				);

				// Jika tidak ada masalah update
				if ($rollback_result['error_code'] == 0) {
                    // $rollback_result[];

                    $this->rdb->Query("UPDATE pengambilan_mk_konversi 
                    SET fd_id_ekuivalensi = null, 
                    fd_sync_on = null 
                    WHERE fd_id_ekuivalensi = {$id_pengambilan_mk}");

                    $result['message'] = ($index_proses + 1) . "Semester {$id_smt}, Rollback {$nim_mhs} : Berhasil";

				} else {
					$result['message'] = ($index_proses + 1) . " Rollback {$nim_mhs} : Gagal. ";
					$result['message'] .= "({$rollback_result['error_code']}) {$rollback_result['error_desc']}";
				}

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Selesai
			// --------------------------------
			else {
				$result['message'] = "Selesai";
				$result['status'] = SYNC_STATUS_DONE;
			}
		}

		echo json_encode($result);
	}

	/**
	 * Tampilan awal sebelum start sinkronisasi
	 */
	function start($mode)
	{
        if ($mode == 'riwayat_pendidikan_mahasiswa') {
			$kode_prodi	= $this->input->post('kode_prodi');
			$angkatan	= $this->input->post('angkatan');

			$program_studi_set = $this->rdb->QueryToArray(
				"SELECT nm_jenjang, nm_program_studi FROM program_studi ps
				JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
				JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
				WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' 
				AND ps.fd_id_sms = '{$kode_prodi}'
				AND ps.STATUS_AKTIF_PRODI=1"
			);

			$this->smarty->assign('jenis_sinkronisasi', 'Mahasiswa ' . $program_studi_set[0]['NM_JENJANG'] . ' ' . $program_studi_set[0]['NM_PROGRAM_STUDI'] . ' Angkatan ' . $angkatan);
			$this->smarty->assign('url', site_url('rollback/proses/' . $mode));
		}

		if ($mode == 'mata_kuliah') {
			$kode_prodi	= $this->input->post('kode_prodi');

			$program_studi_set = $this->rdb->QueryToArray(
				"SELECT nm_jenjang, nm_program_studi FROM program_studi ps
				JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
				JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
				WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' 
				AND ps.kode_program_studi = '{$kode_prodi}'
				AND ps.STATUS_AKTIF_PRODI=1"
			);

			$this->smarty->assign('jenis_sinkronisasi', 'Mata Kuliah ' . $program_studi_set[0]['NM_JENJANG'] . ' ' . $program_studi_set[0]['NM_PROGRAM_STUDI']);
			$this->smarty->assign('url', site_url('rollback/proses/' . $mode));
		}

		if ($mode == 'kurikulum') {
			$kode_prodi	= $this->input->post('kode_prodi');

			$program_studi_set = $this->rdb->QueryToArray(
				"SELECT nm_jenjang, nm_program_studi FROM program_studi ps
				JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
				JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
				WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' 
				AND ps.kode_program_studi = '{$kode_prodi}'
				AND ps.STATUS_AKTIF_PRODI=1"
			);

			$this->smarty->assign('jenis_sinkronisasi', 'Kurikulum ' . $program_studi_set[0]['NM_JENJANG'] . ' ' . $program_studi_set[0]['NM_PROGRAM_STUDI']);
			$this->smarty->assign('url', site_url('rollback/proses/' . $mode));
		}

		if ($mode == 'mata_kuliah_kurikulum') {
			$kode_prodi			= $this->input->post('kode_prodi');
			$id_kurikulum_prodi	= $this->input->post('id_kurikulum');

			$program_studi_set = $this->rdb->QueryToArray(
				"SELECT j.nm_jenjang, ps.nm_program_studi, k.nm_kurikulum
				FROM kurikulum_mk kmk
				JOIN kurikulum k ON k.id_kurikulum = kmk.id_kurikulum
				JOIN program_studi ps ON ps.id_program_studi = kmk.id_program_studi
				JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
				JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
				WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' 
				AND ps.kode_program_studi = '{$kode_prodi}' 
				AND kmk.id_kurikulum = {$id_kurikulum_prodi}
				AND ps.STATUS_AKTIF_PRODI=1"
			);

			$this->smarty->assign('jenis_sinkronisasi', 'Mata Kuliah Kurikulum ' . $program_studi_set[0]['NM_KURIKULUM']);
			$this->smarty->assign('url', site_url('rollback/proses/' . $mode));
		}

		if ($mode == 'kelas_kuliah') {
			$kode_prodi	= $this->input->post('kode_prodi');
			$id_smt		= $this->input->post('semester');

			$program_studi_set = $this->rdb->QueryToArray(
				"SELECT nm_jenjang, nm_program_studi FROM program_studi ps
				JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
				JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
				WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' 
				AND ps.fd_id_sms = '{$kode_prodi}'
				AND STATUS_AKTIF_PRODI=1"
			);

			// Konversi format semester ke id_semester
			$semester_langitan = $this->rdb->QueryToArray(
				"SELECT thn_akademik_semester as thn, nm_semester FROM semester s
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = s.id_perguruan_tinggi
				WHERE 
					pt.npsn = '{$this->satuan_pendidikan['npsn']}' AND 
					s.fd_id_smt = '{$id_smt}'"
			);

			$this->smarty->assign('jenis_sinkronisasi', 'Kelas Perkuliahan ' . $program_studi_set[0]['NM_JENJANG'] . ' ' . $program_studi_set[0]['NM_PROGRAM_STUDI'] . ' - ' . $semester_langitan[0]['THN'] . '/' . ($semester_langitan[0]['THN'] + 1) . ' ' . $semester_langitan[0]['NM_SEMESTER']);
			$this->smarty->assign('url', site_url('rollback/proses/' . $mode));
		}

		if ($mode == 'ajar_dosen') {
			$kode_prodi	= $this->input->post('kode_prodi');
			$id_smt		= $this->input->post('semester');

			$program_studi_set = $this->rdb->QueryToArray(
				"SELECT nm_jenjang, nm_program_studi FROM program_studi ps
				JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
				JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
				WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' 
				AND ps.fd_id_sms = '{$kode_prodi}'
				AND STATUS_AKTIF_PRODI=1"
			);

			// Konversi format semester ke id_semester
			$semester_langitan = $this->rdb->QueryToArray(
				"SELECT thn_akademik_semester as thn, nm_semester FROM semester s
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = s.id_perguruan_tinggi
				WHERE 
					pt.npsn = '{$this->satuan_pendidikan['npsn']}' AND 
					s.fd_id_smt = '{$id_smt}'"
			);

			$this->smarty->assign('jenis_sinkronisasi', 'Dosen Kelas ' . $program_studi_set[0]['NM_JENJANG'] . ' ' . $program_studi_set[0]['NM_PROGRAM_STUDI'] . ' - ' . $semester_langitan[0]['THN'] . '/' . ($semester_langitan[0]['THN'] + 1) . ' ' . $semester_langitan[0]['NM_SEMESTER']);
			$this->smarty->assign('url', site_url('rollback/proses/' . $mode));
		}

		if ($mode == 'nilai_per_prodi') {
			$kode_prodi		= $this->input->post('kode_prodi');
			$id_smt			= $this->input->post('semester');

			$program_studi_set = $this->rdb->QueryToArray(
				"SELECT nm_jenjang, nm_program_studi FROM program_studi ps
				JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
				JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
				WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' 
				AND ps.fd_id_sms = '{$kode_prodi}'
				AND ps.STATUS_AKTIF_PRODI=1"
			);

			// Konversi format semester ke id_semester
			$semester_langitan = $this->rdb->QueryToArray(
				"SELECT thn_akademik_semester as thn, nm_semester FROM semester s
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = s.id_perguruan_tinggi
				WHERE 
					pt.npsn = '{$this->satuan_pendidikan['npsn']}' AND 
					s.fd_id_smt = '{$id_smt}'"
			);

			$this->smarty->assign('jenis_sinkronisasi', 'Nilai Perkuliahan ' . $program_studi_set[0]['NM_JENJANG'] . ' ' . $program_studi_set[0]['NM_PROGRAM_STUDI'] . ' - ' . $semester_langitan[0]['THN'] . '/' . ($semester_langitan[0]['THN'] + 1) . ' ' . $semester_langitan[0]['NM_SEMESTER']);
			$this->smarty->assign('url', site_url('rollback/proses/' . $mode));
		}

        if ($mode == 'nilai_transfer') {
            $kode_prodi		= $this->input->post('kode_prodi');
			$id_smt			= $this->input->post('semester');

			$program_studi_set = $this->rdb->QueryToArray(
				"SELECT nm_jenjang, nm_program_studi FROM program_studi ps
				JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
				JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
				WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' 
				AND ps.fd_id_sms = '{$kode_prodi}'
				AND ps.STATUS_AKTIF_PRODI=1"
			);

			// Konversi format semester ke id_semester
			$semester_langitan = $this->rdb->QueryToArray(
				"SELECT thn_akademik_semester as thn, nm_semester FROM semester s
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = s.id_perguruan_tinggi
				WHERE 
					pt.npsn = '{$this->satuan_pendidikan['npsn']}' AND 
					s.fd_id_smt = '{$id_smt}'"
			);
			$this->smarty->assign('jenis_sinkronisasi', 'Nilai Transfer Mahasiswa');
			$this->smarty->assign('url', site_url('rollback/proses/' . $mode));
		}

		if ($mode == 'kuliah_mahasiswa') {
			$id_smt			= $this->input->post('semester');
			$kode_prodi			= $this->input->post('kode_prodi');

			$semester_langitan = $this->langitan_model->get_semester_langitan($this->satuan_pendidikan['npsn'], $id_smt);

			$program_studi_set = $this->rdb->QueryToArray(
				"SELECT nm_jenjang, nm_program_studi FROM program_studi ps
				JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
				JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
				WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' 
				AND ps.fd_id_sms = '{$kode_prodi}'
				AND ps.STATUS_AKTIF_PRODI=1"
			);

			$this->smarty->assign('jenis_sinkronisasi', 'Aktivitas Mahasiswa - Program Studi : ' . $program_studi_set[0]['NM_JENJANG'] . ' ' . $program_studi_set[0]['NM_PROGRAM_STUDI'] . ' Semester : ' . $semester_langitan);
			$this->smarty->assign('url', site_url('rollback/proses/' . $mode));
		}
		$this->smarty->assign('mode', $mode);
		$this->smarty->display('rollback/start.tpl');
	}

	/**
	 * Pemrosesan sinkronisasi
	 */
	function proses($mode)
	{
		// harus request POST 
		if ($_SERVER['REQUEST_METHOD'] != 'POST') {
			return;
		}

        if ($mode == 'riwayat_pendidikan_mahasiswa') {
			$this->proses_riwayat_pendidikan_mahasiswa();
		} else if ($mode == 'mata_kuliah') {
			$this->proses_mata_kuliah();
		} else if ($mode == 'kurikulum') {
			$this->proses_kurikulum();
		} else if ($mode == 'mata_kuliah_kurikulum') {
			$this->proses_mata_kuliah_kurikulum();
		} else if ($mode == 'kelas_kuliah') {
			$this->proses_kelas_kuliah();
		} else if ($mode == 'ajar_dosen') {
			$this->proses_ajar_dosen();
		} else if ($mode == 'nilai_per_prodi') {
			$this->proses_nilai_per_prodi();
		}  else if ($mode == 'nilai_transfer') {
			$this->proses_nilai_transfer();
		} else if ($mode == 'kuliah_mahasiswa') {
			$this->proses_kuliah_mahasiswa();
		}else {
			echo json_encode(array('status' => 'done', 'message' => 'Not Implemented()'));
		}
	}
}
