<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property Remotedb $rdb Remote DB Sistem Langitan
 * @property string $token Token webservice
 * @property string $npsn Kode Perguruan Tinggi
 * @property array $satuan_pendidikan Row: satuan_pendidikan
 * @property Langitan_model $langitan_model
 * @property PerguruanTinggi_model $pt
 * @property Feederws $feederws
 */
class Sync extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->check_credentials();

		// Inisialisasi Token dan Satuan Pendidikan
		$this->token = $this->session->userdata('token');
		$this->satuan_pendidikan = $this->session->userdata(FEEDER_SATUAN_PENDIDIKAN);

		// Inisialisasi URL Feeder
		$this->load->library('feeder', array('url' => $this->session->userdata('wsdl')));

		// Inisialisasi URL Feeder WS 2
		$this->load->library('feederws', [
			'url' => $this->session->userdata('ws2url'),
			'token' => $this->session->userdata('token')
		]);

		// Inisialisasi Library RemoteDB
		$this->load->library('remotedb', NULL, 'rdb');
		$this->rdb->set_url($this->session->userdata('langitan'));

		// Inisialisasi Langitan_model
		$this->load->model('langitan_model');

		// Inisialisasi Perguruan Tinggi
		$this->pt = $this->session->userdata('pt');
	}

	/**
	 * GET /sync/mahasiswa
	 */
	function mahasiswa()
	{
		$jumlah = array();

		// Ambil jumlah mahasiswa di feeder (off 2022)
		$jumlah['feeder'] = '-';

		// Ambil data mahasiswa
		$sql_mahasiswa_raw = file_get_contents(APPPATH . 'models/sql/mahasiswa.sql');
		$sql_mahasiswa = strtr($sql_mahasiswa_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$mhs_set = $this->rdb->QueryToArray($sql_mahasiswa);

		$jumlah['langitan'] = $mhs_set[0]['JUMLAH'];
		$jumlah['linked'] = $mhs_set[1]['JUMLAH'];
		$jumlah['update'] = $mhs_set[2]['JUMLAH'];
		$jumlah['insert'] = $mhs_set[3]['JUMLAH'];

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		// Ambil Semua angkatan yg ada :
		$sql_angkatan_mhs_raw = file_get_contents(APPPATH . 'models/sql/mahasiswa-angkatan.sql');
		$sql_angkatan_mhs = strtr($sql_angkatan_mhs_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$angkatan_set = $this->rdb->QueryToArray($sql_angkatan_mhs);
		$this->smarty->assign('angkatan_set', $angkatan_set);

		$this->smarty->assign('url_sync', site_url('sync/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync/' . $this->uri->segment(2) . '.tpl');
	}

	/**
	 * Ajax-GET /sync/mahasiswa_data/
	 * @param string $kode_prodi Kode program studi versi Feeder
	 * @param int $angkatan Tahun angkatan mahasiswa
	 */
	function mahasiswa_data($kode_prodi, $angkatan)
	{
		// Khusus UMAHA menggunakan filter nim agar match
		if ($this->satuan_pendidikan['npsn'] == '071086') {
			// Ambil informasi format NIM 
			$format_set = $this->rdb->QueryToArray(
				"SELECT nm_program_studi, coalesce(f.format_nim_fakultas, format_nim_pt) as format_nim, f.kode_nim_fakultas, ps.kode_nim_prodi
				FROM perguruan_tinggi pt 
				JOIN fakultas f ON f.id_perguruan_tinggi = pt.id_perguruan_tinggi
				JOIN program_studi ps ON ps.id_fakultas = f.id_fakultas
				WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' and ps.kode_program_studi = '{$kode_prodi}'"
			);

			$format = $format_set[0];

			$format_nim = str_replace('[F]', $format['KODE_NIM_FAKULTAS'], $format['FORMAT_NIM']);
			$format_nim = str_replace('[PS]', $format['KODE_NIM_PRODI'], $format_nim);
			$format_nim = str_replace('[A]', substr($angkatan, -2), $format_nim);
			$format_nim = str_replace('[Seri]', '___', $format_nim);

			// Jika FIKES 2014 kebawah, pakai format lama
			if ($kode_prodi == '13453' && $angkatan <= 2014) {
				$format_nim = substr($angkatan, -2) . '___';
			}

			$jumlah['feeder'] = '-';
		} else {
			$jumlah['feeder'] = '-';
		}

		// SQL jumlah mahasiswa di Sistem Langitan & yg sudah link
		$sql_mahasiswa_data_raw = file_get_contents(APPPATH . 'models/sql/mahasiswa-data.sql');
		$sql_mahasiswa_data = strtr($sql_mahasiswa_data_raw, array(
			'@npsn' => $this->satuan_pendidikan['npsn'],
			'@kode_prodi' => $kode_prodi,
			'@angkatan' => $angkatan
		));

		// Ambil jumlah mahasiswa di Sistem Langitan & yg sudah link
		$mhs_set = $this->rdb->QueryToArray($sql_mahasiswa_data);

		$jumlah['langitan'] = $mhs_set[0]['JUMLAH'];
		$jumlah['linked'] = $mhs_set[1]['JUMLAH'];
		$jumlah['update'] = $mhs_set[2]['JUMLAH'];
		$jumlah['insert'] = $mhs_set[3]['JUMLAH'];

		echo json_encode($jumlah);
	}

	private function proses_mahasiswa()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		// -----------------------------------
		// Ambil data untuk Insert
		// -----------------------------------
		if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			// Filter Prodi & Angkatan
			$kode_prodi = $this->input->post('kode_prodi');
			$angkatan	= $this->input->post('angkatan');

			// Mendapatkan id_sms
			//$response = $this->feeder->GetRecord($this->token, FEEDER_SMS, "id_sp = '{$this->satuan_pendidikan['id_sp']}' AND trim(kode_prodi) = '{$kode_prodi}'");
			//$sms = $response['result'];

			// Ambil mahasiswa yg akan insert
			$sql_mahasiswa_insert = file_get_contents(APPPATH . 'models/sql/mahasiswa-insert.sql');
			$sql_mahasiswa_insert = strtr($sql_mahasiswa_insert, array(
				'@id_sp'		=> $this->satuan_pendidikan['id_perguruan_tinggi'],
				'@npsn'			=> $this->satuan_pendidikan['npsn'],
				'@kode_prodi'	=> $kode_prodi,
				'@angkatan'		=> $angkatan
			));

			$mahasiswa_set = $this->rdb->QueryToArray($sql_mahasiswa_insert);

			// simpan ke cache
			$this->session->set_userdata('mahasiswa_insert_set', $mahasiswa_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Entri. Jumlah data: ' . count($mahasiswa_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_AMBIL_DATA_LANGITAN_2;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Ambil data untuk Update
		// -----------------------------------
		else if ($mode == MODE_AMBIL_DATA_LANGITAN_2) {
			// Filter Prodi & Angkatan
			$kode_prodi = $this->input->post('kode_prodi');
			$angkatan	= $this->input->post('angkatan');

			// Ambil mahasiswa yg akan UPDATE
			$sql_mahasiswa_update = file_get_contents(APPPATH . 'models/sql/mahasiswa-update.sql');
			$sql_mahasiswa_update = strtr($sql_mahasiswa_update, array(
				'@id_sp'		=> $this->satuan_pendidikan['id_perguruan_tinggi'],
				'@npsn'			=> $this->satuan_pendidikan['npsn'],
				'@kode_prodi'	=> $kode_prodi,
				'@angkatan'		=> $angkatan
			));

			$mahasiswa_set = $this->rdb->QueryToArray($sql_mahasiswa_update);

			// simpan ke cache
			$this->session->set_userdata('mahasiswa_update_set', $mahasiswa_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Update. Jumlah data: ' . count($mahasiswa_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		}
		// ----------------------------------------------
		// Proses Sinkronisasi dari data yg sudah diambil
		// ----------------------------------------------
		else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$mahasiswa_insert_set = $this->session->userdata('mahasiswa_insert_set');
			$jumlah_insert = count($mahasiswa_insert_set);

			// Ambil dari cache
			$mahasiswa_update_set = $this->session->userdata('mahasiswa_update_set');
			$jumlah_update = count($mahasiswa_update_set);

			// Waktu Sinkronisasi
			$time_sync = date('Y-m-d H:i:s');

			// --------------------------------
			// Proses Insert
			// --------------------------------
			if ($index_proses < $jumlah_insert) {
				// Proses dalam bentuk key lowercase
				$mahasiswa_insert = array_change_key_case($mahasiswa_insert_set[$index_proses], CASE_LOWER);

				// Simpan id_mhs untuk update data di langitan
				$id_mhs			= $mahasiswa_insert['id_mhs'];
				$id_pengguna	= $mahasiswa_insert['id_pengguna'];
				$fd_id_pd_lama = $mahasiswa_insert['fd_id_pd_lama'];

				// Hilangkan id_mhs
				unset($mahasiswa_insert['id_mhs']);
				unset($mahasiswa_insert['id_pengguna']);
				unset($mahasiswa_insert['fd_id_pd_lama']);

				// Cleansing data
				if ($mahasiswa_insert['jenis_kelamin'] == '*') unset($mahasiswa_insert['jk']);
				if ($mahasiswa_insert['kode_pos'] == '') unset($mahasiswa_insert['kode_pos']);
				if (!filter_var($mahasiswa_insert['email'], FILTER_VALIDATE_EMAIL)) unset($mahasiswa_insert['email']);
				if ($mahasiswa_insert['rt'] == '') unset($mahasiswa_insert['rt']);
				if ($mahasiswa_insert['rw'] == '') unset($mahasiswa_insert['rw']);
				if ($mahasiswa_insert['id_jns_tinggal'] == '') unset($mahasiswa_insert['id_jns_tinggal']);
				if ($mahasiswa_insert['id_alat_transport'] == '') unset($mahasiswa_insert['id_alat_transport']);
				if ($mahasiswa_insert['tanggal_lahir_ayah'] == '') unset($mahasiswa_insert['tgl_lahir_ayah']);
				if ($mahasiswa_insert['id_jenjang_pendidikan_ayah'] == '') unset($mahasiswa_insert['id_jenjang_pendidikan_ayah']);
				if ($mahasiswa_insert['id_pekerjaan_ayah'] == '') unset($mahasiswa_insert['id_pekerjaan_ayah']);
				if ($mahasiswa_insert['id_penghasilan_ayah'] == '') unset($mahasiswa_insert['id_penghasilan_ayah']);
				if ($mahasiswa_insert['tanggal_lahir_ibu'] == '') unset($mahasiswa_insert['tgl_lahir_ibu']);
				if ($mahasiswa_insert['id_jenjang_pendidikan_ibu'] == '') unset($mahasiswa_insert['id_jenjang_pendidikan_ibu']);
				if ($mahasiswa_insert['id_pekerjaan_ibu'] == '') unset($mahasiswa_insert['id_pekerjaan_ibu']);
				if ($mahasiswa_insert['id_penghasilan_ibu'] == '') unset($mahasiswa_insert['id_penghasilan_ibu']);
				if ($mahasiswa_insert['tanggal_lahir_wali'] == '') unset($mahasiswa_insert['tgl_lahir_wali']);
				if ($mahasiswa_insert['id_jenjang_pendidikan_wali'] == '') unset($mahasiswa_insert['id_jenjang_pendidikan_wali']);
				if ($mahasiswa_insert['id_pekerjaan_wali'] == '') unset($mahasiswa_insert['id_pekerjaan_wali']);
				if ($mahasiswa_insert['id_penghasilan_wali'] == '') unset($mahasiswa_insert['id_penghasilan_wali']);


				// Entri ke Feeder Mahasiswa WS2
				$insert_result = $this->feederws->InsertBiodataMahasiswa($mahasiswa_insert);

				// Jika berhasil insert, terdapat return data.id_mahasiswa
				if (isset($insert_result['data']['id_mahasiswa'])) {
					// FK id_pd
					$id_mahasiswa = $insert_result['data']['id_mahasiswa'];

					// Pesan Insert, nipd (nim) mengambil dari mahasiswa_pt_insert
					$result['message'] = ($index_proses + 1) . " Insert {$mahasiswa_insert['nama_mahasiswa']} : Berhasil";

					// status sandbox
					$is_sandbox = ($this->session->userdata('is_sandbox') == TRUE) ? '1' : '0';

					// Melakukan update ke DB Langitan id_pd dan id_reg_pd hasil insert
					$this->rdb->Query("UPDATE pengguna SET fd_id_pd = '{$id_mahasiswa}', fd_sync_on = to_date('{$time_sync}', 'YYYY-MM-DD HH24:MI:SS') WHERE id_pengguna = {$id_pengguna}");
				} else // Saat insert mahasiswa Gagal
				{
					$error_desc = "Mahasiswa dengan nama, tempat, tanggal lahir dan ibu kandung yang sama sudah ada";
					if (
						isset($insert_result["error_code"]) &&
						($insert_result["error_code"] == 1209 || //jika nik sudah ada
							$insert_result["error_code"] == 200 || // (code) jika nama,tanggal lahir, nama ibu sudah ada
							$insert_result["error_desc"] == $error_desc) // jika nama,tanggal lahir, nama ibu sudah ada
					) {

						//ambil tanggal lahir dan sesuaikan format yg ada di feeder
						$explode_tgl_lahir = explode("-", $mahasiswa_insert["tanggal_lahir"]);
						$tgl_lahir = $explode_tgl_lahir[2] . "-" . $explode_tgl_lahir[1] . "-" . $explode_tgl_lahir[0];

						$nama_mahasiswa = trim(str_replace('\'', '\'\'', $mahasiswa_insert["nama_mahasiswa"]));

						// get mahasiswa berdasarkan nik dan tgl lahir
						$listMahasiswa = $this->feederws->GetBiodataMahasiswa($nama_mahasiswa, trim($mahasiswa_insert["nama_ibu_kandung"]), $tgl_lahir);

						//jika ada error
						if ($listMahasiswa["data"] == '') {
							$result['message'] = ($index_proses + 1) . " Insert {$mahasiswa_insert['nama_mahasiswa']} : Gagal " . json_encode($mahasiswa_insert);
						} else if (count($listMahasiswa["data"]) > 0) {

							$id_mahasiswa = $listMahasiswa["data"][0]["id_mahasiswa"];

							$this->rdb->Query("UPDATE pengguna SET fd_id_pd = '{$id_mahasiswa}', fd_sync_on = to_date('{$time_sync}', 'YYYY-MM-DD HH24:MI:SS') WHERE id_pengguna = {$id_pengguna}");

							$result['message'] = ($index_proses + 1) . " Insert {$mahasiswa_insert['nama_mahasiswa']} : Update id_mahasiswa ketika nama sudah ada : Berhasil. ";
						} else {
							$result['message'] = ($index_proses + 1) . " Insert {$mahasiswa_insert['nama_mahasiswa']} : Gagal. Data di feeder tidak ada, coba cek di daftar mahasiswa hapus dan kemudian restore";
							$result['message'] .= "\n" . json_encode($mahasiswa_insert);
						}
					} else {
						// Pesan Insert, nipd mengambil dari mahasiswa_pt_insert
						$result['message'] = ($index_proses + 1) . " Insert {$mahasiswa_insert['nama_mahasiswa']} : Gagal. " . json_encode($insert_result);
					}
				}


				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Proses Update
			// --------------------------------
			else if ($index_proses < ($jumlah_insert + $jumlah_update)) {
				// index berjalan dikurangi jumlah data insert utk mendapatkan index update
				$index_proses -= $jumlah_insert;

				// Proses dalam bentuk key lowercase
				$mahasiswa_update = array_change_key_case($mahasiswa_update_set[$index_proses], CASE_LOWER);

				// Simpan id_mhs untuk update data di langitan
				$id_mhs		= $mahasiswa_update['id_mhs'];
				$id_pd		= $mahasiswa_update['id_pd'];

				// Hilangkan id_mhs & id_pd & id_reg_pd
				unset($mahasiswa_update['id_mhs']);
				unset($mahasiswa_update['id_pd']);

				// --------------------
				// Cleansing data
				// --------------------
				if (!filter_var($mahasiswa_update['email'], FILTER_VALIDATE_EMAIL)) unset($mahasiswa_update['email']);


				// Update ke Feeder Mahasiswa menggunakan Webservice 2
				$update_result = $this->feederws->UpdateBiodataMahasiswa(['id_mahasiswa' => $id_pd], $mahasiswa_update);

				// Jika tidak ada masalah update
				if ($update_result['error_code'] == 0) {
					$result['message'] = ($index_proses + 1) . " Update {$mahasiswa_update['nik']} : Berhasil";

					$this->rdb->Query("UPDATE pengguna SET fd_sync_on = to_date('{$time_sync}','YYYY-MM-DD HH24:MI:SS') WHERE id_mhs = {$id_mhs}");
				}
				// Jika terdapat masalah update
				else {

					$result['message'] = ($index_proses + 1) . " Update {$mahasiswa_update['nik']} : Gagal update mahasiswa";
					$result['message'] .= "\n" . json_encode($update_result);
					$result['message'] .= "\n" . json_encode($mahasiswa_update);
				}

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// meneruskan index proses ditambah lagi dengan jumlah data insert
				$index_proses += $jumlah_insert;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Selesai
			// --------------------------------
			else {
				$result['message'] = "Selesai";
				$result['status'] = SYNC_STATUS_DONE;
			}
		}

		echo json_encode($result);
	}

	function riwayat_pendidikan_mahasiswa()
	{
		$jumlah = array();

		// Ambil jumlah mahasiswa di feeder (off 2022)
		$jumlah['feeder'] = '-';

		// Ambil data mahasiswa
		$sql_mahasiswa_raw = file_get_contents(APPPATH . 'models/sql/mahasiswa-pt.sql');
		$sql_mahasiswa = strtr($sql_mahasiswa_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$mhs_set = $this->rdb->QueryToArray($sql_mahasiswa);

		$jumlah['langitan'] = $mhs_set[0]['JUMLAH'];
		$jumlah['linked'] = $mhs_set[1]['JUMLAH'];
		$jumlah['update'] = $mhs_set[2]['JUMLAH'];
		$jumlah['insert'] = $mhs_set[3]['JUMLAH'];

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		// Ambil Semua angkatan yg ada :
		$sql_angkatan_mhs_raw = file_get_contents(APPPATH . 'models/sql/mahasiswa-angkatan.sql');
		$sql_angkatan_mhs = strtr($sql_angkatan_mhs_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$angkatan_set = $this->rdb->QueryToArray($sql_angkatan_mhs);
		$this->smarty->assign('angkatan_set', $angkatan_set);

		$this->smarty->assign('url_sync', site_url('sync/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync/' . $this->uri->segment(2) . '.tpl');
	}

	function riwayat_pendidikan_mahasiswa_data($kode_prodi, $angkatan)
	{
		// Khusus UMAHA menggunakan filter nim agar match
		if ($this->satuan_pendidikan['npsn'] == '071086') {
			// Ambil informasi format NIM 
			$format_set = $this->rdb->QueryToArray(
				"SELECT nm_program_studi, coalesce(f.format_nim_fakultas, format_nim_pt) as format_nim, f.kode_nim_fakultas, ps.kode_nim_prodi
				FROM perguruan_tinggi pt 
				JOIN fakultas f ON f.id_perguruan_tinggi = pt.id_perguruan_tinggi
				JOIN program_studi ps ON ps.id_fakultas = f.id_fakultas
				WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' and ps.kode_program_studi = '{$kode_prodi}'"
			);

			$format = $format_set[0];

			$format_nim = str_replace('[F]', $format['KODE_NIM_FAKULTAS'], $format['FORMAT_NIM']);
			$format_nim = str_replace('[PS]', $format['KODE_NIM_PRODI'], $format_nim);
			$format_nim = str_replace('[A]', substr($angkatan, -2), $format_nim);
			$format_nim = str_replace('[Seri]', '___', $format_nim);

			// Jika FIKES 2014 kebawah, pakai format lama
			if ($kode_prodi == '13453' && $angkatan <= 2014) {
				$format_nim = substr($angkatan, -2) . '___';
			}

			$jumlah['feeder'] = '-';
		} else {
			$jumlah['feeder'] = '-';
		}

		// SQL jumlah mahasiswa di Sistem Langitan & yg sudah link
		$sql_mahasiswa_data_raw = file_get_contents(APPPATH . 'models/sql/mahasiswa-pt-data.sql');
		$sql_mahasiswa_data = strtr($sql_mahasiswa_data_raw, array(
			'@npsn' => $this->satuan_pendidikan['npsn'],
			'@kode_prodi' => $kode_prodi,
			'@angkatan' => $angkatan
		));

		// Ambil jumlah mahasiswa di Sistem Langitan & yg sudah link
		$mhs_set = $this->rdb->QueryToArray($sql_mahasiswa_data);

		$jumlah['langitan'] = $mhs_set[0]['JUMLAH'];
		$jumlah['linked'] = $mhs_set[1]['JUMLAH'];
		$jumlah['update'] = $mhs_set[2]['JUMLAH'];
		$jumlah['insert'] = $mhs_set[3]['JUMLAH'];

		echo json_encode($jumlah);
	}

	private function proses_riwayat_pendidikan_mahasiswa()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		// -----------------------------------
		// Ambil data untuk Insert
		// -----------------------------------
		if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			// Filter Prodi & Angkatan
			$kode_prodi = $this->input->post('kode_prodi');
			$angkatan	= $this->input->post('angkatan');

			// Ambil mahasiswa_pt yg akan insert
			$sql_mahasiswa_pt_insert = file_get_contents(APPPATH . 'models/sql/mahasiswa-pt-insert.sql');
			$sql_mahasiswa_pt_insert = strtr($sql_mahasiswa_pt_insert, array(
				'@id_sp'		=> $this->satuan_pendidikan['id_perguruan_tinggi'],
				'@npsn'			=> $this->satuan_pendidikan['npsn'],
				'@kode_prodi'	=> $kode_prodi,
				'@angkatan'		=> $angkatan
			));

			$mahasiswa_pt_set = $this->rdb->QueryToArray($sql_mahasiswa_pt_insert);

			// simpan ke cache
			$this->session->set_userdata('mahasiswa_pt_insert_set', $mahasiswa_pt_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Entri. Jumlah data: ' . count($mahasiswa_pt_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_AMBIL_DATA_LANGITAN_2;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Ambil data untuk Update
		// -----------------------------------
		else if ($mode == MODE_AMBIL_DATA_LANGITAN_2) {
			// Filter Prodi & Angkatan
			$kode_prodi = $this->input->post('kode_prodi');
			$angkatan	= $this->input->post('angkatan');

			// Ambil mahasiswa_pt yg akan di update
			$sql_mahasiswa_pt_update = file_get_contents(APPPATH . 'models/sql/mahasiswa-pt-update.sql');
			$sql_mahasiswa_pt_update = strtr($sql_mahasiswa_pt_update, array(
				'@id_sp'		=> $this->satuan_pendidikan['id_perguruan_tinggi'],
				'@npsn'			=> $this->satuan_pendidikan['npsn'],
				'@kode_prodi'	=> $kode_prodi,
				'@angkatan'		=> $angkatan
			));

			$mahasiswa_pt_set = $this->rdb->QueryToArray($sql_mahasiswa_pt_update);

			// simpan ke cache
			$this->session->set_userdata('mahasiswa_pt_update_set', $mahasiswa_pt_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Update. Jumlah data: ' . count($mahasiswa_pt_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		}
		// ----------------------------------------------
		// Proses Sinkronisasi dari data yg sudah diambil
		// ----------------------------------------------
		else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$mahasiswa_pt_insert_set = $this->session->userdata('mahasiswa_pt_insert_set');
			$jumlah_insert = count($mahasiswa_pt_insert_set);

			// Ambil dari cache
			$mahasiswa_pt_update_set = $this->session->userdata('mahasiswa_pt_update_set');
			$jumlah_update = count($mahasiswa_pt_update_set);

			// Waktu Sinkronisasi
			$time_sync = date('Y-m-d H:i:s');

			// --------------------------------
			// Proses Insert
			// --------------------------------
			if ($index_proses < $jumlah_insert) {
				// Proses dalam bentuk key lowercase
				$mahasiswa_pt_insert = array_change_key_case($mahasiswa_pt_insert_set[$index_proses], CASE_LOWER);
				// Hilangkan id_mhs
				$id_mhs = $mahasiswa_pt_insert['id_mhs'];
				unset($mahasiswa_pt_insert['id_mhs']);

				// ---------------------------------------------------------------------------------------------
				// Khusus UMAHA : Angkatan dibawah 2014 di geser ke 2014 Ganjil sebagai mhs transfer, default SKS = 1
				// ---------------------------------------------------------------------------------------------
				if ($this->satuan_pendidikan['npsn'] == '071086' && (int)$mahasiswa_pt_insert['id_periode_masuk'] < 20141) {
					$mahasiswa_pt_insert['mulai_smt'] = '20141';			// 2014/2015 Ganjil
					$mahasiswa_pt_insert['tgl_masuk_sp'] = '2014-10-17';	// Tanggal SK Penggabungan Institusi
					$mahasiswa_pt_insert['id_jenis_daftar'] = '2';			// Transfer
					$mahasiswa_pt_insert['sks_diakui'] = '1';				// SKS Diakui min 1 utk bisa di sinkronisasi

					/**
					 * 1	TEKNIK INFORMATIKA	a9a267c7-63ff-4cbd-bcf6-b7f957fe1d80
					 * 1	TEKNIK INDUSTRI	a9fec275-28ab-4895-894a-b545b8f7193a
					 * 1	TEKNIK MESIN	944fc32c-18f3-47f9-bc19-6e711480c858
					 * 5	TEKNIK KOMPUTER	613ec101-53d0-405c-b6e1-c4125f042f52
					 * 1	MANAJEMEN	c9e0c8b8-91c0-427a-bace-7215337e6515
					 * 1	AKUNTANSI	f0def671-d948-4d68-ab3e-a7f936f7a3b4
					 * 5	AKUNTANSI	25b084b0-b377-4369-8a3b-c196a86246bb
					 * 1	ILMU HUKUM	96f4e4f6-adba-41b6-8d92-92369808ae44
					 * 5	ANALIS KESEHATAN	f9351bde-0387-4f6e-99c8-f38ec28543e4
					 */

					// Asal PT & Prodi
					if ($mahasiswa_pt_insert['id_sms'] == 'a9a267c7-63ff-4cbd-bcf6-b7f957fe1d80') {
						$mahasiswa_pt_insert['nm_pt_asal'] = 'Sekolah Tinggi Teknik YPM Sepanjang';
						$mahasiswa_pt_insert['nm_prodi_asal'] = 'Teknik Informatika';
					} else if ($mahasiswa_pt_insert['id_sms'] == 'a9fec275-28ab-4895-894a-b545b8f7193a') {
						$mahasiswa_pt_insert['nm_pt_asal'] = 'Sekolah Tinggi Teknik YPM Sepanjang';
						$mahasiswa_pt_insert['nm_prodi_asal'] = 'Teknik Industri';
					} else if ($mahasiswa_pt_insert['id_sms'] == '944fc32c-18f3-47f9-bc19-6e711480c858') {
						$mahasiswa_pt_insert['nm_pt_asal'] = 'Sekolah Tinggi Teknik YPM Sepanjang';
						$mahasiswa_pt_insert['nm_prodi_asal'] = 'Teknik Mesin';
					} else if ($mahasiswa_pt_insert['id_sms'] == '613ec101-53d0-405c-b6e1-c4125f042f52') {
						$mahasiswa_pt_insert['nm_pt_asal'] = 'Sekolah Tinggi Teknik YPM Sepanjang';
						$mahasiswa_pt_insert['nm_prodi_asal'] = 'Teknik Komputer';
					} else if ($mahasiswa_pt_insert['id_sms'] == 'c9e0c8b8-91c0-427a-bace-7215337e6515') {
						$mahasiswa_pt_insert['nm_pt_asal'] = 'Sekolah Tinggi Ilmu Ekonomi YPM Sepanjang';
						$mahasiswa_pt_insert['nm_prodi_asal'] = 'Manajemen';
					} else if ($mahasiswa_pt_insert['id_sms'] == 'f0def671-d948-4d68-ab3e-a7f936f7a3b4') {
						$mahasiswa_pt_insert['nm_pt_asal'] = 'Sekolah Tinggi Ilmu Ekonomi YPM Sepanjang';
						$mahasiswa_pt_insert['nm_prodi_asal'] = 'Akuntansi';
					} else if ($mahasiswa_pt_insert['id_sms'] == '25b084b0-b377-4369-8a3b-c196a86246bb') {
						$mahasiswa_pt_insert['nm_pt_asal'] = 'Sekolah Tinggi Ilmu Ekonomi YPM Sepanjang';
						$mahasiswa_pt_insert['nm_prodi_asal'] = 'Akuntansi';
					} else if ($mahasiswa_pt_insert['id_sms'] == '96f4e4f6-adba-41b6-8d92-92369808ae44') {
						$mahasiswa_pt_insert['nm_pt_asal'] = 'Sekolah Tinggi Ilmu Hukum Ypm Sepanjang';
						$mahasiswa_pt_insert['nm_prodi_asal'] = 'Ilmu Hukum';
					} else if ($mahasiswa_pt_insert['id_sms'] == 'f9351bde-0387-4f6e-99c8-f38ec28543e4') {
						$mahasiswa_pt_insert['nm_pt_asal'] = 'Akademi Analis Kesehatan YPM Sidoarjo';
						$mahasiswa_pt_insert['nm_prodi_asal'] = 'Analis kesehatan';
					}
				}

				// Jika pendaftaran baru, sks diakui wajib 0
				if ($mahasiswa_pt_insert['id_jenis_daftar'] == '1') $mahasiswa_pt_insert['sks_diakui'] = '0';
				// Jika transfer & sks = 0, sks diakui set minimal 1 --> agar bisa sync
				if ($mahasiswa_pt_insert['id_jenis_daftar'] == '2' && $mahasiswa_pt_insert['sks_diakui'] == '0') $mahasiswa_pt_insert['sks_diakui'] = 1;

				// prodi baru (hukum yg sebelumnya ilmu hukum)
				if (
					$mahasiswa_pt_insert['id_prodi'] == '557a4732-66f6-4600-aa71-292b90c4637d'
					&& (int)$mahasiswa_pt_insert['id_periode_masuk'] < 20221
				) {
					$mahasiswa_pt_insert['tanggal_daftar'] = '2022-09-01';
					$mahasiswa_pt_insert['id_jenis_daftar'] = '2';
					$mahasiswa_pt_insert['id_perguruan_tinggi_asal'] = $mahasiswa_pt_insert['id_perguruan_tinggi'];

					// prodi lama (ilmu hukum)
					$mahasiswa_pt_insert['id_prodi_asal'] = '96f4e4f6-adba-41b6-8d92-92369808ae44';
					$mahasiswa_pt_insert['id_mahasiswa'] = $mahasiswa_pt_insert['fd_id_pd_lama'];
				}
				unset($mahasiswa_pt_insert['fd_id_pd_lama']);

				// TODO: Sampai sini, untuk Insert Riwayat Pendidikan WS2
				$insert_pt_result = $this->feederws->InsertRiwayatPendidikanMahasiswa($mahasiswa_pt_insert);

				// Jika berhasil insert, terdapat return id_reg_pd
				if (isset($insert_pt_result['data']['id_registrasi_mahasiswa']) && $insert_pt_result['data']['id_registrasi_mahasiswa'] != '') {
					// Pesan Insert, nipd (nim) mengambil dari mahasiswa_pt_insert
					$result['message'] = ($index_proses + 1) . " Insert {$mahasiswa_pt_insert['nim']} : Berhasil";

					// status sandbox
					$is_sandbox = ($this->session->userdata('is_sandbox') == TRUE) ? '1' : '0';

					// Melakukan update ke DB Langitan id_pd dan id_reg_pd hasil insert
					$this->rdb->Query("UPDATE mahasiswa SET fd_id_reg_pd = '{$insert_pt_result['data']['id_registrasi_mahasiswa']}', fd_sync_on = to_date('{$time_sync}', 'YYYY-MM-DD HH24:MI:SS') WHERE id_mhs = {$id_mhs}");
				} else // saat insert mahasiswa_pt gagal
				{
					// Pesan Insert, nipd mengambil dari mahasiswa_pt_insert
					$result['message'] = ($index_proses + 1) . ' Insert ' . $mahasiswa_pt_insert['nim'] . ' : ' . json_encode($insert_pt_result);
				}
				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Proses Update
			// --------------------------------
			else if ($index_proses < ($jumlah_insert + $jumlah_update)) {
				// index berjalan dikurangi jumlah data insert utk mendapatkan index update
				$index_proses -= $jumlah_insert;

				// Proses dalam bentuk key lowercase
				$mahasiswa_pt_update = array_change_key_case($mahasiswa_pt_update_set[$index_proses], CASE_LOWER);

				// Simpan id_mhs untuk update data di langitan
				$id_reg_pd	= $mahasiswa_pt_update['id_registrasi_mahasiswa'];
				$id_mhs	= $mahasiswa_pt_update['id_mhs'];

				// Hilangkan id_mhs & id_pd & id_reg_pd
				unset($mahasiswa_pt_update['id_mhs']);
				unset($mahasiswa_pt_update['id_registrasi_mahasiswa']);

				// ---------------------------------------------------------------------------------------------
				// Khusus UMAHA : Angkatan dibawah 2014 di geser ke 2014 Ganjil sebagai mhs transfer, default SKS = 1
				// ---------------------------------------------------------------------------------------------
				if ($this->satuan_pendidikan['npsn'] == '071086' && (int)$mahasiswa_pt_update['id_periode_masuk'] < 20141) {
					$mahasiswa_pt_update['mulai_smt'] = '20141';			// 2014/2015 Ganjil
					$mahasiswa_pt_update['tgl_masuk_sp'] = '2014-10-17';	// Tanggal SK Penggabungan Institusi
					$mahasiswa_pt_update['id_jenis_daftar'] = '2';			// Transfer

					// Asal PT & Prodi
					if ($mahasiswa_pt_update['id_sms'] == 'a9a267c7-63ff-4cbd-bcf6-b7f957fe1d80') {
						$mahasiswa_pt_update['nm_pt_asal'] = 'Sekolah Tinggi Teknik YPM Sepanjang';
						$mahasiswa_pt_update['nm_prodi_asal'] = 'Teknik Informatika';
					} else if ($mahasiswa_pt_update['id_sms'] == 'a9fec275-28ab-4895-894a-b545b8f7193a') {
						$mahasiswa_pt_update['nm_pt_asal'] = 'Sekolah Tinggi Teknik YPM Sepanjang';
						$mahasiswa_pt_update['nm_prodi_asal'] = 'Teknik Industri';
					} else if ($mahasiswa_pt_update['id_sms'] == '944fc32c-18f3-47f9-bc19-6e711480c858') {
						$mahasiswa_pt_update['nm_pt_asal'] = 'Sekolah Tinggi Teknik YPM Sepanjang';
						$mahasiswa_pt_update['nm_prodi_asal'] = 'Teknik Mesin';
					} else if ($mahasiswa_pt_update['id_sms'] == '613ec101-53d0-405c-b6e1-c4125f042f52') {
						$mahasiswa_pt_update['nm_pt_asal'] = 'Sekolah Tinggi Teknik YPM Sepanjang';
						$mahasiswa_pt_update['nm_prodi_asal'] = 'Teknik Komputer';
					} else if ($mahasiswa_pt_update['id_sms'] == 'c9e0c8b8-91c0-427a-bace-7215337e6515') {
						$mahasiswa_pt_update['nm_pt_asal'] = 'Sekolah Tinggi Ilmu Ekonomi YPM Sepanjang';
						$mahasiswa_pt_update['nm_prodi_asal'] = 'Manajemen';
					} else if ($mahasiswa_pt_update['id_sms'] == 'f0def671-d948-4d68-ab3e-a7f936f7a3b4') {
						$mahasiswa_pt_update['nm_pt_asal'] = 'Sekolah Tinggi Ilmu Ekonomi YPM Sepanjang';
						$mahasiswa_pt_update['nm_prodi_asal'] = 'Akuntansi';
					} else if ($mahasiswa_pt_update['id_sms'] == '25b084b0-b377-4369-8a3b-c196a86246bb') {
						$mahasiswa_pt_update['nm_pt_asal'] = 'Sekolah Tinggi Ilmu Ekonomi YPM Sepanjang';
						$mahasiswa_pt_update['nm_prodi_asal'] = 'Akuntansi';
					} else if ($mahasiswa_pt_update['id_sms'] == '96f4e4f6-adba-41b6-8d92-92369808ae44') {
						$mahasiswa_pt_update['nm_pt_asal'] = 'Sekolah Tinggi Ilmu Hukum Ypm Sepanjang';
						$mahasiswa_pt_update['nm_prodi_asal'] = 'Ilmu Hukum';
					} else if ($mahasiswa_pt_update['id_sms'] == 'f9351bde-0387-4f6e-99c8-f38ec28543e4') {
						$mahasiswa_pt_update['nm_pt_asal'] = 'Akademi Analis Kesehatan YPM Sidoarjo';
						$mahasiswa_pt_update['nm_prodi_asal'] = 'Analis kesehatan';
					}
				}

				// Jika pendaftaran baru, sks diakui wajib 0
				if ($mahasiswa_pt_update['id_jenis_daftar'] == '1') $mahasiswa_pt_update['sks_diakui'] = '0';
				// Jika transfer & sks = 0, sks diakui set minimal 1 --> agar bisa sync
				if ($mahasiswa_pt_update['id_jenis_daftar'] == '2' && $mahasiswa_pt_update['sks_diakui'] == '0') $mahasiswa_pt_update['sks_diakui'] = 1;

				// Update ke Feeder Riwayat Mahasiswa menggunakan Webservice 2
				$update_result = $this->feederws->UpdateRiwayatPendidikanMahasiswa(['id_registrasi_mahasiswa' => $id_reg_pd], $mahasiswa_pt_update);

				// Jika tidak ada masalah update
				if ($update_result['error_code'] == 0) {
					$result['message'] = ($index_proses + 1) . " Update {$mahasiswa_pt_update['nipd']} : Berhasil";

					$this->rdb->Query("UPDATE mahasiswa SET fd_sync_on = to_date('{$time_sync}','YYYY-MM-DD HH24:MI:SS') WHERE id_mhs = {$id_mhs}");
				}
				// Jika terdapat masalah update
				else {

					$result['message'] = ($index_proses + 1) . " Update {$mahasiswa_pt_update['nipd']} : Gagal update mahasiswa";
					$result['message'] .= "\n" .  $id_reg_pd . json_encode($update_result);
					$result['message'] .= "\n" . json_encode($mahasiswa_pt_update);
				}

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// meneruskan index proses ditambah lagi dengan jumlah data insert
				$index_proses += $jumlah_insert;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Selesai
			// --------------------------------
			else {
				$result['message'] = "Selesai";
				$result['status'] = SYNC_STATUS_DONE;
			}
		}

		echo json_encode($result);
	}


	/**
	 * GET /sync/mata_kuliah
	 */
	function mata_kuliah()
	{
		$jumlah = array();

		// Ambil jumlah mata_kuliah di feeder. Sudah obsolete
		$jumlah['feeder'] = '-';

		// Ambil data mata kuliah
		$sql_mk_raw = file_get_contents(APPPATH . 'models/sql/mata-kuliah.sql');
		$sql_mk = strtr($sql_mk_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$mk_set = $this->rdb->QueryToArray($sql_mk);

		$jumlah['langitan'] = $mk_set[0]['JUMLAH'];
		$jumlah['linked'] = $mk_set[1]['JUMLAH'];
		$jumlah['update'] = $mk_set[2]['JUMLAH'];
		$jumlah['insert'] = $mk_set[3]['JUMLAH'];

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		$this->smarty->assign('url_sync', site_url('sync/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync/' . $this->uri->segment(2) . '.tpl');
	}

	/**
	 * Ajax-GET /sync/mata_kuliah_data/
	 * @param string $kode_prodi Kode program studi versi Feeder
	 */
	function mata_kuliah_data($kode_prodi)
	{
		// Ambil jumlah mata kuliah di feeder. Sudah obsolete.
		
		$program_studi_set = $this->rdb->QueryToArray("SELECT FD_ID_SMS FROM PROGRAM_STUDI WHERE KODE_PROGRAM_STUDI = '{$kode_prodi}'");
		$id_sms = $program_studi_set[0]['FD_ID_SMS'];

		$filter = "id_prodi='{$id_sms}'";

		$response = $this->feederws->GetListMataKuliahFilterBy($filter);

		// Ambil data mata kuliah
		$sql_mk_raw = file_get_contents(APPPATH . 'models/sql/mata-kuliah-data.sql');
		$sql_mk = strtr($sql_mk_raw, array(
			'@npsn' => $this->satuan_pendidikan['npsn'],
			'@kode_prodi' => $kode_prodi
		));

		$mk_set = $this->rdb->QueryToArray($sql_mk);

		$jumlah['feeder'] = count($response['data']);
		$jumlah['langitan'] = $mk_set[0]['JUMLAH'];
		$jumlah['linked'] = $mk_set[1]['JUMLAH'];
		$jumlah['update'] = $mk_set[2]['JUMLAH'];
		$jumlah['insert'] = $mk_set[3]['JUMLAH'];

		echo json_encode($jumlah);
	}

	private function proses_mata_kuliah()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		// -----------------------------------
		// Ambil data untuk Insert
		// -----------------------------------
		if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			// Filter Prodi & Angkatan
			$kode_prodi = $this->input->post('kode_prodi');

			// Mendapatkan id_sms
			// $response = $this->feeder->GetRecord($this->token, FEEDER_SMS, "id_sp = '{$this->satuan_pendidikan['id_sp']}' AND trim(kode_prodi) = '{$kode_prodi}'");
			// $sms = $response['result'];

			// Ambil data mata kuliah
			$sql_mk_raw = file_get_contents(APPPATH . 'models/sql/mata-kuliah-insert.sql');
			$sql_mk = strtr($sql_mk_raw, array(
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@kode_prodi' => $kode_prodi
			));
			$mk_set = $this->rdb->QueryToArray($sql_mk);

			for ($i = 0; $i < count($mk_set); $i++) {
				$mk_set[$i]["SKS_MATA_KULIAH"] = (int)$mk_set[$i]["SKS_MATA_KULIAH"];
				$mk_set[$i]["SKS_TATAP_MUKA"] = (int)$mk_set[$i]["SKS_TATAP_MUKA"];
				$mk_set[$i]["SKS_PRAKTEK"] = (int)$mk_set[$i]["SKS_PRAKTEK"];
				$mk_set[$i]["SKS_PRAKTEK_LAPANGAN"] = (int)$mk_set[$i]["SKS_PRAKTEK_LAPANGAN"];
				$mk_set[$i]["SKS_SIMULASI"] = (int)$mk_set[$i]["SKS_SIMULASI"];
				$mk_set[$i]["ADA_SAP"] = (int)$mk_set[$i]["ADA_SAP"];
				$mk_set[$i]["ADA_SILABUS"] = (int)$mk_set[$i]["ADA_SILABUS"];
				$mk_set[$i]["ADA_BAHAN_AJAR"] = (int)$mk_set[$i]["ADA_BAHAN_AJAR"];
				$mk_set[$i]["ADA_ACARA_PRAKTEK"] = (int)$mk_set[$i]["ADA_ACARA_PRAKTEK"];
				$mk_set[$i]["ADA_DIKTAT"] = (int)$mk_set[$i]["ADA_DIKTAT"];
			}
			$this->session->set_userdata('mata_kuliah_insert_set', $mk_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Entri. Jumlah data: ' . count($mk_set);
			$result['status'] = SYNC_STATUS_PROSES;
			// $result['status'] = SYNC_STATUS_DONE;

			// die();
			// ganti parameter
			$_POST['mode'] = MODE_AMBIL_DATA_LANGITAN_2;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Ambil data untuk Update
		// -----------------------------------
		else if ($mode == MODE_AMBIL_DATA_LANGITAN_2) {
			// Filter Prodi & Angkatan
			$kode_prodi = $this->input->post('kode_prodi');

			// Mendapatkan id_sms
			// $response = $this->feeder->GetRecord($this->token, FEEDER_SMS, "id_sp = '{$this->satuan_pendidikan['id_sp']}' AND trim(kode_prodi) = '{$kode_prodi}'");
			// $sms = $response['result'];

			// Ambil data mata kuliah
			$sql_mk_raw = file_get_contents(APPPATH . 'models/sql/mata-kuliah-update.sql');
			$sql_mk = strtr($sql_mk_raw, array(
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@kode_prodi' => $kode_prodi
			));

			$mk_set = $this->rdb->QueryToArray($sql_mk);

			for ($i = 0; $i < count($mk_set); $i++) {
				$mk_set[$i]["SKS_MATA_KULIAH"] = (int)$mk_set[$i]["SKS_MATA_KULIAH"];
				$mk_set[$i]["SKS_TATAP_MUKA"] = (int)$mk_set[$i]["SKS_TATAP_MUKA"];
				$mk_set[$i]["SKS_PRAKTEK"] = (int)$mk_set[$i]["SKS_PRAKTEK"];
				$mk_set[$i]["SKS_PRAKTEK_LAPANGAN"] = (int)$mk_set[$i]["SKS_PRAKTEK_LAPANGAN"];
				$mk_set[$i]["SKS_SIMULASI"] = (int)$mk_set[$i]["SKS_SIMULASI"];
				$mk_set[$i]["ADA_SAP"] = (int)$mk_set[$i]["ADA_SAP"];
				$mk_set[$i]["ADA_SILABUS"] = (int)$mk_set[$i]["ADA_SILABUS"];
				$mk_set[$i]["ADA_BAHAN_AJAR"] = (int)$mk_set[$i]["ADA_BAHAN_AJAR"];
				$mk_set[$i]["ADA_ACARA_PRAKTEK"] = (int)$mk_set[$i]["ADA_ACARA_PRAKTEK"];
				$mk_set[$i]["ADA_DIKTAT"] = (int)$mk_set[$i]["ADA_DIKTAT"];
			}
			$this->session->set_userdata('mata_kuliah_update_set', $mk_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Update. Jumlah data: ' . count($mk_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Proses Sinkronisasi dari data yg sudah diambil
		// -----------------------------------
		else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$mata_kuliah_insert_set = $this->session->userdata('mata_kuliah_insert_set');
			$jumlah_insert = count($mata_kuliah_insert_set);


			// Ambil dari cache
			$mata_kuliah_update_set = $this->session->userdata('mata_kuliah_update_set');
			$jumlah_update = count($mata_kuliah_update_set);

			// Waktu Sinkronisasi
			$time_sync = date('Y-m-d H:i:s');

			// --------------------------------
			// Proses Insert
			// --------------------------------
			if ($index_proses < $jumlah_insert) {
				// Proses dalam bentuk key lowercase
				$mata_kuliah_insert = array_change_key_case($mata_kuliah_insert_set[$index_proses], CASE_LOWER);

				// Simpan id_mata_kuliah untuk update data di langitan
				$id_mata_kuliah = $mata_kuliah_insert['id_mata_kuliah'];

				// Hilangkan id_mata_kuliah
				unset($mata_kuliah_insert['id_mata_kuliah']);

				// Entri ke Feeder Mata Kuliah
				$insert_result = $this->feederws->InsertMataKuliah($mata_kuliah_insert);

				// Jika berhasil insert, terdapat return id_mk
				if (isset($insert_result['data']['id_matkul'])) {
					// Pesan Insert, tampilkan kode mk dan nama mk
					$result['message'] = ($index_proses + 1) . " Insert {$mata_kuliah_insert['kode_mata_kuliah']} {$mata_kuliah_insert['nama_mata_kuliah']} : Berhasil";

					// status sandbox
					$is_sandbox = ($this->session->userdata('is_sandbox') == TRUE) ? '1' : '0';

					// Update mata_kuliah.fd_id_mk
					$this->rdb->Query("UPDATE mata_kuliah SET fd_id_mk = '{$insert_result['data']['id_matkul']}', fd_sync_on = sysdate WHERE id_mata_kuliah = {$id_mata_kuliah}");
				} else // saat insert mata_kuliah gagal
				{
					// Pesan Insert, kode mk  mengambil dari mata_kuliah
					$result['message'] = ($index_proses + 1) . " Insert {$mata_kuliah_insert['kode_mata_kuliah']} {$mata_kuliah_insert['nama_mata_kuliah']} : " . json_encode($insert_result);
				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Proses Update
			// --------------------------------
			else if ($index_proses < ($jumlah_insert + $jumlah_update)) {
				// index berjalan dikurangi jumlah data insert utk mendapatkan index update
				$index_proses -= $jumlah_insert;

				// Proses dalam bentuk key lowercase
				$mata_kuliah_update = array_change_key_case($mata_kuliah_update_set[$index_proses], CASE_LOWER);

				// Simpan id_mk dan id_mata_kuliah untuk update data di langitan
				$id_mk			= $mata_kuliah_update['id_mk'];
				$id_mata_kuliah	= $mata_kuliah_update['id_mata_kuliah'];
				$kode_mk		= $mata_kuliah_update['kode_mata_kuliah'];
				$nm_mk			= $mata_kuliah_update['nama_mata_kuliah'];

				// Hilangkan id_mk & id_mata_kuliah
				unset($mata_kuliah_update['id_mk']);
				unset($mata_kuliah_update['id_mata_kuliah']);
				// unset($mata_kuliah_update['kode_mata_kuliah']);
				// unset($mata_kuliah_update['nama_mata_kuliah']);

				// Build data format
				$data_update = array(
					'key'	=> array('id_matkul' => $id_mk),
					$mata_kuliah_update
				);

				// Update ke Feeder Mata Kuliah
				$update_result = $this->feederws->UpdateMataKuliah(['id_matkul' => $id_mk], $mata_kuliah_update);

				// Jika tidak ada masalah update
				if ($update_result['error_code'] == 0) {
					$result['message'] = ($index_proses + 1) . " Update {$kode_mk} {$nm_mk} : Berhasil";

					$this->rdb->Query("UPDATE mata_kuliah SET fd_sync_on = sysdate WHERE id_mata_kuliah = {$id_mata_kuliah}");
				} else {
					$result['message'] = ($index_proses + 1) . " Update {$kode_mk} : Gagal. ";
					$result['message'] .= "({$update_result['error_code']}) {$update_result['error_desc']}";
					$result['message'] .= "\n" . json_encode($data_update);
				}

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// meneruskan index proses ditambah lagi dengan jumlah data insert
				$index_proses += $jumlah_insert;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Selesai
			// --------------------------------
			else {
				$result['message'] = "Selesai";
				$result['status'] = SYNC_STATUS_DONE;
			}
		}

		echo json_encode($result);
	}

	/**
	 * GET /sync/kurikulum/
	 */
	function kurikulum()
	{
		// Ambil jumlah kurikulum di feeder. Sudah obsolete.
		$jumlah['feeder'] = '-';

		// Ambil data kurikulum
		$sql_kurikulum_raw = file_get_contents(APPPATH . 'models/sql/kurikulum.sql');
		$sql_kurikulum = strtr($sql_kurikulum_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$kurikulum_set = $this->rdb->QueryToArray($sql_kurikulum);

		$jumlah['langitan'] = $kurikulum_set[0]['JUMLAH'];
		$jumlah['linked'] = $kurikulum_set[1]['JUMLAH'];
		$jumlah['update'] = $kurikulum_set[2]['JUMLAH'];
		$jumlah['insert'] = $kurikulum_set[3]['JUMLAH'];

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		$this->smarty->assign('url_sync', site_url('sync/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync/' . $this->uri->segment(2) . '.tpl');
	}

	/**
	 * Ajax-GET /sync/kurikulum_data/
	 * @param string $kode_prodi Kode program studi versi Feeder
	 */
	function kurikulum_data($kode_prodi)
	{
		// Ambil jumlah kurikulum di feeder. Sudah obsolete.
		
		$program_studi_set = $this->rdb->QueryToArray("SELECT FD_ID_SMS FROM PROGRAM_STUDI WHERE KODE_PROGRAM_STUDI = '{$kode_prodi}'");
		$id_sms = $program_studi_set[0]['FD_ID_SMS'];

		// Ambil data kurikulum
		$filter = "id_prodi='{$id_sms}'";
		$response = $this->feederws->GetListKurikulumFilterBy($filter);

		$sql_kurikulum_raw = file_get_contents(APPPATH . 'models/sql/kurikulum-data.sql');
		$sql_kurikulum = strtr($sql_kurikulum_raw, array(
			'@npsn' => $this->satuan_pendidikan['npsn'],
			'@kode_prodi' => $kode_prodi
		));

		$kurikulum_set = $this->rdb->QueryToArray($sql_kurikulum);
		
		$jumlah['feeder'] = count($response['data']);
		$jumlah['langitan'] = $kurikulum_set[0]['JUMLAH'];
		$jumlah['linked'] = $kurikulum_set[1]['JUMLAH'];
		$jumlah['update'] = $kurikulum_set[2]['JUMLAH'];
		$jumlah['insert'] = $kurikulum_set[3]['JUMLAH'];

		echo json_encode($jumlah);
	}

	private function proses_kurikulum()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		// -----------------------------------
		// Ambil data untuk Insert
		// -----------------------------------
		if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			// Filter Prodi
			$kode_prodi = $this->input->post('kode_prodi');

			// Ambil data kurikulum yang akan di insert
			$sql_kurikulum_raw = file_get_contents(APPPATH . 'models/sql/kurikulum-insert.sql');
			$sql_kurikulum = strtr($sql_kurikulum_raw, array(
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@kode_prodi' => $kode_prodi
			));

			$kurikulum_set = $this->rdb->QueryToArray($sql_kurikulum);

			$this->session->set_userdata('kurikulum_insert_set', $kurikulum_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Entri. Jumlah data: ' . count($kurikulum_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_AMBIL_DATA_LANGITAN_2;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Ambil data untuk Update
		// -----------------------------------
		else if ($mode == MODE_AMBIL_DATA_LANGITAN_2) {
			// Filter Prodi & Angkatan
			$kode_prodi = $this->input->post('kode_prodi');

			// Ambil data kurikulum yang akan di insert
			$sql_kurikulum_raw = file_get_contents(APPPATH . 'models/sql/kurikulum-update.sql');
			$sql_kurikulum = strtr($sql_kurikulum_raw, array(
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@kode_prodi' => $kode_prodi
			));

			$kurikulum_set = $this->rdb->QueryToArray($sql_kurikulum);

			$this->session->set_userdata('kurikulum_update_set', $kurikulum_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Update. Jumlah data: ' . count($kurikulum_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Proses Sinkronisasi dari data yg sudah diambil
		// -----------------------------------
		else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$kurikulum_insert_set = $this->session->userdata('kurikulum_insert_set');
			$jumlah_insert = count($kurikulum_insert_set);

			// Ambil dari cache
			$kurikulum_update_set = $this->session->userdata('kurikulum_update_set');
			$jumlah_update = count($kurikulum_update_set);

			// Waktu Sinkronisasi
			$time_sync = date('Y-m-d H:i:s');

			// --------------------------------
			// Proses Insert
			// --------------------------------
			if ($index_proses < $jumlah_insert) {
				// Proses dalam bentuk key lowercase
				$kurikulum_insert = array_change_key_case($kurikulum_insert_set[$index_proses], CASE_LOWER);

				// Simpan id_kurikulum untuk update data di langitan
				$id_kurikulum = $kurikulum_insert['id_kurikulum'];

				// Hilangkan id_kurikulum
				unset($kurikulum_insert['id_kurikulum']);

				// Entri ke Feeder Kurikulum
				$insert_result = $this->feederws->InsertKurikulum($kurikulum_insert);

				// Jika berhasil insert, terdapat return id_kurikulum_sp
				if (isset($insert_result['data']['id_kurikulum'])) {
					// Pesan Insert, tampilkan nama kurikulum
					$result['message'] = ($index_proses + 1) . " Insert {$kurikulum_insert['nama_kurikulum']} : Berhasil";

					// Update status sinkron fd_id_kurilum_sp 
					$fd_id_kurikulum_sp = $insert_result['data']['id_kurikulum'];
					$this->rdb->Query("UPDATE kurikulum SET fd_id_kurikulum_sp = '{$fd_id_kurikulum_sp}', fd_sync_on = sysdate WHERE id_kurikulum = {$id_kurikulum}");
				} else // saat insert kurikulum gagal
				{
					// Pesan insert jika gagal
					$result['message'] = ($index_proses + 1) . " Insert {$kurikulum_insert['nama_kurikulum']} : " .
						json_encode($insert_result) . "\n" .
						json_encode($kurikulum_insert);
				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Proses Update
			// --------------------------------
			else if ($index_proses < ($jumlah_insert + $jumlah_update)) {
				// index berjalan dikurangi jumlah data insert utk mendapatkan index update
				$index_proses -= $jumlah_insert;

				// Proses dalam bentuk key lowercase
				$kurikulum_update = array_change_key_case($kurikulum_update_set[$index_proses], CASE_LOWER);

				// Simpan id_kurikulum_sp dan kurikulum_prodi untuk update data di langitan
				$id_kurikulum_sp	= $kurikulum_update['id_kurikulum_sp'];
				$id_kurikulum		= $kurikulum_update['id_kurikulum'];

				// Hilangkan id_kurikulum_sp & id_kurikulum
				unset($kurikulum_update['id_kurikulum_sp']);
				unset($kurikulum_update['id_kurikulum']);

				// Update ke Feeder Kurikulum
				$update_result = $this->feederws->UpdateKurikulum(
					['id_kurikulum' => $id_kurikulum_sp],
					$kurikulum_update
				);

				// Jika tidak ada masalah update
				if ($update_result['error_code'] == 0) {
					$result['message'] = ($index_proses + 1) . " Update {$kurikulum_update['nama_kurikulum']} : Berhasil";

					// Update waktu sync
					$this->rdb->Query("UPDATE kurikulum SET fd_sync_on = sysdate WHERE id_kurikulum = {$id_kurikulum}");
				} else {
					$result['message'] = ($index_proses + 1) . " Update {$kurikulum_update['nama_kurikulum']} : Gagal. ";
					$result['message'] .= "({$update_result['error_code']}) {$update_result['error_desc']}";
					$result['message'] .= "\n" . json_encode($kurikulum_update);
				}

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// meneruskan index proses ditambah lagi dengan jumlah data insert
				$index_proses += $jumlah_insert;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Selesai
			// --------------------------------
			else {
				$result['message'] = "Selesai";
				$result['status'] = SYNC_STATUS_DONE;
			}
		}

		echo json_encode($result);
	}

	/**
	 * GET /sync/mata_kuliah_kurikulum/
	 */
	function mata_kuliah_kurikulum()
	{
		// Ambil jumlah mk kurikulum di feeder
		// $response = $this->feeder->GetCountRecordset($this->token, FEEDER_MK_KURIKULUM, null);
		$jumlah['feeder'] = '-';

		// Ambil data mata kuliah kurikulum
		$sql_mk_kurikulum_raw = file_get_contents(APPPATH . 'models/sql/mata-kuliah-kurikulum.sql');
		$sql_mk_kurikulum = strtr($sql_mk_kurikulum_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$data_set = $this->rdb->QueryToArray($sql_mk_kurikulum);

		$jumlah['langitan'] = $data_set[0]['JUMLAH'];
		$jumlah['linked'] = $data_set[1]['JUMLAH'];
		$jumlah['update'] = $data_set[2]['JUMLAH'];
		$jumlah['insert'] = $data_set[3]['JUMLAH'];

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		$this->smarty->assign('url_sync', site_url('sync/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync/' . $this->uri->segment(2) . '.tpl');
	}

	/**
	 * 
	 * @param string $kode_prodi
	 * @param string $id_kurikulum_prodi
	 */
	function mata_kuliah_kurikulum_data($kode_prodi, $id_kurikulum)
	{
		$program_studi_set = $this->rdb->QueryToArray("SELECT FD_ID_SMS FROM PROGRAM_STUDI WHERE KODE_PROGRAM_STUDI = '{$kode_prodi}'");
		$id_sms = $program_studi_set[0]['FD_ID_SMS'];

		$kurikulum_set = $this->rdb->QueryToArray("SELECT FD_ID_KURIKULUM_SP FROM KURIKULUM WHERE ID_KURIKULUM = '{$id_kurikulum}'");
		$fd_id_kurikulum = $kurikulum_set[0]['FD_ID_KURIKULUM_SP'];

		// Ambil data kurikulum
		$filter = "id_prodi='{$id_sms}' and id_kurikulum='{$fd_id_kurikulum}'";
		$response = $this->feederws->GetListMatkulKurikulumFilterBy($filter);

		// Query berbasis id_kurikulum_sp & id_mk

		$sql_mk_kurikulum_raw = file_get_contents(APPPATH . 'models/sql/mata-kuliah-kurikulum-data.sql');
		$sql_mk_kurikulum = strtr(
			$sql_mk_kurikulum_raw,
			[
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@id_kurikulum' => $id_kurikulum
			]
		);

		$data_set = $this->rdb->QueryToArray($sql_mk_kurikulum);

		$jumlah['feeder'] = count($response['data']);
		$jumlah['langitan'] = $data_set[0]['JUMLAH'];
		$jumlah['linked'] = $data_set[1]['JUMLAH'];
		$jumlah['update'] = $data_set[2]['JUMLAH'];
		$jumlah['insert'] = $data_set[3]['JUMLAH'];

		echo json_encode($jumlah);
	}

	private function proses_mata_kuliah_kurikulum()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		// -----------------------------------
		// Ambil data untuk Insert
		// -----------------------------------
		if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			// Filter prodi & kurikulum_prodi
			$kode_prodi			= $this->input->post('kode_prodi');
			$id_kurikulum	= $this->input->post('id_kurikulum');


			// Ambil data kurikulum yang akan di insert
			$sql_mata_kuliah_kurikulum_raw = file_get_contents(APPPATH . 'models/sql/mata-kuliah-kurikulum-insert.sql');
			$sql_mata_kuliah_kurikulum = strtr($sql_mata_kuliah_kurikulum_raw, array(
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@id_kurikulum' => $id_kurikulum,
				'@kode_prodi' => $kode_prodi
			));

			$data_set = $this->rdb->QueryToArray($sql_mata_kuliah_kurikulum);

			$this->session->set_userdata('data_insert_set', $data_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Entri. Jumlah data: ' . count($data_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Proses Sinkronisasi dari data yg sudah diambil
		// -----------------------------------
		else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$data_insert_set = $this->session->userdata('data_insert_set');
			$jumlah_insert = count($data_insert_set);

			// Ambil dari cache
			// $data_update_set = $this->session->userdata('data_update_set');
			// $jumlah_update = count($data_update_set);

			// Waktu Sinkronisasi
			$time_sync = date('Y-m-d H:i:s');

			// --------------------------------
			// Proses Insert
			// --------------------------------
			if ($index_proses < $jumlah_insert) {
				// Proses dalam bentuk key lowercase
				$data_insert = array_change_key_case($data_insert_set[$index_proses], CASE_LOWER);

				// Simpan informasi mata kuliah
				$id_kurikulum_mk		= $data_insert['id_kurikulum_mk'];
				$nm_mata_kuliah		= $data_insert['nm_mata_kuliah'];

				// Hilangkan data tdk diperlukan untuk insert
				unset($data_insert['id_kurikulum_mk']);
				unset($data_insert['nm_mata_kuliah']);

				// Entri ke Feeder mata_kuliah_kurikulum
				$insert_result = $this->feederws->InsertMatkulKurikulum($data_insert);

				// Jika berhasil insert, terdapat return id_kurikulum_sp & id_mk
				if (isset($insert_result['data']['id_kurikulum']) && isset($insert_result['data']['id_matkul'])) {
					// Pesan Insert, tampilkan nama kelas
					$result['message'] = ($index_proses + 1) . " Insert id_kurikulum_mk {$id_kurikulum_mk} {$nm_mata_kuliah} : Berhasil";

					// status sandbox
					$is_sandbox = ($this->session->userdata('is_sandbox') == TRUE) ? '1' : '0';

					// Melakukan insert ke feeder_mk_kurikulum hasil insert
					// $this->rdb->Query(
					// 	"INSERT INTO feeder_mk_kurikulum (ID_KURIKULUM_SP, ID_MK, LAST_SYNC, LAST_UPDATE, IS_SANDBOX)
					// 		VALUES ('{$insert_result['result']['id_kurikulum_sp']}', '{$insert_result['result']['id_mk']}', to_date('{$time_sync}', 'YYYY-MM-DD HH24:MI:SS'), to_date('{$time_sync}', 'YYYY-MM-DD HH24:MI:SS'), {$is_sandbox})"
					// );
					$query = "UPDATE kurikulum_mk 
							SET fd_id_kurikulum_sp = '{$insert_result['data']['id_kurikulum']}',
							fd_id_mk = '{$insert_result['data']['id_matkul']}',
							fd_sync_on = sysdate 
							WHERE id_kurikulum_mk = {$id_kurikulum_mk}";
					$this->rdb->Query($query);
				} else if ($insert_result['error_code'] == 630) { //jika Data mata kuliah kurikulum ini sudah ada
					//get mk kurikulum
					$get_mk_kurikulum_result = $this->feederws
						->GetMatkulKurikulum(
							$data_insert["id_matkul"],
							$data_insert["id_kurikulum"],
						);
					if (
						$get_mk_kurikulum_result["error_code"] == 0 &&
						count($get_mk_kurikulum_result["data"]) > 0
					) {
						$query = "UPDATE kurikulum_mk 
							SET fd_id_kurikulum_sp = '{$get_mk_kurikulum_result['data'][0]['id_kurikulum']}',
							fd_id_mk = '{$get_mk_kurikulum_result['data'][0]['id_matkul']}',
							fd_sync_on = sysdate 
							WHERE id_kurikulum_mk = {$id_kurikulum_mk}";
						$this->rdb->Query($query);
						$result['message'] = ($index_proses + 1) . " Insert id_kurikulum_mk {$id_kurikulum_mk} {$nm_mata_kuliah} : Berhasil";
					}
				} else // saat insert mk kurikulum gagal
				{
					// Pesan insert jika gagal
					$result['message'] = ($index_proses + 1) . " Insert Gagal {$id_kurikulum_mk} {$nm_mata_kuliah} : " . json_encode($insert_result);
				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}

			// --------------------------------
			// Selesai
			// --------------------------------
			else {
				$result['message'] = "Selesai";
				$result['status'] = SYNC_STATUS_DONE;
			}
		}

		echo json_encode($result);
	}

	/**
	 * AJAX-GET /sync/ambil_kurikulum/{$kode_prodi}
	 */
	function ambil_kurikulum($kode_prodi)
	{
		// Ambil data kurikulum yg sudah ada di feeder
		$sql =
			"SELECT
			k.id_kurikulum,
			nm_kurikulum AS nama_kurikulum
		FROM kurikulum k
		JOIN semester s ON s.id_semester = k.id_semester_mulai
		JOIN program_studi ps ON ps.id_program_studi = k.id_program_studi
		JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
		JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
		JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
		WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' 
		AND ps.kode_program_studi = '{$kode_prodi}' 
		AND ps.status_aktif_prodi = 1
		AND k.fd_id_kurikulum_sp IS NOT NULL";

		$kurikulum_set = $this->rdb->QueryToArray($sql);

		echo json_encode($kurikulum_set);
	}

	/**
	 * GET /sync/kelas_kuliah/
	 */
	function kelas_kuliah()
	{
		// Ambil jumlah kelas kuliah di feeder. Sudah obsolete.
		$jumlah['feeder'] = '-';

		// Ambil data mata kuliah
		$sql_kelas_kuliah_raw = file_get_contents(APPPATH . 'models/sql/kelas-kuliah.sql');
		$sql_kelas_kuliah = strtr($sql_kelas_kuliah_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$data_set = $this->rdb->QueryToArray($sql_kelas_kuliah);

		$jumlah['langitan'] = $data_set[0]['JUMLAH'];
		$jumlah['linked'] = $data_set[1]['JUMLAH'];
		$jumlah['update'] = $data_set[2]['JUMLAH'];
		$jumlah['insert'] = $data_set[3]['JUMLAH'];

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		$this->smarty->assign('url_sync', site_url('sync/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync/' . $this->uri->segment(2) . '.tpl');
	}

	/**
	 * Ajax-GET /sync/kelas_kuliah_data/
	 * @param string $kode_prodi Kode program studi versi Feeder
	 * @param string $id_smt Kode semester, format 20151 / 20152
	 */
	function kelas_kuliah_data($kode_prodi, $id_smt)
	{
		$program_studi_set = $this->rdb->QueryToArray("SELECT FD_ID_SMS FROM PROGRAM_STUDI WHERE KODE_PROGRAM_STUDI = '{$kode_prodi}'");
		$id_sms = $program_studi_set[0]['FD_ID_SMS'];

		$filter = "id_prodi='{$id_sms}' and id_semester='{$id_smt}'";
		$response = $this->feederws->GetListKelasKuliah($filter);

		// Konversi format semester ke id_semester
		$semester_langitan = $this->rdb->QueryToArray(
			"SELECT id_semester FROM semester s
			JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = s.id_perguruan_tinggi
			WHERE 
				pt.npsn = '{$this->satuan_pendidikan['npsn']}' AND 
				s.fd_id_smt = '{$id_smt}'"
		);
		$id_semester = $semester_langitan[0]['ID_SEMESTER'];

		// Ambil data mata kuliah
		$sql_kelas_kuliah_raw = file_get_contents(APPPATH . 'models/sql/kelas-kuliah-data.sql');
		$sql_kelas_kuliah = strtr($sql_kelas_kuliah_raw, array(
			'@npsn' => $this->satuan_pendidikan['npsn'],
			'@kode_prodi' => $kode_prodi,
			'@smt' => $id_semester
		));
		$data_set = $this->rdb->QueryToArray($sql_kelas_kuliah);

		$jumlah['feeder'] = count($response['data']);
		$jumlah['langitan'] = $data_set[0]['JUMLAH'];
		$jumlah['linked'] = $data_set[1]['JUMLAH'];
		$jumlah['update'] = $data_set[2]['JUMLAH'];
		$jumlah['insert'] = $data_set[3]['JUMLAH'];

		echo json_encode($jumlah);
	}

	private function proses_kelas_kuliah()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		// -----------------------------------
		// Ambil data untuk Insert
		// -----------------------------------
		if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			// Filter Prodi & Semester
			$kode_prodi = $this->input->post('kode_prodi');
			$id_smt		= $this->input->post('semester');

			// Konversi format semester ke id_semester
			$semester_langitan = $this->rdb->QueryToArray(
				"SELECT id_semester FROM semester s
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = s.id_perguruan_tinggi
				WHERE 
					pt.npsn = '{$this->satuan_pendidikan['npsn']}' AND 
					s.fd_id_smt = '{$id_smt}'"
			);
			$id_semester = $semester_langitan[0]['ID_SEMESTER'];

			// Ambil data kelas kuliah insert
			$sql_kelas_kuliah_raw = file_get_contents(APPPATH . 'models/sql/kelas-kuliah-insert.sql');
			$sql_kelas_kuliah = strtr($sql_kelas_kuliah_raw, array(
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@kode_prodi' => $kode_prodi,
				'@smt' => $id_semester,
				'@id_smt' => $id_smt
			));
			$data_set = $this->rdb->QueryToArray($sql_kelas_kuliah);

			$this->session->set_userdata('data_insert_set', $data_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Entri. Jumlah data: ' . count($data_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_AMBIL_DATA_LANGITAN_2;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Ambil data untuk Update
		// -----------------------------------
		else if ($mode == MODE_AMBIL_DATA_LANGITAN_2) {
			// Filter Prodi & Semester
			$kode_prodi = $this->input->post('kode_prodi');
			$id_smt		= $this->input->post('semester');

			// Konversi format semester ke id_semester
			$semester_langitan = $this->rdb->QueryToArray(
				"SELECT id_semester FROM semester s
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = s.id_perguruan_tinggi
				WHERE 
					pt.npsn = '{$this->satuan_pendidikan['npsn']}' AND 
					s.fd_id_smt = '{$id_smt}'"
			);
			$id_semester = $semester_langitan[0]['ID_SEMESTER'];

			// Ambil data kelas kuliah yang akan di update
			$sql_kelas_kuliah_raw = file_get_contents(APPPATH . 'models/sql/kelas-kuliah-update.sql');
			$sql_kelas_kuliah = strtr($sql_kelas_kuliah_raw, array(
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@kode_prodi' => $kode_prodi,
				'@smt' => $id_semester,
				'@id_smt' => $id_smt
			));
			$data_set = $this->rdb->QueryToArray($sql_kelas_kuliah);

			$this->session->set_userdata('data_update_set', $data_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Update. Jumlah data: ' . count($data_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Proses Sinkronisasi dari data yg sudah diambil
		// -----------------------------------
		else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$data_insert_set = $this->session->userdata('data_insert_set');
			$jumlah_insert = count($data_insert_set);

			// Ambil dari cache
			$data_update_set = $this->session->userdata('data_update_set');
			$jumlah_update = count($data_update_set);

			// --------------------------------
			// Proses Insert
			// --------------------------------
			if ($index_proses < $jumlah_insert) {
				// Proses dalam bentuk key lowercase
				$data_insert = array_change_key_case($data_insert_set[$index_proses], CASE_LOWER);

				// Simpan id_kelas_mk untuk update data di langitan, nama mk untuk tampilan sync
				$id_kelas_mk	= $data_insert['id_kelas_mk'];
				$nm_mata_kuliah	= $data_insert['nm_mata_kuliah'];

				// Hilangkan id_kelas_mk, nm_mata_kuliah dari array
				unset($data_insert['id_kelas_mk']);
				unset($data_insert['nm_mata_kuliah']);

				if ($data_insert['id_prodi'] == '96f4e4f6-adba-41b6-8d92-92369808ae44') { //jika ilmu hukum (prodi lama) 
					$data_insert['id_prodi'] = '557a4732-66f6-4600-aa71-292b90c4637d'; // ganti dengan hukum(prodi baru)
				}
				// Entri ke Feeder kelas_kuliah
				$insert_result = $this->feederws->InsertKelasKuliah($data_insert);

				// Jika berhasil insert, terdapat return id_kls
				if (isset($insert_result['data']['id_kelas_kuliah'])) {
					// Pesan Insert, tampilkan nama kelas
					$result['message'] = ($index_proses + 1) . " Insert {$nm_mata_kuliah} ({$data_insert['nama_kelas_kuliah']}) : Berhasil";

					$fd_id_kls = $insert_result['data']['id_kelas_kuliah'];
					$this->rdb->Query("UPDATE kelas_mk SET fd_id_kls = '{$fd_id_kls}', fd_sync_on = sysdate WHERE id_kelas_mk = {$id_kelas_mk}");
				} else // saat insert kelas kuliah gagal
				{
					// Pesan insert jika gagal
					$result['message'] = ($index_proses + 1) . " Insert {$data_insert['id_matkul']} {$nm_mata_kuliah} ({$data_insert['nama_kelas_kuliah']}) : " . json_encode($insert_result);
				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Proses Update
			// --------------------------------
			else if ($index_proses < ($jumlah_insert + $jumlah_update)) {
				// index berjalan dikurangi jumlah data insert utk mendapatkan index update
				$index_proses -= $jumlah_insert;

				// Proses dalam bentuk key lowercase
				$data_update = array_change_key_case($data_update_set[$index_proses], CASE_LOWER);

				// Simpan id_kls, id_kelas_mk, nm_mata_kuliah
				$id_kls			= $data_update['id_kls'];
				$id_kelas_mk	= $data_update['id_kelas_mk'];
				$nm_mata_kuliah	= $data_update['nm_mata_kuliah'];

				// Hilangkan id_kls, id_kelas_mk, nm_mata_kuliah
				unset($data_update['id_kls']);
				unset($data_update['id_kelas_mk']);
				unset($data_update['nm_mata_kuliah']);

				if ($data_update['id_prodi'] == '96f4e4f6-adba-41b6-8d92-92369808ae44') { //jika ilmu hukum (prodi lama) 
					$data_update['id_prodi'] = '557a4732-66f6-4600-aa71-292b90c4637d'; // ganti dengan hukum(prodi baru)
				}
				// Update ke Feeder kelas kuliah
				$update_result = $this->feederws->UpdateKelasKuliah(
					['id_kelas_kuliah' => $id_kls],
					$data_update
				);

				// Jika tidak ada masalah update
				if ($update_result['error_code'] == 0) {
					$result['message'] = ($index_proses + 1) . " Update {$nm_mata_kuliah} ({$data_update['nama_kelas_kuliah']}) : Berhasil";

					// Update waktu sync
					$this->rdb->Query("UPDATE kelas_mk SET fd_sync_on = sysdate WHERE id_kelas_mk = {$id_kelas_mk}");
				} else {
					$result['message'] = ($index_proses + 1) . " Update {$nm_mata_kuliah} ({$data_update['nama_kelas_kuliah']}) : Gagal. ";
					$result['message'] .= json_encode($update_result);
					$result['message'] .= json_encode($data_update);
				}

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// meneruskan index proses ditambah lagi dengan jumlah data insert
				$index_proses += $jumlah_insert;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Selesai
			// --------------------------------
			else {
				$result['message'] = "Selesai";
				$result['status'] = SYNC_STATUS_DONE;
			}
		}

		echo json_encode($result);
	}

	/**
	 * GET /sync/ajar_dosen/
	 */
	function ajar_dosen()
	{
		// Ambil jumlah kelas kuliah di feeder. Sudah obsolete.
		$jumlah['feeder'] = '-';

		// Ambil data ajar dosen / pengampu mk
		$sql_ajar_dosen_raw = file_get_contents(APPPATH . 'models/sql/ajar-dosen.sql');
		$sql_ajar_dosen = strtr($sql_ajar_dosen_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$data_set = $this->rdb->QueryToArray($sql_ajar_dosen);

		$jumlah['langitan'] = $data_set[0]['JUMLAH'];
		$jumlah['linked'] = $data_set[1]['JUMLAH'];
		$jumlah['update'] = $data_set[2]['JUMLAH'];
		$jumlah['insert'] = $data_set[3]['JUMLAH'];

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		$this->smarty->assign('url_sync', site_url('sync/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync/' . $this->uri->segment(2) . '.tpl');
	}

	/**
	 * Ajax-GET /sync/ajar_dosen_data/
	 * @param string $kode_prodi Kode program studi versi Feeder
	 * @param string $id_smt Kode semester, format 20151 / 20152
	 */
	function ajar_dosen_data($kode_prodi, $id_smt)
	{
		// Dosen ajar tidak bisa di query jumlah
		// $response = $this->feeder->GetCountRecordset($this->token, FEEDER_AJAR_DOSEN, "p.id_kls = '{$id_kls}'");
		$jumlah['feeder'] = '-';

		// Konversi format semester ke id_semester
		$sql_semester_raw = file_get_contents(APPPATH . 'models/sql/semester-konversi.sql');
		$sql_semester = strtr($sql_semester_raw, array('@npsn' => $this->satuan_pendidikan['npsn'], '@id_smt' => $id_smt));
		$semester_langitan = $this->rdb->QueryToArray($sql_semester);
		$id_semester = $semester_langitan[0]['ID_SEMESTER'];

		// Ambil data ajar dosen / pengampu mk
		$sql_ajar_dosen_raw = file_get_contents(APPPATH . 'models/sql/ajar-dosen-data.sql');
		$sql_ajar_dosen = strtr($sql_ajar_dosen_raw, array(
			'@npsn' => $this->satuan_pendidikan['npsn'],
			'@kode_prodi' => $kode_prodi,
			'@id_semester' => $id_semester
		));
		$data_set = $this->rdb->QueryToArray($sql_ajar_dosen);

		$jumlah['langitan'] = $data_set[0]['JUMLAH'];
		$jumlah['linked'] = $data_set[1]['JUMLAH'];
		$jumlah['update'] = $data_set[2]['JUMLAH'];
		$jumlah['insert'] = $data_set[3]['JUMLAH'];

		echo json_encode($jumlah);
	}

	private function proses_ajar_dosen()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		// -----------------------------------
		// Ambil data untuk Insert
		// -----------------------------------
		if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			// Filter Prodi & Semester
			$kode_prodi = $this->input->post('kode_prodi');
			$id_smt		= $this->input->post('semester');

			// Konversi format semester ke id_semester
			$sql_semester_raw = file_get_contents(APPPATH . 'models/sql/semester-konversi.sql');
			$sql_semester = strtr($sql_semester_raw, array('@npsn' => $this->satuan_pendidikan['npsn'], '@id_smt' => $id_smt));
			$semester_langitan = $this->rdb->QueryToArray($sql_semester);
			$id_semester = $semester_langitan[0]['ID_SEMESTER'];

			// Ambil data ajar dosen / pengampu mk
			$sql_ajar_dosen_raw = file_get_contents(APPPATH . 'models/sql/ajar-dosen-insert.sql');
			$sql_ajar_dosen = strtr($sql_ajar_dosen_raw, array(
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@kode_prodi' => $kode_prodi,
				'@smt' => $id_semester
			));
			$data_set = $this->rdb->QueryToArray($sql_ajar_dosen);

			$this->session->set_userdata('data_insert_set', $data_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Entri. Jumlah data: ' . count($data_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_AMBIL_DATA_LANGITAN_2;
			// $_POST['mode'] = MODE_SYNC;  // skip TO SINKRON LANGSUNG
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Ambil data untuk Update
		// -----------------------------------
		else if ($mode == MODE_AMBIL_DATA_LANGITAN_2) {
			// Filter Prodi & Semester
			$kode_prodi = $this->input->post('kode_prodi');
			$id_smt		= $this->input->post('semester');

			// Konversi format semester ke id_semester
			$sql_semester_raw = file_get_contents(APPPATH . 'models/sql/semester-konversi.sql');
			$sql_semester = strtr($sql_semester_raw, array('@npsn' => $this->satuan_pendidikan['npsn'], '@id_smt' => $id_smt));
			$semester_langitan = $this->rdb->QueryToArray($sql_semester);
			$id_semester = $semester_langitan[0]['ID_SEMESTER'];

			// Ambil data ajar dosen / pengampu mk
			$sql_ajar_dosen_raw = file_get_contents(APPPATH . 'models/sql/ajar-dosen-update.sql');
			$sql_ajar_dosen = strtr($sql_ajar_dosen_raw, array(
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@kode_prodi' => $kode_prodi,
				'@smt' => $id_semester
			));
			$data_set = $this->rdb->QueryToArray($sql_ajar_dosen);

			$this->session->set_userdata('data_update_set', $data_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Update. Jumlah data: ' . count($data_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Proses Sinkronisasi dari data yg sudah diambil
		// -----------------------------------
		else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$data_insert_set = $this->session->userdata('data_insert_set');
			$jumlah_insert = count($data_insert_set);

			// Ambil dari cache
			$data_update_set = $this->session->userdata('data_update_set');
			$jumlah_update = count($data_update_set);

			// Waktu Sinkronisasi
			$time_sync = date('Y-m-d H:i:s');

			// --------------------------------
			// Proses Insert
			// --------------------------------
			if ($index_proses < $jumlah_insert) {
				// Proses dalam bentuk key lowercase
				$data_insert = array_change_key_case($data_insert_set[$index_proses], CASE_LOWER);

				// Simpan data yg diperlukan
				$id_pengampu_mk	= $data_insert['id_pengampu_mk'];
				$nm_dosen		= $data_insert['nm_dosen'];
				$nm_kelas		= $data_insert['nm_kelas'];

				// Hilangkan dari array
				unset($data_insert['id_pengampu_mk']);
				unset($data_insert['nm_dosen']);
				unset($data_insert['nm_kelas']);

				// Entri ke Feeder ajar_dosen
				$insert_result = $this->feederws->InsertDosenPengajarKelasKuliah($data_insert);

				// Jika berhasil insert, terdapat return id_aktivitas_mengajar
				if (isset($insert_result['data']['id_aktivitas_mengajar'])) {
					// Pesan Insert 
					$result['message'] = ($index_proses + 1) . " Insert {$nm_dosen} ke kelas {$nm_kelas} SKS {$data_insert['sks_substansi_total']}: Berhasil";

					// Update status Sync
					$this->rdb->Query("UPDATE pengampu_mk SET fd_id_ajar = '{$insert_result['data']['id_aktivitas_mengajar']}', fd_sync_on = sysdate WHERE id_pengampu_mk = {$id_pengampu_mk}");
				} else // saat insert ajar dosen gagal
				{
					$pesan_error = "";
					if ($insert_result['error_code'] == 1150) {
						$pesan_error = "karena bukan dosen tetap";
					}
					else if ($insert_result['error_code'] == 15) {
						$pesan_error = "Pastikan sinkronisasi sebelumnya dilakukan terlebih dahulu, seperti sync kelas kuliah, sync mata kuliah kurikulum, dst";
					}else {
						$pesan_error = $insert_result['error_desc'];
					}
					// Pesan insert jika gagal
					$result['message'] = ($index_proses + 1) . " Insert {$nm_dosen} ke kelas {$nm_kelas} : Gagal. ";
					$result['message'] .= "{$insert_result['error_code']} {$pesan_error}";
					// $result['message'] .= "\n" . json_encode($data_insert);
				}


				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Proses Update
			// --------------------------------
			else if ($index_proses < ($jumlah_insert + $jumlah_update)) {
				// index berjalan dikurangi jumlah data insert utk mendapatkan index update
				$index_proses -= $jumlah_insert;

				// Proses dalam bentuk key lowercase
				$data_update = array_change_key_case($data_update_set[$index_proses], CASE_LOWER);

				// Simpan data yg diperlukan
				$id_pengampu_mk	= $data_update['id_pengampu_mk'];
				$id_ajar		= $data_update['id_aktivitas_mengajar'];
				$nm_dosen		= $data_update['nm_dosen'];
				$nm_kelas		= $data_update['nm_kelas'];

				// Hilangkan dari array
				unset($data_update['id_pengampu_mk']);
				unset($data_update['id_aktivitas_mengajar']);
				unset($data_update['nm_dosen']);
				unset($data_update['nm_kelas']);

				// Update ke Feeder kelas kuliah
				$update_result = $this->feederws->UpdateDosenPengajarKelasKuliah(
					['id_aktivitas_mengajar' => $id_ajar],
					$data_update
				);

				// Jika tidak ada masalah update
				if ($update_result['error_code'] == 0) {
					$result['message'] = ($index_proses + 1) . " Update {$nm_dosen} ke kelas {$nm_kelas} SKS {$data_update['sks_substansi_total']} : Berhasil";

					// Update waktu sync
					$this->rdb->Query("UPDATE pengampu_mk SET fd_sync_on = sysdate WHERE id_pengampu_mk = {$id_pengampu_mk}");
				} else {
					$result['message'] = ($index_proses + 1) . " Update {$nm_dosen} ke kelas {$nm_kelas} SKS {$data_update['sks_substansi_total']} : Gagal. ";
					$result['message'] .= json_encode($update_result);
					$result['message'] .= "\n" . json_encode($data_update);
				}

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// meneruskan index proses ditambah lagi dengan jumlah data insert
				$index_proses += $jumlah_insert;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Selesai
			// --------------------------------
			else {
				$result['message'] = "Selesai";
				$result['status'] = SYNC_STATUS_DONE;
			}
		}

		echo json_encode($result);
	}


	/**
	 * GET /sync/nilai/
	 */
	function nilai()
	{
		// Ambil jumlah nilai di feeder. Sudah obsolete;
		$jumlah['feeder'] = '-';

		// Ambil data nilai
		$sql_nilai_raw = file_get_contents(APPPATH . 'models/sql/nilai.sql');
		$sql_nilai = strtr($sql_nilai_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$data_set = $this->rdb->QueryToArray($sql_nilai);

		$jumlah['langitan'] = $data_set[0]['JUMLAH'];
		$jumlah['linked'] = $data_set[1]['JUMLAH'];
		$jumlah['update'] = $data_set[2]['JUMLAH'];
		$jumlah['insert'] = $data_set[3]['JUMLAH'];

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		$this->smarty->assign('url_sync', site_url('sync/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync/' . $this->uri->segment(2) . '.tpl');
	}

	/**
	 * Ajax-GET /sync/nilai_data/
	 * @param string $kode_prodi Kode program studi versi Feeder
	 * @param string $id_smt Kode semester versi Feeder
	 */
	function nilai_data($kode_prodi, $id_smt, $id_kelas_mk)
	{
		// Konversi id_kelas_mk ke id_kls feeder
		$kelas_langitan = $this->rdb->QueryToArray(
			"SELECT fd_id_kls as id_kls FROM kelas_mk kmk
			WHERE kmk.id_kelas_mk = {$id_kelas_mk}"
		);
		$id_kls = $kelas_langitan[0]['ID_KLS'];

		// Ambil peserta kelas di feeder. Sudah obsolete.
		$jumlah['feeder'] = '-';

		// Ambil data nilai
		$sql_nilai_raw = file_get_contents(APPPATH . 'models/sql/nilai-data.sql');
		$sql_nilai = strtr($sql_nilai_raw, array('@id_kelas_mk' => $id_kelas_mk));
		$data_set = $this->rdb->QueryToArray($sql_nilai);

		$jumlah['langitan'] = $data_set[0]['JUMLAH'];
		$jumlah['linked'] = $data_set[1]['JUMLAH'];
		$jumlah['update'] = $data_set[2]['JUMLAH'];
		$jumlah['insert'] = $data_set[3]['JUMLAH'];

		echo json_encode($jumlah);
	}

	function nilai_per_prodi_data($kode_prodi, $id_smt)
	{
		// Konversi id_kelas_mk ke id_kls feeder

		$program_studi_set = $this->rdb->QueryToArray("SELECT FD_ID_SMS FROM PROGRAM_STUDI WHERE KODE_PROGRAM_STUDI = '{$kode_prodi}'");
		$id_sms = $program_studi_set[0]['FD_ID_SMS'];

		$filter = "id_prodi='{$id_sms}' and id_semester='{$id_smt}'";
		$response = $this->feederws->GetDetailNilaiPerkuliahanKelas($filter);

		$sql_semester_raw = file_get_contents(APPPATH . 'models/sql/semester-konversi.sql');
		$sql_semester = strtr($sql_semester_raw, array('@npsn' => $this->satuan_pendidikan['npsn'], '@id_smt' => $id_smt));
		$semester_langitan = $this->rdb->QueryToArray($sql_semester);
		$id_semester = $semester_langitan[0]['ID_SEMESTER'];
		// Ambil data nilai
		$sql_nilai_raw = file_get_contents(APPPATH . 'models/sql/nilai-per-prodi-data.sql');
		$sql_nilai = strtr($sql_nilai_raw, array(
			'@npsn' => $this->satuan_pendidikan['npsn'],
			'@kode_prodi' => $kode_prodi,
			'@smt' => $id_semester
		));
		$data_set = $this->rdb->QueryToArray($sql_nilai);

		$jumlah['feeder'] = count($response['data']);
		$jumlah['langitan'] = $data_set[0]['JUMLAH'];
		$jumlah['linked'] = $data_set[1]['JUMLAH'];
		$jumlah['update'] = $data_set[2]['JUMLAH'];
		$jumlah['insert'] = $data_set[3]['JUMLAH'];
		$jumlah['delete'] = $data_set[4]['JUMLAH'];

		echo json_encode($jumlah);
	}

	public function nilai_per_prodi()
	{
		// Ambil jumlah nilai di feeder. Sudah obsolete.
		$jumlah['feeder'] = '-';

		// Ambil data nilai
		$sql_nilai_raw = file_get_contents(APPPATH . 'models/sql/nilai.sql');
		$sql_nilai = strtr($sql_nilai_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$data_set = $this->rdb->QueryToArray($sql_nilai);

		$jumlah['langitan'] = $data_set[0]['JUMLAH'];
		$jumlah['linked'] = $data_set[1]['JUMLAH'];
		$jumlah['update'] = $data_set[2]['JUMLAH'];
		$jumlah['insert'] = $data_set[3]['JUMLAH'];
		$jumlah['delete'] = $data_set[4]['JUMLAH'];

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		$this->smarty->assign('url_sync', site_url('sync/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync/' . $this->uri->segment(2) . '.tpl');
	}

	function nilai_transfer_data($id_smt)
	{
		// Konversi id_kelas_mk ke id_kls feeder


		// Ambil peserta kelas di feeder. Sudah obsolete.
		$jumlah['feeder'] = '-';

		$sql_semester_raw = file_get_contents(APPPATH . 'models/sql/semester-konversi.sql');
		$sql_semester = strtr($sql_semester_raw, array('@npsn' => $this->satuan_pendidikan['npsn'], '@id_smt' => $id_smt));
		$semester_langitan = $this->rdb->QueryToArray($sql_semester);
		$id_semester = $semester_langitan[0]['ID_SEMESTER'];
		// Ambil data nilai
		$sql_nilai_raw = file_get_contents(APPPATH . 'models/sql/nilai-transfer-data.sql');
		$sql_nilai = strtr($sql_nilai_raw, array(
			'@npsn' => $this->satuan_pendidikan['npsn'],
			'@smt' => $id_semester
		));
		$data_set = $this->rdb->QueryToArray($sql_nilai);

		$jumlah['langitan'] = $data_set[0]['JUMLAH'];
		$jumlah['linked'] = $data_set[1]['JUMLAH'];
		$jumlah['update'] = $data_set[2]['JUMLAH'];
		$jumlah['insert'] = $data_set[3]['JUMLAH'];
		$jumlah['delete'] = $data_set[4]['JUMLAH'];
		echo json_encode($jumlah);
	}

	public function nilai_transfer()
	{
		$jumlah['feeder'] = '-';

		$sql_nilai_transfer_raw = file_get_contents(APPPATH . 'models/sql/nilai-transfer.sql');
		$sql_nilai_transfer = strtr($sql_nilai_transfer_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$data_set = $this->rdb->QueryToArray($sql_nilai_transfer);

		$jumlah['langitan'] = $data_set[0]['JUMLAH'];
		$jumlah['linked'] = $data_set[1]['JUMLAH'];
		$jumlah['update'] = $data_set[2]['JUMLAH'];
		$jumlah['insert'] = $data_set[3]['JUMLAH'];
		$jumlah['delete'] = $data_set[4]['JUMLAH'];

		$this->smarty->assign('jumlah', $jumlah);

		$this->smarty->assign('url_sync', site_url('sync/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync/' . $this->uri->segment(2) . '.tpl');
	}

	public function nilai_transfer_umaha()
	{
		$jumlah['feeder'] = '-';

		// Ambil data nilai transfer
		$sql_nilai_raw = file_get_contents(APPPATH . 'models/sql/nilai-transfer-umaha.sql');
		$data_set = $this->rdb->QueryToArray($sql_nilai_raw);

		$jumlah['langitan'] = $data_set[0]['JUMLAH'];
		$jumlah['linked'] = $data_set[1]['JUMLAH'];
		$jumlah['update'] = $data_set[2]['JUMLAH'];
		$jumlah['insert'] = $data_set[3]['JUMLAH'];
		$jumlah['delete'] = 0;

		$this->smarty->assign('jumlah', $jumlah);

		$this->smarty->assign('url_sync', site_url('sync/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync/' . $this->uri->segment(2) . '.tpl');
	}

	private function proses_nilai()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		// -----------------------------------
		// Ambil data untuk Insert
		// -----------------------------------
		if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			// Parameter
			$id_kelas_mk = $this->input->post('id_kelas_mk');

			// Ambil data nilai peserta kuliah yg akan insert
			$sql_nilai_raw = file_get_contents(APPPATH . 'models/sql/nilai-insert.sql');
			$sql_nilai = strtr($sql_nilai_raw, array('@id_kelas_mk' => $id_kelas_mk));
			$data_set = $this->rdb->QueryToArray($sql_nilai);

			$this->session->set_userdata('data_insert_set', $data_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Entri. Jumlah data: ' . count($data_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_AMBIL_DATA_LANGITAN_2;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Ambil data untuk Update
		// -----------------------------------
		else if ($mode == MODE_AMBIL_DATA_LANGITAN_2) {
			// Parameter
			$id_kelas_mk = $this->input->post('id_kelas_mk');

			// Ambil peserta kuliah yg akan di update
			$sql_nilai_raw = file_get_contents(APPPATH . 'models/sql/nilai-update.sql');
			$sql_nilai = strtr($sql_nilai_raw, array('@id_kelas_mk' => $id_kelas_mk));
			$data_set = $this->rdb->QueryToArray($sql_nilai);

			$this->session->set_userdata('data_update_set', $data_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Update. Jumlah data: ' . count($data_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Proses Sinkronisasi dari data yg sudah diambil
		// -----------------------------------
		else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$data_insert_set = $this->session->userdata('data_insert_set');
			$jumlah_insert = count($data_insert_set);

			// Ambil dari cache
			$data_update_set = $this->session->userdata('data_update_set');
			$jumlah_update = count($data_update_set);

			// Waktu Sinkronisasi
			$time_sync = date('Y-m-d H:i:s');

			// --------------------------------
			// Proses Insert
			// --------------------------------
			if ($index_proses < $jumlah_insert) {
				// Proses dalam bentuk key lowercase
				$data_insert = array_change_key_case($data_insert_set[$index_proses], CASE_LOWER);

				// Simpan mhs untuk tampilan sync
				$id_pengambilan_mk	= $data_insert['id_pengambilan_mk'];
				$mhs				= $data_insert['mhs'];
				$nilai_huruf		= $data_insert['nilai_huruf'];
				$nilai_angka		= $data_insert['nilai_angka'];
				$nilai_indeks		= $data_insert['nilai_indeks'];

				// Hilangkan mhs dari array
				unset($data_insert['id_pengambilan_mk']);
				unset($data_insert['mhs']);

				// Cleansing data
				if ($data_insert['nilai_huruf'] == '') {
					unset($data_insert['nilai_huruf']);
				}
				if ($data_insert['nilai_angka'] == '') {
					unset($data_insert['nilai_angka']);
				}

				// Entri ke Feeder nilai
				$insert_result = $this->feederws->UpdateNilaiPerkuliahanKelas(
					[
						'id_registrasi_mahasiswa' => $data_insert['id_registrasi_mahasiswa'],
						'id_kelas_kuliah' => $data_insert['id_kelas_kuliah']
					],
					$data_insert
				);

				// Jika berhasil insert, terdapat return id_kls & id_reg_pd
				if (isset($insert_result['id_kelas_kuliah']) && isset($insert_result['id_registrasi_mahasiswa'])) {
					// Pesan Insert, tampilkan nama mahasiswa
					$result['message'] = ($index_proses + 1) . " Insert {$mhs} Nilai = {$nilai_angka} ({$nilai_huruf} : $nilai_indeks) : Berhasil";

					// Update status sync
					$this->rdb->Query("UPDATE pengambilan_mk 
					SET 
						fd_id_kls = '{$insert_result['id_kelas_kuliah']}', 
						fd_id_reg_pd = '{$insert_result['id_registrasi_mahasiswa']}', 
						fd_nilai_huruf = '{$nilai_huruf}',
						fd_nilai_angka = '{$nilai_angka}',
						fd_sync_on = sysdate 
					WHERE id_pengambilan_mk = {$id_pengambilan_mk}");
				} else // saat insert nilai gagal
				{
					// Pesan insert jika gagal
					$result['message'] = ($index_proses + 1) . " Insert {$mhs} Nilai = {$nilai_angka} ({$nilai_huruf} : $nilai_indeks) : Gagal. " . json_encode($insert_result);
					$result['message'] .= "\n" . json_encode($data_insert);
				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Proses Update
			// --------------------------------
			else if ($index_proses < ($jumlah_insert + $jumlah_update)) {
				// index berjalan dikurangi jumlah data insert utk mendapatkan index update
				$index_proses -= $jumlah_insert;

				// Proses dalam bentuk key lowercase
				$data_update = array_change_key_case($data_update_set[$index_proses], CASE_LOWER);

				// Simpan informasi mahasiswa
				$id_pengambilan_mk	= $data_update['id_pengambilan_mk'];
				$mhs				= $data_update['mhs'];
				$id_kls				= $data_update['id_kelas_kuliah'];
				$id_reg_pd			= $data_update['id_registrasi_mahasiswa'];
				$nilai_huruf		= $data_update['nilai_huruf'];
				$nilai_angka		= $data_update['nilai_angka'];
				$nilai_indeks		= $data_update['nilai_indeks'];

				// Hilangkan data tdk diperlukan untuk update
				unset($data_update['id_pengambilan_mk']);
				unset($data_update['mhs']);
				unset($data_update['id_kelas_kuliah']);
				unset($data_update['id_registrasi_mahasiswa']);

				// Cleansing data
				if ($data_update['nilai_huruf'] == '') {
					unset($data_update['nilai_huruf']);
				}
				if ($data_update['nilai_angka'] == '') {
					unset($data_update['nilai_angka']);
				}

				// Update ke Feeder nilai
				$update_result = $this->feederws->UpdateNilaiPerkuliahanKelas(
					[
						'id_registrasi_mahasiswa' => $id_reg_pd,
						'id_kelas_kuliah' => $id_kls
					],
					$data_update
				);

				// Jika tidak ada masalah update
				if ($update_result['error_code'] == 0) {
					$result['message'] = ($index_proses + 1) . " Update {$mhs} Nilai = {$nilai_angka} ({$nilai_huruf} : $nilai_indeks) : Berhasil";

					// Update status sync
					$this->rdb->Query("UPDATE pengambilan_mk 
					SET 
						fd_nilai_huruf = '{$nilai_huruf}',
						fd_nilai_angka = '{$nilai_angka}',
						fd_sync_on = sysdate 
					WHERE id_pengambilan_mk = {$id_pengambilan_mk}");
				} else {
					$result['message'] = ($index_proses + 1) . " Update {$mhs} Nilai: {$nilai_huruf} ({$nilai_angka}) : Gagal. ";
					$result['message'] .= json_encode($update_result);
					$result['message'] .= "\n" . json_encode($data_update);
				}

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// meneruskan index proses ditambah lagi dengan jumlah data insert
				$index_proses += $jumlah_insert;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Selesai
			// --------------------------------
			else {
				$result['message'] = "Selesai";
				$result['status'] = SYNC_STATUS_DONE;
			}
		}

		echo json_encode($result);
	}

	private function proses_nilai_per_prodi()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		// -----------------------------------
		// Ambil data untuk Insert
		// -----------------------------------
		if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			// Parameter
			$kode_prodi = $this->input->post('kode_prodi');
			$id_smt		= $this->input->post('semester');

			// Konversi format semester ke id_semester
			$sql_semester_raw = file_get_contents(APPPATH . 'models/sql/semester-konversi.sql');
			$sql_semester = strtr($sql_semester_raw, array('@npsn' => $this->satuan_pendidikan['npsn'], '@id_smt' => $id_smt));
			$semester_langitan = $this->rdb->QueryToArray($sql_semester);
			$id_semester = $semester_langitan[0]['ID_SEMESTER'];

			// Ambil data nilai peserta kuliah yg akan insert
			$sql_nilai_prodi_raw = file_get_contents(APPPATH . 'models/sql/nilai-per-prodi-insert.sql');
			$sql_nilai_prodi = strtr($sql_nilai_prodi_raw, array(
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@kode_prodi' => $kode_prodi,
				'@smt' => $id_semester
			));
			$data_set = $this->rdb->QueryToArray($sql_nilai_prodi);

			$this->session->set_userdata('data_insert_set', $data_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Entri. Jumlah data: ' . count($data_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_AMBIL_DATA_LANGITAN_2;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Ambil data untuk Update
		// -----------------------------------
		else if ($mode == MODE_AMBIL_DATA_LANGITAN_2) {
			// Parameter
			$kode_prodi = $this->input->post('kode_prodi');
			$id_smt		= $this->input->post('semester');

			// Konversi format semester ke id_semester
			$sql_semester_raw = file_get_contents(APPPATH . 'models/sql/semester-konversi.sql');
			$sql_semester = strtr($sql_semester_raw, array('@npsn' => $this->satuan_pendidikan['npsn'], '@id_smt' => $id_smt));
			$semester_langitan = $this->rdb->QueryToArray($sql_semester);
			$id_semester = $semester_langitan[0]['ID_SEMESTER'];

			// Ambil peserta kuliah yg akan di update
			$sql_nilai_raw = file_get_contents(APPPATH . 'models/sql/nilai-per-prodi-update.sql');
			$sql_nilai = strtr($sql_nilai_raw, array(
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@kode_prodi' => $kode_prodi,
				'@smt' => $id_semester
			));
			$data_set = $this->rdb->QueryToArray($sql_nilai);

			$this->session->set_userdata('data_update_set', $data_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Update. Jumlah data: ' . count($data_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_AMBIL_DATA_LANGITAN_3;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Ambil data untuk Delete
		// -----------------------------------
		else if ($mode == MODE_AMBIL_DATA_LANGITAN_3) {
			// Parameter
			$kode_prodi = $this->input->post('kode_prodi');
			$id_smt		= $this->input->post('semester');

			// Konversi format semester ke id_semester
			$sql_semester_raw = file_get_contents(APPPATH . 'models/sql/semester-konversi.sql');
			$sql_semester = strtr($sql_semester_raw, array('@npsn' => $this->satuan_pendidikan['npsn'], '@id_smt' => $id_smt));
			$semester_langitan = $this->rdb->QueryToArray($sql_semester);
			$id_semester = $semester_langitan[0]['ID_SEMESTER'];

			// Ambil peserta kuliah yg akan di update
			$sql_nilai_raw = file_get_contents(APPPATH . 'models/sql/nilai-per-prodi-delete.sql');
			$sql_nilai = strtr($sql_nilai_raw, array(
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@kode_prodi' => $kode_prodi,
				'@smt' => $id_semester
			));
			$data_set = $this->rdb->QueryToArray($sql_nilai);

			$this->session->set_userdata('data_delete_set', $data_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Delete. Jumlah data: ' . count($data_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Proses Sinkronisasi dari data yg sudah diambil
		// -----------------------------------
		else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil data insert
			$data_insert_set = $this->session->userdata('data_insert_set');
			$jumlah_insert = count($data_insert_set);

			// Ambil data update
			$data_update_set = $this->session->userdata('data_update_set');
			$jumlah_update = count($data_update_set);

			// Ambil data delete
			$data_delete_set = $this->session->userdata('data_delete_set');
			$jumlah_delete = count($data_delete_set);

			// --------------------------------
			// Proses Insert
			// --------------------------------
			if ($index_proses < $jumlah_insert) {
				// Proses dalam bentuk key lowercase
				$data_insert = array_change_key_case($data_insert_set[$index_proses], CASE_LOWER);

				// Simpan mhs untuk tampilan sync
				$id_pengambilan_mk	= $data_insert['id_pengambilan_mk'];
				$mhs				= $data_insert['mhs'];
				$nilai_huruf		= $data_insert['nilai_huruf'];
				$nilai_angka		= $data_insert['nilai_angka'];
				$nilai_indeks		= $data_insert['nilai_indeks'];
				$nama_kelas			= $data_insert['nama_kelas'];

				// Hilangkan mhs dari array
				unset($data_insert['id_pengambilan_mk']);
				unset($data_insert['mhs']);
				unset($data_insert['nama_kelas']);

				// Cleansing data
				if ($data_insert['nilai_huruf'] == '') {
					unset($data_insert['nilai_huruf']);
				}
				if ($data_insert['nilai_angka'] == '') {
					unset($data_insert['nilai_angka']);
				}

				// Entri KRS nya
				$this->feederws->InsertPesertaKelasKuliah(
					[
						'id_registrasi_mahasiswa' => $data_insert['id_registrasi_mahasiswa'],
						'id_kelas_kuliah' => $data_insert['id_kelas_kuliah']
					]
				);

				// Entri ke Feeder nilai
				$insert_result = $this->feederws->UpdateNilaiPerkuliahanKelas(
					[
						'id_registrasi_mahasiswa' => $data_insert['id_registrasi_mahasiswa'],
						'id_kelas_kuliah' => $data_insert['id_kelas_kuliah']
					],
					$data_insert
				);

				// Jika berhasil insert, terdapat return id_kls & id_reg_pd
				if ($insert_result['error_code'] == 0) {
					// Pesan Insert, tampilkan nama mahasiswa
					$result['message'] = ($index_proses + 1) . " Insert {$nama_kelas} {$mhs} Nilai = {$nilai_angka} ({$nilai_huruf} : $nilai_indeks) : Berhasil. ";

					// Update status sync
					$this->rdb->Query("UPDATE pengambilan_mk 
					SET 
						fd_id_kls = '{$insert_result['data']['id_kelas_kuliah']}', 
						fd_id_reg_pd = '{$insert_result['data']['id_registrasi_mahasiswa']}', 
						fd_nilai_huruf = '{$nilai_huruf}',
						fd_nilai_angka = '{$nilai_angka}',
						fd_sync_on = sysdate 
					WHERE id_pengambilan_mk = {$id_pengambilan_mk}");
				} else // saat insert nilai gagal
				{
					$result['message'] = ($index_proses + 1) . " Insert {$nama_kelas} {$mhs} Nilai = {$nilai_angka} ({$nilai_huruf} : $nilai_indeks) : Gagal. ";

					// error_code 800 : Data nilai dari id_kelas_kuliah dan id_registrasi_mahasiswa ini sudah ada
					if ($insert_result['error_code'] == 800) {
						// Tambahan Informasi
						$result['message'] .= $insert_result['error_desc'] . ' Lakukan sinkronisasi ulang.';

						// Update status sync di langitan, agar bisa di update untuk sinkron ulang
						$this->rdb->Query("UPDATE pengambilan_mk SET fd_id_kls = '{$data_insert['id_kelas_kuliah']}', fd_id_reg_pd = '{$data_insert['id_registrasi_mahasiswa']}', fd_sync_on = created_on, updated_on = sysdate WHERE id_pengambilan_mk = {$id_pengambilan_mk}");
					} else {
						// Tambahan Informasi
						$result['message'] .= json_encode($insert_result);
						$result['message'] .= "\n" . json_encode($data_insert);
					}
				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Proses Update
			// --------------------------------
			else if ($index_proses < ($jumlah_insert + $jumlah_update)) {
				// index berjalan dikurangi jumlah data insert utk mendapatkan index update
				$index_proses -= $jumlah_insert;

				// Proses dalam bentuk key lowercase
				$data_update = array_change_key_case($data_update_set[$index_proses], CASE_LOWER);

				// Simpan informasi mahasiswa
				$id_pengambilan_mk	= $data_update['id_pengambilan_mk'];
				$mhs				= $data_update['mhs'];
				$id_kls				= $data_update['id_kelas_kuliah'];
				$id_reg_pd			= $data_update['id_registrasi_mahasiswa'];
				$nilai_huruf		= $data_update['nilai_huruf'];
				$nilai_angka		= $data_update['nilai_angka'];
				$nilai_indeks		= $data_update['nilai_indeks'];
				$nama_kelas			= $data_update['nama_kelas'];

				// Hilangkan data tdk diperlukan untuk update
				unset($data_update['id_pengambilan_mk']);
				unset($data_update['mhs']);
				unset($data_update['id_kelas_kuliah']);
				unset($data_update['id_registrasi_mahasiswa']);
				unset($data_update['nama_kelas']);

				// Cleansing data
				if ($data_update['nilai_huruf'] == '') {
					unset($data_update['nilai_huruf']);
				}
				if ($data_update['nilai_angka'] == '') {
					unset($data_update['nilai_angka']);
				}

				// Update ke Feeder nilai
				$update_result = $this->feederws->UpdateNilaiPerkuliahanKelas(
					[
						'id_registrasi_mahasiswa' => $id_reg_pd,
						'id_kelas_kuliah' => $id_kls
					],
					$data_update
				);

				// Jika tidak ada masalah update
				if ($update_result['error_code'] == 0) {
					$result['message'] = ($index_proses + 1) . " Update {$nama_kelas} {$mhs} Nilai = {$nilai_angka} ({$nilai_huruf} : $nilai_indeks) : Berhasil";

					// Update status sync
					$this->rdb->Query("UPDATE pengambilan_mk 
					SET 
						fd_nilai_huruf = '{$nilai_huruf}',
						fd_nilai_angka = '{$nilai_angka}',
						fd_sync_on = sysdate 
					WHERE id_pengambilan_mk = {$id_pengambilan_mk}");
				} else {
					$result['message'] = ($index_proses + 1) . " Update {$mhs} Nilai: {$nilai_huruf} ({$nilai_angka}) : Gagal. ";
					$result['message'] .= json_encode($update_result);
					$result['message'] .= "\n" . json_encode([$id_reg_pd,$id_kls]);
				}

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// meneruskan index proses ditambah lagi dengan jumlah data insert
				$index_proses += $jumlah_insert;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Proses Delete
			// --------------------------------
			else if ($index_proses < ($jumlah_insert + $jumlah_update + $jumlah_delete)) {
				// index berjalan dikurangi jumlah data insert+update utk mendapatkan index delete
				$index_proses -= ($jumlah_insert + $jumlah_update);

				// Proses dalam bentuk key lowercase
				$data_delete = array_change_key_case($data_delete_set[$index_proses], CASE_LOWER);

				// Simpan informasi mahasiswa
				$id_pengambilan_mk	= $data_delete['id_pengambilan_mk'];
				$mhs				= $data_delete['mhs'];
				$id_kls				= $data_delete['id_kelas_kuliah'];
				$id_reg_pd			= $data_delete['id_registrasi_mahasiswa'];
				$nilai_huruf		= $data_delete['nilai_huruf'];
				$nilai_angka		= $data_delete['nilai_angka'];
				$nilai_indeks		= $data_delete['nilai_indeks'];
				$nama_kelas			= $data_delete['nama_kelas'];

				// Hilangkan data tdk diperlukan untuk delete
				// unset($data_delete['id_pengambilan_mk']);
				// unset($data_delete['mhs']);
				// unset($data_delete['id_kls']);
				// unset($data_delete['id_reg_pd']);
				// unset($data_delete['nama_kelas']);

				// Build data format
				// $data_delete_json = array(
				// 	'id_kls' => $id_kls,
				// 	'id_reg_pd' => $id_reg_pd
				// );

				// Delete ke Feeder nilai
				// $delete_result = $this->feeder->DeleteRecord($this->token, FEEDER_NILAI, json_encode($data_delete_json));

				// Jika tidak ada masalah delete
				// if ($delete_result['result']['error_code'] == 0) {
				$result['message'] = ($index_proses + 1) . " Delete {$nama_kelas} {$mhs} Nilai = {$nilai_angka} ({$nilai_huruf} : $nilai_indeks) : Berhasil";

				// Update status sync di set null
				$this->rdb->Query("UPDATE pengambilan_mk_del SET fd_sync_on = NULL WHERE id_pengambilan_mk = {$id_pengambilan_mk}");
				// } else {
				// 	$result['message'] = ($index_proses + 1) . " Delete {$mhs} Nilai: {$nilai_huruf} ({$nilai_angka}) : Gagal. ";
				// 	$result['message'] .= "({$delete_result['result']['error_code']}) {$delete_result['result']['error_desc']}";
				// 	$result['message'] .= "\n" . json_encode($data_delete_json);
				// }

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// meneruskan index proses ditambah lagi dengan jumlah data insert+update
				$index_proses += ($jumlah_insert + $jumlah_update);

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Selesai
			// --------------------------------
			else {
				$result['message'] = "Selesai";
				$result['status'] = SYNC_STATUS_DONE;
			}
		}

		echo json_encode($result);
	}

	private function proses_nilai_transfer()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		// -----------------------------------
		// Ambil data untuk Insert
		// -----------------------------------
		if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			$id_smt		= $this->input->post('semester');
			// Konversi format semester ke id_semester
			$sql_semester_raw = file_get_contents(APPPATH . 'models/sql/semester-konversi.sql');
			$sql_semester = strtr($sql_semester_raw, array('@npsn' => $this->satuan_pendidikan['npsn'], '@id_smt' => $id_smt));
			$semester_langitan = $this->rdb->QueryToArray($sql_semester);
			$id_semester = $semester_langitan[0]['ID_SEMESTER'];
			// Ambil nilai transfer yang akan di insert
			$sql_nilai_transfer_raw = file_get_contents(APPPATH . 'models/sql/nilai-transfer-insert.sql');
			$sql_nilai_transfer = strtr($sql_nilai_transfer_raw, array(
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@smt' => $id_semester
			));
			$data_set = $this->rdb->QueryToArray($sql_nilai_transfer);

			$this->session->set_userdata('data_insert_set', $data_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Entri. Jumlah data: ' . count($data_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_AMBIL_DATA_LANGITAN_2;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Ambil data untuk Update
		// -----------------------------------
		else if ($mode == MODE_AMBIL_DATA_LANGITAN_2) {
			$id_smt		= $this->input->post('semester');
			// Konversi format semester ke id_semester
			$sql_semester_raw = file_get_contents(APPPATH . 'models/sql/semester-konversi.sql');
			$sql_semester = strtr($sql_semester_raw, array('@npsn' => $this->satuan_pendidikan['npsn'], '@id_smt' => $id_smt));
			$semester_langitan = $this->rdb->QueryToArray($sql_semester);
			$id_semester = $semester_langitan[0]['ID_SEMESTER'];
			// Ambil nilai transfer yang akan di insert
			$sql_nilai_transfer_raw = file_get_contents(APPPATH . 'models/sql/nilai-transfer-update.sql');
			$sql_nilai_transfer = strtr($sql_nilai_transfer_raw, array(
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@smt' => $id_semester
			));
			$data_set = $this->rdb->QueryToArray($sql_nilai_transfer);

			$this->session->set_userdata('data_update_set', $data_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Update. Jumlah data: ' . count($data_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_AMBIL_DATA_LANGITAN_3;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Ambil data untuk Delete
		// -----------------------------------
		else if ($mode == MODE_AMBIL_DATA_LANGITAN_3) {
			// Parameter
			$id_smt		= $this->input->post('semester');

			// Konversi format semester ke id_semester
			$sql_semester_raw = file_get_contents(APPPATH . 'models/sql/semester-konversi.sql');
			$sql_semester = strtr($sql_semester_raw, array('@npsn' => $this->satuan_pendidikan['npsn'], '@id_smt' => $id_smt));
			$semester_langitan = $this->rdb->QueryToArray($sql_semester);
			$id_semester = $semester_langitan[0]['ID_SEMESTER'];

			// Ambil peserta kuliah yg akan di update
			$sql_nilai_raw = file_get_contents(APPPATH . 'models/sql/nilai-transfer-delete.sql');
			$sql_nilai = strtr($sql_nilai_raw, array(
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@smt' => $id_semester
			));
			$data_set = $this->rdb->QueryToArray($sql_nilai);

			$this->session->set_userdata('data_delete_set', $data_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Delete. Jumlah data: ' . count($data_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Proses Sinkronisasi dari data yg sudah diambil
		// -----------------------------------
		else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil data insert
			$data_insert_set = $this->session->userdata('data_insert_set');
			$jumlah_insert = count($data_insert_set);

			// Ambil data update
			$data_update_set = $this->session->userdata('data_update_set');
			$jumlah_update = count($data_update_set);

			// Ambil data delete
			$data_delete_set = $this->session->userdata('data_delete_set');
			$jumlah_delete = count($data_delete_set);

			// --------------------------------
			// Proses Insert
			// --------------------------------
			if ($index_proses < $jumlah_insert) {
				// Proses dalam bentuk key lowercase
				$data_insert = array_change_key_case($data_insert_set[$index_proses], CASE_LOWER);

				// Simpan untuk tampilan sync
				$id_pengambilan_mk	= $data_insert['id_pengambilan_mk_konversi'];
				$mhs				= $data_insert['mhs'];
				$kode_mk_asal		= $data_insert['kode_mata_kuliah_asal'];
				$nm_mk_asal			= $data_insert['nama_mata_kuliah_asal'];
				$sks_asal			= $data_insert['sks_mata_kuliah_asal'];
				$sks_diakui			= $data_insert['sks_mata_kuliah_diakui'];
				$nilai_huruf_asal	= $data_insert['nilai_huruf_asal'];
				$nilai_huruf_diakui	= $data_insert['nilai_huruf_diakui'];

				// Hilangkan variabel tak dibutuhkan
				unset($data_insert['id_pengambilan_mk_konversi']);
				unset($data_insert['mhs']);
				unset($data_insert['id_sp']);
				if($data_insert['is_migrasi_hukum'] == 1)
					$data_insert['id_semester'] = '20221';

				// Entri ke Feeder nilai transfer
				$insert_result = $this->feederws->InsertNilaiTransferPendidikanMahasiswa($data_insert);

				// Jika berhasil insert, terdapat return id_ekuivalensi
				if (isset($insert_result['data']['id_transfer'])) {
					// Pesan Insert, tampilkan nama mahasiswa
					$result['message'] = ($index_proses + 1) . " Insert {$mhs} {$kode_mk_asal} {$nm_mk_asal} ({$sks_asal} SKS) = {$nilai_huruf_asal} diakui ({$sks_diakui} SKS) {$nilai_huruf_diakui} : Berhasil";

					// Update status sync
					$this->rdb->Query("UPDATE pengambilan_mk_konversi SET fd_id_ekuivalensi = '{$insert_result['data']['id_transfer']}', fd_sync_on = sysdate WHERE id_pengambilan_mk_konversi = {$id_pengambilan_mk}");
				} else // saat insert nilai transfer gagal
				{
					// Pesan insert jika gagal
					$result['message'] = ($index_proses + 1) . " Insert {$mhs} {$kode_mk_asal} {$nm_mk_asal} ({$sks_asal} SKS) = {$nilai_huruf_asal} diakui ({$sks_diakui} SKS) {$nilai_huruf_diakui} : Gagal. " . json_encode($insert_result);
					// $result['message'] .= "\n" . json_encode($data_insert);
				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Proses Update
			// --------------------------------
			else if ($index_proses < ($jumlah_insert + $jumlah_update)) {
				// index berjalan dikurangi jumlah data insert utk mendapatkan index update
				$index_proses -= $jumlah_insert;

				// Proses dalam bentuk key lowercase
				$data_update = array_change_key_case($data_update_set[$index_proses], CASE_LOWER);

				// Simpan untuk tampilan sync
				$id_pengambilan_mk	= $data_update['id_pengambilan_mk_konversi'];
				$mhs				= $data_update['mhs'];
				$kode_mk_asal		= $data_update['kode_mata_kuliah_asal'];
				$nm_mk_asal			= $data_update['nama_mata_kuliah_asal'];
				$sks_asal			= $data_update['sks_mata_kuliah_asal'];
				$sks_diakui			= $data_update['sks_mata_kuliah_diakui'];
				$nilai_huruf_asal	= $data_update['nilai_huruf_asal'];
				$nilai_huruf_diakui	= $data_update['nilai_huruf_diakui'];
				$id_ekuivalensi		= $data_update['id_transfer'];

				// Hilangkan variabel tak dibutuhkan
				unset($data_update['id_pengambilan_mk_konversi']);
				unset($data_update['mhs']);
				unset($data_update['id_transfer']);
				unset($data_update['id_sp']);

				// Build data format
				$data_update_json = array(
					'key'	=> array('id_transfer' => $id_ekuivalensi),
					'data'	=> $data_update
				);

				// Update ke Feeder Nilai Transfer
				$update_result = $this->feederws->UpdateNilaiTransferPendidikanMahasiswa($data_update_json["key"], $data_update_json["data"]);

				// Jika tidak ada masalah update
				if ($update_result['error_code'] == 0) {
					$result['message'] = ($index_proses + 1) . " Update {$mhs} {$kode_mk_asal} {$nm_mk_asal} ({$sks_asal} SKS) = {$nilai_huruf_asal} diakui ({$sks_diakui} SKS) {$nilai_huruf_diakui} : Berhasil";

					// Update status sync
					$this->rdb->Query("UPDATE pengambilan_mk_konversi SET fd_sync_on = sysdate WHERE id_pengambilan_mk_konversi = {$id_pengambilan_mk}");
				} else {
					$result['message'] = ($index_proses + 1) . " Update {$mhs} {$kode_mk_asal} {$nm_mk_asal} ({$sks_asal} SKS) = {$nilai_huruf_asal} diakui ({$sks_diakui} SKS) {$nilai_huruf_diakui} : Gagal. ";
					$result['message'] .= json_encode($update_result);
					$result['message'] .= "\n" . json_encode($data_update_json);
				}

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// meneruskan index proses ditambah lagi dengan jumlah data insert
				$index_proses += $jumlah_insert;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Proses Delete
			// --------------------------------
			else if ($index_proses < ($jumlah_insert + $jumlah_update + $jumlah_delete)) {
				// index berjalan dikurangi jumlah data insert+update utk mendapatkan index delete
				$index_proses -= ($jumlah_insert + $jumlah_update);

				// Proses dalam bentuk key lowercase
				$data_delete = array_change_key_case($data_delete_set[$index_proses], CASE_LOWER);

				// Simpan untuk tampilan sync

				$id_pengambilan_mk_konv_del	= $data_delete['id_pengambilan_mk_konv_del'];
				$mhs				= $data_delete['mhs'];
				$kode_mk_asal		= $data_delete['kode_mata_kuliah_asal'];
				$nm_mk_asal			= $data_delete['nama_mata_kuliah_asal'];
				$id_transfer		= $data_delete['id_transfer'];

				// Hilangkan variabel tak dibutuhkan
				unset($data_delete['id_pengambilan_mk_konv_del']);
				unset($data_delete['mhs']);

				// Build data format
				$key = array(
					'id_transfer' => $id_transfer
				);

				// Delete ke Feeder nilai transfer
				$delete_result = $this->feederws->DeleteNilaiTransferPendidikanMahasiswa($key);

				// Jika tidak ada masalah delete
				if ($delete_result['error_code'] == 0) {
					$result['message'] = ($index_proses + 1) . " Delete {$mhs} {$kode_mk_asal} {$nm_mk_asal} : Berhasil";

					// Update status sync di set null
					$this->rdb->Query("UPDATE pengambilan_mk_konv_del SET fd_sync_on = NULL WHERE id_pengambilan_mk_konv_del = {$id_pengambilan_mk_konv_del}");
				} else {
					$result['message'] = ($index_proses + 1) . " Delete {$mhs} {$kode_mk_asal} {$nm_mk_asal} : Gagal. ";
					$result['message'] .= json_encode($delete_result);
					$result['message'] .= "\n" . json_encode($key);
				}

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// meneruskan index proses ditambah lagi dengan jumlah data insert+update
				$index_proses += ($jumlah_insert + $jumlah_update);

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Selesai
			// --------------------------------
			else {
				$result['message'] = "Selesai";
				$result['status'] = SYNC_STATUS_DONE;
			}
		}

		echo json_encode($result);
	}

	private function proses_nilai_transfer_umaha()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		// -----------------------------------
		// Ambil data untuk Insert
		// -----------------------------------
		if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			// Ambil data nilai transfer kuliah yg akan insert
			$sql_nilai_transfer_umaha_raw = file_get_contents(APPPATH . 'models/sql/nilai-transfer-umaha-insert.sql');
			$data_set = $this->rdb->QueryToArray($sql_nilai_transfer_umaha_raw);

			$this->session->set_userdata('data_insert_set', $data_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Entri. Jumlah data: ' . count($data_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_AMBIL_DATA_LANGITAN_2;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Ambil data untuk Update
		// -----------------------------------
		else if ($mode == MODE_AMBIL_DATA_LANGITAN_2) {
			// Ambil peserta kuliah yg akan di update
			$sql_nilai_transfer_umaha_raw = file_get_contents(APPPATH . 'models/sql/nilai-transfer-umaha-update.sql');
			$data_set = $this->rdb->QueryToArray($sql_nilai_transfer_umaha_raw);

			$this->session->set_userdata('data_update_set', $data_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Update. Jumlah data: ' . count($data_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_AMBIL_DATA_LANGITAN_3;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Ambil data untuk Delete
		// -----------------------------------
		else if ($mode == MODE_AMBIL_DATA_LANGITAN_3) {
			// Ambil peserta kuliah yg akan di update
			$sql_nilai_transfer_umaha_raw = file_get_contents(APPPATH . 'models/sql/nilai-transfer-umaha-delete.sql');
			$data_set = $this->rdb->QueryToArray($sql_nilai_transfer_umaha_raw);

			$this->session->set_userdata('data_delete_set', $data_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Delete. Jumlah data: ' . count($data_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Proses Sinkronisasi dari data yg sudah diambil
		// -----------------------------------
		else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil data insert
			$data_insert_set = $this->session->userdata('data_insert_set');
			$jumlah_insert = count($data_insert_set);

			// Ambil data update
			$data_update_set = $this->session->userdata('data_update_set');
			$jumlah_update = count($data_update_set);

			// Ambil data delete
			$data_delete_set = $this->session->userdata('data_delete_set');
			$jumlah_delete = count($data_delete_set);

			// --------------------------------
			// Proses Insert
			// --------------------------------
			if ($index_proses < $jumlah_insert) {
				// Proses dalam bentuk key lowercase
				$data_insert = array_change_key_case($data_insert_set[$index_proses], CASE_LOWER);

				// Simpan untuk tampilan sync
				$id_pengambilan_mk	= $data_insert['id_pengambilan_mk'];
				$mhs				= $data_insert['mhs'];
				$kode_mk_asal		= $data_insert['kode_mk_asal'];
				$nm_mk_asal			= $data_insert['nm_mk_asal'];
				$sks_asal			= $data_insert['sks_asal'];
				$sks_diakui			= $data_insert['sks_diakui'];
				$nilai_huruf_asal	= $data_insert['nilai_huruf_asal'];
				$nilai_huruf_diakui	= $data_insert['nilai_huruf_diakui'];

				// Hilangkan variabel tak dibutuhkan
				unset($data_insert['id_pengambilan_mk']);
				unset($data_insert['mhs']);

				// Entri ke Feeder nilai transfer
				$insert_result = $this->feeder->InsertRecord($this->token, FEEDER_NILAI_TRANSFER, json_encode($data_insert));

				// Jika berhasil insert, terdapat return id_ekuivalensi
				if (isset($insert_result['result']['id_ekuivalensi'])) {
					// Pesan Insert, tampilkan nama mahasiswa
					$result['message'] = ($index_proses + 1) . " Insert {$mhs} {$kode_mk_asal} {$nm_mk_asal} ({$sks_asal} SKS) = {$nilai_huruf_asal} diakui ({$sks_diakui} SKS) {$nilai_huruf_diakui} : Berhasil";

					// Update status sync
					$this->rdb->Query("UPDATE pengambilan_mk SET fd_id_ekuivalensi = '{$insert_result['result']['id_ekuivalensi']}', fd_sync_on = sysdate WHERE id_pengambilan_mk = {$id_pengambilan_mk}");
				} else // saat insert nilai transfer gagal
				{
					// Pesan insert jika gagal
					$result['message'] = ($index_proses + 1) . " Insert {$mhs} {$kode_mk_asal} {$nm_mk_asal} ({$sks_asal} SKS) = {$nilai_huruf_asal} diakui ({$sks_diakui} SKS) {$nilai_huruf_diakui} : Gagal. " . json_encode($insert_result['result']);
					$result['message'] .= "\n" . json_encode($data_insert);
				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Proses Update
			// --------------------------------
			else if ($index_proses < ($jumlah_insert + $jumlah_update)) {
				// index berjalan dikurangi jumlah data insert utk mendapatkan index update
				$index_proses -= $jumlah_insert;

				// Proses dalam bentuk key lowercase
				$data_update = array_change_key_case($data_update_set[$index_proses], CASE_LOWER);

				// Simpan untuk tampilan sync
				$id_pengambilan_mk	= $data_update['id_pengambilan_mk'];
				$mhs				= $data_update['mhs'];
				$kode_mk_asal		= $data_update['kode_mk_asal'];
				$nm_mk_asal			= $data_update['nm_mk_asal'];
				$sks_asal			= $data_update['sks_asal'];
				$sks_diakui			= $data_update['sks_diakui'];
				$nilai_huruf_asal	= $data_update['nilai_huruf_asal'];
				$nilai_huruf_diakui	= $data_update['nilai_huruf_diakui'];
				$id_ekuivalensi		= $data_update['id_ekuivalensi'];

				// Hilangkan variabel tak dibutuhkan
				unset($data_update['id_pengambilan_mk']);
				unset($data_update['mhs']);
				unset($data_update['id_ekuivalensi']);

				// Build data format
				$data_update_json = array(
					'key'	=> array('id_ekuivalensi' => $id_ekuivalensi),
					'data'	=> $data_update
				);

				// Update ke Feeder Nilai Transfer
				$update_result = $this->feeder->UpdateRecord($this->token, FEEDER_NILAI_TRANSFER, json_encode($data_update_json));

				// Jika tidak ada masalah update
				if ($update_result['result']['error_code'] == 0) {
					$result['message'] = ($index_proses + 1) . " Update {$mhs} {$kode_mk_asal} {$nm_mk_asal} ({$sks_asal} SKS) = {$nilai_huruf_asal} diakui ({$sks_diakui} SKS) {$nilai_huruf_diakui} : Berhasil";

					// Update status sync
					$this->rdb->Query("UPDATE pengambilan_mk SET fd_sync_on = sysdate WHERE id_pengambilan_mk = {$id_pengambilan_mk}");
				} else {
					$result['message'] = ($index_proses + 1) . " Update {$mhs} {$kode_mk_asal} {$nm_mk_asal} ({$sks_asal} SKS) = {$nilai_huruf_asal} diakui ({$sks_diakui} SKS) {$nilai_huruf_diakui} : Gagal. ";
					$result['message'] .= "({$update_result['result']['error_code']}) {$update_result['result']['error_desc']}";
					$result['message'] .= "\n" . json_encode($data_update_json);
				}

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// meneruskan index proses ditambah lagi dengan jumlah data insert
				$index_proses += $jumlah_insert;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Proses Delete
			// --------------------------------
			else if ($index_proses < ($jumlah_insert + $jumlah_update + $jumlah_delete)) {
				// index berjalan dikurangi jumlah data insert+update utk mendapatkan index delete
				$index_proses -= ($jumlah_insert + $jumlah_update);

				// Proses dalam bentuk key lowercase
				$data_delete = array_change_key_case($data_delete_set[$index_proses], CASE_LOWER);

				// Simpan untuk tampilan sync
				$id_pengambilan_mk	= $data_delete['id_pengambilan_mk'];
				$mhs				= $data_delete['mhs'];
				$kode_mk_asal		= $data_delete['kode_mk_asal'];
				$nm_mk_asal			= $data_delete['nm_mk_asal'];
				$sks_asal			= $data_delete['sks_asal'];
				$sks_diakui			= $data_delete['sks_diakui'];
				$nilai_huruf_asal	= $data_delete['nilai_huruf_asal'];
				$nilai_huruf_diakui	= $data_delete['nilai_huruf_diakui'];
				$id_ekuivalensi		= $data_delete['id_ekuivalensi'];

				// Hilangkan variabel tak dibutuhkan
				unset($data_delete['id_pengambilan_mk']);
				unset($data_delete['mhs']);

				// Build data format
				$data_delete_json = array(
					'id_ekuivalensi' => $id_ekuivalensi
				);

				// Delete ke Feeder nilai transfer
				$delete_result = $this->feeder->DeleteRecord($this->token, FEEDER_NILAI_TRANSFER, json_encode($data_delete_json));

				// Jika tidak ada masalah delete
				if ($delete_result['result']['error_code'] == 0) {
					$result['message'] = ($index_proses + 1) . " Delete {$mhs} {$kode_mk_asal} {$nm_mk_asal} ({$sks_asal} SKS) = {$nilai_huruf_asal} diakui ({$sks_diakui} SKS) {$nilai_huruf_diakui} : Berhasil";

					// Update status sync di set null
					$this->rdb->Query("UPDATE pengambilan_mk_del SET fd_sync_on = NULL WHERE id_pengambilan_mk = {$id_pengambilan_mk}");
				} else {
					$result['message'] = ($index_proses + 1) . " Delete {$mhs} {$kode_mk_asal} {$nm_mk_asal} ({$sks_asal} SKS) = {$nilai_huruf_asal} diakui ({$sks_diakui} SKS) {$nilai_huruf_diakui} : Gagal. ";
					$result['message'] .= "({$delete_result['result']['error_code']}) {$delete_result['result']['error_desc']}";
					$result['message'] .= "\n" . json_encode($data_delete_json);
				}

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// meneruskan index proses ditambah lagi dengan jumlah data insert+update
				$index_proses += ($jumlah_insert + $jumlah_update);

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Selesai
			// --------------------------------
			else {
				$result['message'] = "Selesai";
				$result['status'] = SYNC_STATUS_DONE;
			}
		}

		echo json_encode($result);
	}

	/**
	 * Ajax-GET /sync/ambil_kelas/ Ambil kelas untuk nilai perkuliahan
	 * @param string $kode_prodi Kode program studi versi Feeder
	 * @param string $id_smt Kode semester versi Feeder
	 */
	function ambil_kelas($kode_prodi, $id_smt)
	{
		// Konversi format semester ke id_semester
		$sql_semester_raw = file_get_contents(APPPATH . 'models/sql/semester-konversi.sql');
		$sql_semester = strtr($sql_semester_raw, array('@npsn' => $this->satuan_pendidikan['npsn'], '@id_smt' => $id_smt));
		$semester_langitan = $this->rdb->QueryToArray($sql_semester);
		$id_semester = $semester_langitan[0]['ID_SEMESTER'];

		// Ambil data kelas yang ada di langitan yg sudah ter-sync (sudah ada fd_id_kls di kelas_mk)
		$sql_ambil_kelas_raw = file_get_contents(APPPATH . 'models/sql/ambil-kelas-kuliah.sql');
		$sql_ambil_kelas = strtr($sql_ambil_kelas_raw, array('@npsn' => $this->satuan_pendidikan['npsn'], '@kode_prodi' => $kode_prodi, '@smt' => $id_semester));
		$data_set = $this->rdb->QueryToArray($sql_ambil_kelas);

		echo json_encode($data_set);
	}


	/**
	 * GET /sync/kuliah_mahasiswa/
	 */
	function kuliah_mahasiswa()
	{
		$jumlah = array();

		// Ambil jumlah kuliah mahasiswa di feeder. Sudah obsolete.
		$jumlah['feeder'] = '-';

		// SQL jumlah kuliah mahasiswa di sistem langitan & yg sudah link
		$sql_kuliah_mahasiswa = file_get_contents(APPPATH . 'models/sql/kuliah-mahasiswa.sql');
		$sql_kuliah_mahasiswa = strtr($sql_kuliah_mahasiswa, array(
			'@npsn' => $this->satuan_pendidikan['npsn']
		));

		// Ambil data
		$kuliah_mahasiswa_set = $this->rdb->QueryToArray($sql_kuliah_mahasiswa);

		$jumlah['langitan'] = $kuliah_mahasiswa_set[0]['JUMLAH'];
		$jumlah['linked'] = $kuliah_mahasiswa_set[1]['JUMLAH'];
		$jumlah['update'] = $kuliah_mahasiswa_set[2]['JUMLAH'];
		$jumlah['insert'] = $kuliah_mahasiswa_set[3]['JUMLAH'];
		$this->smarty->assign('jumlah', $jumlah);

		// Data program studi
		$this->smarty->assign('program_studi_set', $this->langitan_model->list_program_studi($this->satuan_pendidikan['npsn']));

		$this->smarty->assign('url_sync', site_url('sync/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync/' . $this->uri->segment(2) . '.tpl');
	}

	/**
	 * Ajax-GET /sync/kuliah_mahasiswa_data/
	 */
	function kuliah_mahasiswa_data($kode_prodi, $id_smt)
	{
		// Ambil jumlah kuliah mahasiswa di feeder
		$program_studi_set = $this->rdb->QueryToArray("SELECT FD_ID_SMS FROM PROGRAM_STUDI WHERE KODE_PROGRAM_STUDI = '{$kode_prodi}'");
		$id_sms = $program_studi_set[0]['FD_ID_SMS'];

		$filter = "id_prodi='{$id_sms}' and id_semester='{$id_smt}'";
		$response = $this->feederws->GetListKelasKuliah($filter);

		// Ambil id semester
		$id_semester = $this->langitan_model->get_id_semester($this->satuan_pendidikan['npsn'], $id_smt);

		// SQL jumlah kuliah mahasiswa di Sistem Langitan & yg sudah link
		$sql_kuliah_mahasiswa_data = file_get_contents(APPPATH . 'models/sql/kuliah-mahasiswa-data.sql');
		$sql_kuliah_mahasiswa_data = strtr($sql_kuliah_mahasiswa_data, array(
			'@npsn' => $this->satuan_pendidikan['npsn'],
			'@id_semester' => $id_semester,
			'@kode_prodi' => $kode_prodi
		));

		// Ambil jumlah mahasiswa di Sistem Langitan & yg sudah link
		$kuliah_mahasiswa_set = $this->rdb->QueryToArray($sql_kuliah_mahasiswa_data);

		$jumlah['feeder'] = count($response['data']);
		$jumlah['langitan'] = $kuliah_mahasiswa_set[0]['JUMLAH'];
		$jumlah['linked'] = $kuliah_mahasiswa_set[1]['JUMLAH'];
		$jumlah['update'] = $kuliah_mahasiswa_set[2]['JUMLAH'];
		$jumlah['insert'] = $kuliah_mahasiswa_set[3]['JUMLAH'];

		echo json_encode($jumlah);
	}

	private function proses_kuliah_mahasiswa()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		// -----------------------------------
		// Ambil data untuk Insert
		// -----------------------------------
		if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			$id_smt = $this->input->post('semester');
			$kode_prodi = $this->input->post('kode_prodi');

			$sql_kuliah_mahasiswa_insert = file_get_contents(APPPATH . 'models/sql/kuliah-mahasiswa-insert.sql');
			$sql_kuliah_mahasiswa_insert = strtr($sql_kuliah_mahasiswa_insert, array(
				'@npsn'		=> $this->satuan_pendidikan['npsn'],
				'@id_smt'	=> $id_smt,
				'@kode_prodi' => $kode_prodi
			));

			$kuliah_mahasiswa_set = $this->rdb->QueryToArray($sql_kuliah_mahasiswa_insert);

			$this->session->set_userdata('kuliah_mahasiswa_insert_set', $kuliah_mahasiswa_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Entri. Jumlah data: ' . count($kuliah_mahasiswa_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_AMBIL_DATA_LANGITAN_2;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Ambil data untuk Update
		// -----------------------------------
		else if ($mode == MODE_AMBIL_DATA_LANGITAN_2) {
			$id_smt = $this->input->post('semester');
			$kode_prodi = $this->input->post('kode_prodi');

			$sql_kuliah_mahasiswa_update = file_get_contents(APPPATH . 'models/sql/kuliah-mahasiswa-update.sql');
			$sql_kuliah_mahasiswa_update = strtr($sql_kuliah_mahasiswa_update, array(
				'@npsn'		=> $this->satuan_pendidikan['npsn'],
				'@id_smt'	=> $id_smt,
				'@kode_prodi' => $kode_prodi
			));

			$kuliah_mahasiswa_set = $this->rdb->QueryToArray($sql_kuliah_mahasiswa_update);

			// simpan ke cache
			$this->session->set_userdata('kuliah_mahasiswa_update_set', $kuliah_mahasiswa_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Update. Jumlah data: ' . count($kuliah_mahasiswa_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		}
		// ----------------------------------------------
		// Proses Sinkronisasi dari data yg sudah diambil
		// ----------------------------------------------
		else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$kuliah_mahasiswa_insert_set = $this->session->userdata('kuliah_mahasiswa_insert_set');
			$jumlah_insert = count($kuliah_mahasiswa_insert_set);

			// Ambil dari cache
			$kuliah_mahasiswa_update_set = $this->session->userdata('kuliah_mahasiswa_update_set');
			$jumlah_update = count($kuliah_mahasiswa_update_set);

			// --------------------------------
			// Proses Insert
			// --------------------------------
			if ($index_proses < $jumlah_insert) {
				// Proses dalam bentuk key lowercase
				$kuliah_mahasiswa_insert = array_change_key_case($kuliah_mahasiswa_insert_set[$index_proses], CASE_LOWER);

				// Simpan id_mhs_status & nim untuk update data di langitan
				$id_mhs_status	= $kuliah_mahasiswa_insert['id_mhs_status'];
				$nim_mhs		= $kuliah_mahasiswa_insert['nim_mhs'];

				// Hilangkan yang tidak diperlukan di tabel kuliah_mahasiswa
				unset($kuliah_mahasiswa_insert['id_mhs_status']);
				unset($kuliah_mahasiswa_insert['nim_mhs']);

				// Entri ke Feeder Kuliah Mahasiswa
				$insert_result = $this->feederws->InsertPerkuliahanMahasiswa($kuliah_mahasiswa_insert);

				// Jika berhasil insert, terdapat return id_smt dan id_reg_pd
				if (isset($insert_result['data']['id_semester']) && isset($insert_result['data']['id_registrasi_mahasiswa'])) {
					// Pesan Insert, tampilkan nim
					$result['message'] = ($index_proses + 1) . " Insert Aktivitas {$nim_mhs} : Berhasil";

					// Update status sync
					$this->rdb->Query("UPDATE mahasiswa_status 
					SET fd_id_smt = '{$insert_result['data']['id_semester']}', 
					fd_id_reg_pd = '{$insert_result['data']['id_registrasi_mahasiswa']}', 
					fd_sync_on = sysdate WHERE id_mhs_status = {$id_mhs_status}");
				} 
				else if($insert_result['error_code'] == 1260){
					$result['message'] = ($index_proses + 1) . " Insert Aktivitas {$nim_mhs} : Berhasil";

					// Update status sync
					$this->rdb->Query("UPDATE mahasiswa_status 
					SET fd_id_smt = '{$kuliah_mahasiswa_insert['id_semester']}', 
					fd_id_reg_pd = '{$kuliah_mahasiswa_insert['id_registrasi_mahasiswa']}', 
					fd_sync_on = sysdate WHERE id_mhs_status = {$id_mhs_status}");
				}
				else // saat insert kuliah_mahasiswa gagal
				{
					// Pesan Insert, NIM Mahasiswa
					$result['message'] = ($index_proses + 1) . " Insert {$nim_mhs} : Gagal. ";
					$result['message'] .= "({$insert_result['error_code']}) {$insert_result['error_desc']}";
				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Proses Update
			// --------------------------------
			else if ($index_proses < ($jumlah_insert + $jumlah_update)) {
				// index berjalan dikurangi jumlah data insert utk mendapatkan index update
				$index_proses -= $jumlah_insert;

				// Proses dalam bentuk key lowercase
				$kuliah_mahasiswa_update = array_change_key_case($kuliah_mahasiswa_update_set[$index_proses], CASE_LOWER);

				// Simpan id_mhs_status untuk keperluan update di langitan
				$id_mhs_status = $kuliah_mahasiswa_update['id_mhs_status'];
				$nim_mhs = $kuliah_mahasiswa_update['nim_mhs'];
				$id_smt = $kuliah_mahasiswa_update['id_semester'];
				$id_reg_pd = $kuliah_mahasiswa_update['id_registrasi_mahasiswa'];

				// Hilangkan id_mhs_status & nim_mhs
				unset($kuliah_mahasiswa_update['id_mhs_status']);
				unset($kuliah_mahasiswa_update['nim_mhs']);
				unset($kuliah_mahasiswa_update['id_semester']);
				unset($kuliah_mahasiswa_update['id_registrasi_mahasiswa']);

				// Update ke Feeder Kuliah Mahasiswa
				$update_result = $this->feederws->UpdatePerkuliahanMahasiswa(
					['id_registrasi_mahasiswa' => $id_reg_pd, 'id_semester' => $id_smt],
					$kuliah_mahasiswa_update
				);

				// Jika tidak ada masalah update
				if ($update_result['error_code'] == 0) {
					$result['message'] = ($index_proses + 1) . " Update {$nim_mhs} : Berhasil";

					$this->rdb->Query("UPDATE mahasiswa_status SET fd_sync_on = sysdate WHERE id_mhs_status = {$id_mhs_status}");
				} else {
					$result['message'] = ($index_proses + 1) . " Update {$nim_mhs} : Gagal. ";
					$result['message'] .= "({$update_result['error_code']}) {$update_result['error_desc']}";
				}

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// meneruskan index proses ditambah lagi dengan jumlah data insert
				$index_proses += $jumlah_insert;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Selesai
			// --------------------------------
			else {
				$result['message'] = "Selesai";
				$result['status'] = SYNC_STATUS_DONE;
			}
		}

		echo json_encode($result);
	}

	/**
	 * Mahasiswa Lulus / DO / Keluar
	 */
	public function lulusan_do_data($kode_prodi)
	{
		// Ambil jumlah mata kuliah di feeder. Sudah obsolete.
		$jumlah['feeder'] = '-';

		// Ambil data mahasiswa lulus
		$sql_lulusan_raw = file_get_contents(APPPATH . 'models/sql/lulusan-do-data.sql');
		$sql_lulusan = strtr($sql_lulusan_raw, array(
			'@npsn' => $this->satuan_pendidikan['npsn'],
			'@kode_prodi' => $kode_prodi
		));
		$mhs_set = $this->rdb->QueryToArray($sql_lulusan);

		$jumlah['langitan'] = $mhs_set[0]['JUMLAH'];
		$jumlah['linked'] = $mhs_set[1]['JUMLAH'];
		$jumlah['update'] = $mhs_set[2]['JUMLAH'];
		$jumlah['insert'] = $mhs_set[3]['JUMLAH'];

		echo json_encode($jumlah);
	}

	public function lulusan_do()
	{
		$jumlah = array();

		// Jumlah lulusan tidak bisa di dapat di feeder
		// $response = $this->feeder->GetCountRecordset($this->token, FEEDER_MAHASISWA_PT, "p.id_jns_keluar is not null");
		$jumlah['feeder'] = "-";

		// Ambil data mahasiswa lulus
		$sql_lulusan_raw = file_get_contents(APPPATH . 'models/sql/lulusan-do.sql');
		$sql_lulusan = strtr($sql_lulusan_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$mhs_set = $this->rdb->QueryToArray($sql_lulusan);

		$jumlah['langitan'] = $mhs_set[0]['JUMLAH'];
		$jumlah['linked'] = $mhs_set[1]['JUMLAH'];
		$jumlah['update'] = $mhs_set[2]['JUMLAH'];
		$jumlah['insert'] = $mhs_set[3]['JUMLAH'];

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		$this->smarty->assign('url_sync', site_url('sync/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync/' . $this->uri->segment(2) . '.tpl');
	}

	private function proses_lulusan_do()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		// -----------------------------------
		// Ambil data untuk Insert
		// -----------------------------------
		if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			$kode_prodi = $this->input->post('kode_prodi');

			$sql_lulusan_do_insert_raw = file_get_contents(APPPATH . 'models/sql/lulusan-do-insert.sql');
			$sql_lulusan_do_insert = strtr($sql_lulusan_do_insert_raw, [
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@kode_prodi' => $kode_prodi
			]);

			$lulusan_do_set = $this->rdb->QueryToArray($sql_lulusan_do_insert);

			$this->session->set_userdata('lulusan_do_insert_set', $lulusan_do_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Entri. Jumlah data: ' . count($lulusan_do_set);
			$result['status'] = SYNC_STATUS_PROSES;
			// $result['status'] = SYNC_STATUS_DONE;

			// ganti parameter
			$_POST['mode'] = MODE_AMBIL_DATA_LANGITAN_2;
			$result['params'] = http_build_query($_POST);
		}
		// -----------------------------------
		// Ambil data untuk Update
		// -----------------------------------
		else if ($mode == MODE_AMBIL_DATA_LANGITAN_2) {
			$kode_prodi = $this->input->post('kode_prodi');

			$sql_lulusan_do_update_raw = file_get_contents(APPPATH . 'models/sql/lulusan-do-update.sql');
			$sql_lulusan_do_update = strtr($sql_lulusan_do_update_raw, [
				'@npsn' => $this->satuan_pendidikan['npsn'],
				'@kode_prodi' => $kode_prodi
			]);

			$lulusan_do_set = $this->rdb->QueryToArray($sql_lulusan_do_update);

			// simpan ke cache
			$this->session->set_userdata('lulusan_do_update_set', $lulusan_do_set);

			$result['message'] = 'Ambil data Sistem Langitan yang akan di proses Update. Jumlah data: ' . count($lulusan_do_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		}
		// ----------------------------------------------
		// Proses Sinkronisasi dari data yg sudah diambil
		// ----------------------------------------------
		else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$lulusan_do_insert_set = $this->session->userdata('lulusan_do_insert_set');
			$jumlah_insert = count($lulusan_do_insert_set);

			// Ambil dari cache
			$lulusan_do_update_set = $this->session->userdata('lulusan_do_update_set');
			$jumlah_update = count($lulusan_do_update_set);

			// --------------------------------
			// Proses Insert. Insert lulusan 
			// --------------------------------
			if ($index_proses < $jumlah_insert) {
				// Proses dalam bentuk key lowercase
				$lulusan_do_insert = array_change_key_case($lulusan_do_insert_set[$index_proses], CASE_LOWER);

				// Simpan id_admisi & nim untuk update data di langitan
				$nim_mhs	= $lulusan_do_insert['nim_mhs'];
				$id_admisi	= $lulusan_do_insert['id_admisi'];
				$nm_status	= $lulusan_do_insert['nm_status_pengguna'];

				// Hilangkan yang tidak diperlukan di tabel mahasiswa_pt
				unset($lulusan_do_insert['nim_mhs']);
				unset($lulusan_do_insert['id_admisi']);
				unset($lulusan_do_insert['nm_status_pengguna']);

				// reformat data
				$lulusan_do_insert['ipk'] = floatval($lulusan_do_insert['ipk']);

				// Update ke Feeder Mahasiswa PT
				//$update_result = $this->feeder->UpdateRecord($this->token, FEEDER_MAHASISWA_PT, json_encode($data_update));

				// Insert ke Feeder Mahasiswa Lulus DO menggunakan Webservice 2
				$insert_result = json_decode(
					$this->feederws->runWS(json_encode([
						'token'     => $this->session->userdata('token'),
						'act'       => 'InsertMahasiswaLulusDO',
						'record'    => $lulusan_do_insert
					])),
					true
				);

				// Jika berhasil update, tidak ada error
				if ($insert_result['error_code'] == 0) {
					// Pesan Insert lulusan, tampilkan nim
					$result['message'] = ($index_proses + 1) . " Insert {$nim_mhs} {$nm_status} : Berhasil";

					// Update status sync
					$this->rdb->Query("UPDATE admisi SET fd_sync_on = sysdate WHERE id_admisi = {$id_admisi}");
				} else // saat update mahasiswa_pt gagal
				{
					// Tampilkan pesan gagal
					$result['message'] = ($index_proses + 1) . " Insert {$nim_mhs} {$nm_status} error : ";
					$result['message'] .= $insert_result['error_desc'] . "\n";
					$result['message'] .= json_encode($lulusan_do_insert);
				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Proses Update
			// --------------------------------
			else if ($index_proses < ($jumlah_insert + $jumlah_update)) {
				// index berjalan dikurangi jumlah data insert utk mendapatkan index update
				$index_proses -= $jumlah_insert;

				// Proses dalam bentuk key lowercase
				$lulusan_do_update = array_change_key_case($lulusan_do_update_set[$index_proses], CASE_LOWER);

				// Simpan variabel untuk keperluan update di langitan dan tampilan
				$id_admisi	= $lulusan_do_update['id_admisi'];
				$nim_mhs	= $lulusan_do_update['nim'];
				$id_reg_pd	= $lulusan_do_update['id_registrasi_mahasiswa'];
				$nm_status	= $lulusan_do_update['nm_status_pengguna'];

				// Hilangkan yang tidak diperlukan di tabel mahasiswa_pt
				unset($lulusan_do_update['id_admisi']);
				unset($lulusan_do_update['nm_status_pengguna']);

				// Build data format
				$data_update = array(
					'key'	=> array('id_registrasi_mahasiswa' => $id_reg_pd),
					'record'	=> $lulusan_do_update
				);

				$update_result = json_decode(
					$this->feederws->runWS(json_encode([
						'token'     => $this->session->userdata('token'),
						'act'       => 'UpdateMahasiswaLulusDO',
						'key'		=> array('id_registrasi_mahasiswa' => $id_reg_pd),
						'record'	=> $lulusan_do_update
					])),
					true
				);

				// Update ke Feeder Mahasiswa PT
				// $update_result = $this->feeder->UpdateRecord($this->token, FEEDER_MAHASISWA_PT, json_encode($data_update));

				// Jika berhasil update, tidak ada error
				if ($update_result['error_code'] == 0) {
					// Pesan Insert lulusan, tampilkan nim
					$result['message'] = ($index_proses + 1) . " Update {$nim_mhs} {$nm_status} : Berhasil";

					// Update status sync
					$this->rdb->Query("UPDATE admisi SET fd_sync_on = sysdate WHERE id_admisi = {$id_admisi}");
				} else // saat update mahasiswa_pt gagal
				{
					// Tampilkan pesan gagal
					$result['message'] = ($index_proses + 1) . " Update {$nim_mhs} {$nm_status} : " . json_encode($lulusan_do_update);
				}

				// Status proses
				$result['status'] = SYNC_STATUS_PROSES;

				// meneruskan index proses ditambah lagi dengan jumlah data insert
				$index_proses += $jumlah_insert;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			}
			// --------------------------------
			// Selesai
			// --------------------------------
			else {
				$result['message'] = "Selesai";
				$result['status'] = SYNC_STATUS_DONE;
			}
		}

		echo json_encode($result);
	}

	/**
	 * Tampilan awal sebelum start sinkronisasi
	 */
	function start($mode)
	{
		if ($mode == 'mahasiswa') {
			$kode_prodi	= $this->input->post('kode_prodi');
			$angkatan	= $this->input->post('angkatan');

			$program_studi_set = $this->rdb->QueryToArray(
				"SELECT nm_jenjang, nm_program_studi FROM program_studi ps
				JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
				JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
				WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' 
				AND ps.kode_program_studi = '{$kode_prodi}'
				AND ps.STATUS_AKTIF_PRODI=1"
			);

			$this->smarty->assign('jenis_sinkronisasi', 'Mahasiswa ' . $program_studi_set[0]['NM_JENJANG'] . ' ' . $program_studi_set[0]['NM_PROGRAM_STUDI'] . ' Angkatan ' . $angkatan);
			$this->smarty->assign('url', site_url('sync/proses/' . $mode));
		}

		if ($mode == 'riwayat_pendidikan_mahasiswa') {
			$kode_prodi	= $this->input->post('kode_prodi');
			$angkatan	= $this->input->post('angkatan');

			$program_studi_set = $this->rdb->QueryToArray(
				"SELECT nm_jenjang, nm_program_studi FROM program_studi ps
				JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
				JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
				WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' 
				AND ps.kode_program_studi = '{$kode_prodi}'
				AND ps.STATUS_AKTIF_PRODI=1"
			);

			$this->smarty->assign('jenis_sinkronisasi', 'Mahasiswa ' . $program_studi_set[0]['NM_JENJANG'] . ' ' . $program_studi_set[0]['NM_PROGRAM_STUDI'] . ' Angkatan ' . $angkatan);
			$this->smarty->assign('url', site_url('sync/proses/' . $mode));
		}

		if ($mode == 'mata_kuliah') {
			$kode_prodi	= $this->input->post('kode_prodi');

			$program_studi_set = $this->rdb->QueryToArray(
				"SELECT nm_jenjang, nm_program_studi FROM program_studi ps
				JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
				JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
				WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' 
				AND ps.kode_program_studi = '{$kode_prodi}'
				AND ps.STATUS_AKTIF_PRODI=1"
			);

			$this->smarty->assign('jenis_sinkronisasi', 'Mata Kuliah ' . $program_studi_set[0]['NM_JENJANG'] . ' ' . $program_studi_set[0]['NM_PROGRAM_STUDI']);
			$this->smarty->assign('url', site_url('sync/proses/' . $mode));
		}

		if ($mode == 'kurikulum') {
			$kode_prodi	= $this->input->post('kode_prodi');

			$program_studi_set = $this->rdb->QueryToArray(
				"SELECT nm_jenjang, nm_program_studi FROM program_studi ps
				JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
				JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
				WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' 
				AND ps.kode_program_studi = '{$kode_prodi}'
				AND ps.STATUS_AKTIF_PRODI=1"
			);

			$this->smarty->assign('jenis_sinkronisasi', 'Kurikulum ' . $program_studi_set[0]['NM_JENJANG'] . ' ' . $program_studi_set[0]['NM_PROGRAM_STUDI']);
			$this->smarty->assign('url', site_url('sync/proses/' . $mode));
		}

		if ($mode == 'mata_kuliah_kurikulum') {
			$kode_prodi			= $this->input->post('kode_prodi');
			$id_kurikulum_prodi	= $this->input->post('id_kurikulum');

			$program_studi_set = $this->rdb->QueryToArray(
				"SELECT j.nm_jenjang, ps.nm_program_studi, k.nm_kurikulum
				FROM kurikulum_mk kmk
				JOIN kurikulum k ON k.id_kurikulum = kmk.id_kurikulum
				JOIN program_studi ps ON ps.id_program_studi = kmk.id_program_studi
				JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
				JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
				WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' 
				AND ps.kode_program_studi = '{$kode_prodi}' 
				AND kmk.id_kurikulum = {$id_kurikulum_prodi}
				AND ps.STATUS_AKTIF_PRODI=1"
			);

			$this->smarty->assign('jenis_sinkronisasi', 'Mata Kuliah Kurikulum ' . $program_studi_set[0]['NM_KURIKULUM']);
			$this->smarty->assign('url', site_url('sync/proses/' . $mode));
		}

		if ($mode == 'kelas_kuliah') {
			$kode_prodi	= $this->input->post('kode_prodi');
			$id_smt		= $this->input->post('semester');

			$program_studi_set = $this->rdb->QueryToArray(
				"SELECT nm_jenjang, nm_program_studi FROM program_studi ps
				JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
				JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
				WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' AND ps.kode_program_studi = '{$kode_prodi}' AND STATUS_AKTIF_PRODI=1"
			);

			// Konversi format semester ke id_semester
			$semester_langitan = $this->rdb->QueryToArray(
				"SELECT thn_akademik_semester as thn, nm_semester FROM semester s
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = s.id_perguruan_tinggi
				WHERE 
					pt.npsn = '{$this->satuan_pendidikan['npsn']}' AND 
					s.fd_id_smt = '{$id_smt}'"
			);

			$this->smarty->assign('jenis_sinkronisasi', 'Kelas Perkuliahan ' . $program_studi_set[0]['NM_JENJANG'] . ' ' . $program_studi_set[0]['NM_PROGRAM_STUDI'] . ' - ' . $semester_langitan[0]['THN'] . '/' . ($semester_langitan[0]['THN'] + 1) . ' ' . $semester_langitan[0]['NM_SEMESTER']);
			$this->smarty->assign('url', site_url('sync/proses/' . $mode));
		}

		if ($mode == 'ajar_dosen') {
			$kode_prodi	= $this->input->post('kode_prodi');
			$id_smt		= $this->input->post('semester');

			$program_studi_set = $this->rdb->QueryToArray(
				"SELECT nm_jenjang, nm_program_studi FROM program_studi ps
				JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
				JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
				WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' 
				AND ps.kode_program_studi = '{$kode_prodi}'
				AND STATUS_AKTIF_PRODI=1"
			);

			// Konversi format semester ke id_semester
			$semester_langitan = $this->rdb->QueryToArray(
				"SELECT thn_akademik_semester as thn, nm_semester FROM semester s
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = s.id_perguruan_tinggi
				WHERE 
					pt.npsn = '{$this->satuan_pendidikan['npsn']}' AND 
					s.fd_id_smt = '{$id_smt}'"
			);

			$this->smarty->assign('jenis_sinkronisasi', 'Dosen Kelas ' . $program_studi_set[0]['NM_JENJANG'] . ' ' . $program_studi_set[0]['NM_PROGRAM_STUDI'] . ' - ' . $semester_langitan[0]['THN'] . '/' . ($semester_langitan[0]['THN'] + 1) . ' ' . $semester_langitan[0]['NM_SEMESTER']);
			$this->smarty->assign('url', site_url('sync/proses/' . $mode));
		}

		if ($mode == 'nilai') {
			$kode_prodi		= $this->input->post('kode_prodi');
			$id_smt			= $this->input->post('semester');
			$id_kelas_mk	= $this->input->post('id_kelas_mk');

			// Ambil informasi data kelas
			$kelas_set = $this->rdb->QueryToArray(
				"SELECT kls.id_kelas_mk, mk.kd_mata_kuliah||' '||mk.nm_mata_kuliah||' ('||nk.nama_kelas||') ' as nm_kelas, nm_jenjang, nm_program_studi
				FROM kelas_mk kls
				JOIN kurikulum_mk kmk ON kmk.id_kurikulum_mk = kls.id_kurikulum_mk
				JOIN mata_kuliah mk ON mk.id_mata_kuliah = kmk.id_mata_kuliah
				JOIN nama_kelas nk ON nk.id_nama_kelas = kls.no_kelas_mk
				JOIN program_studi ps on ps.id_program_studi= kls.id_program_studi
				JOIN jenjang j on j.id_jenjang = ps.id_jenjang
				WHERE kls.id_kelas_mk = {$id_kelas_mk}"
			);

			// Konversi format semester ke id_semester
			$semester_langitan = $this->rdb->QueryToArray(
				"SELECT thn_akademik_semester as thn, nm_semester FROM semester s
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = s.id_perguruan_tinggi
				WHERE 
					pt.npsn = '{$this->satuan_pendidikan['npsn']}' AND 
					s.fd_id_smt = '{$id_smt}'"
			);

			$this->smarty->assign('jenis_sinkronisasi', "Nilai Perkuliahan {$kelas_set[0]['NM_JENJANG']} {$kelas_set[0]['NM_PROGRAM_STUDI']} - {$kelas_set[0]['NM_KELAS']} {$semester_langitan[0]['THN']}/" . ($semester_langitan[0]['THN'] + 1) . " {$semester_langitan[0]['NM_SEMESTER']}");
			$this->smarty->assign('url', site_url('sync/proses/' . $mode));
		}

		if ($mode == 'nilai_per_prodi') {
			$kode_prodi		= $this->input->post('kode_prodi');
			$id_smt			= $this->input->post('semester');

			$program_studi_set = $this->rdb->QueryToArray(
				"SELECT nm_jenjang, nm_program_studi FROM program_studi ps
				JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
				JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
				WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' 
				AND ps.kode_program_studi = '{$kode_prodi}'
				AND ps.STATUS_AKTIF_PRODI=1"
			);

			// Konversi format semester ke id_semester
			$semester_langitan = $this->rdb->QueryToArray(
				"SELECT thn_akademik_semester as thn, nm_semester FROM semester s
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = s.id_perguruan_tinggi
				WHERE 
					pt.npsn = '{$this->satuan_pendidikan['npsn']}' AND 
					s.fd_id_smt = '{$id_smt}'"
			);

			$this->smarty->assign('jenis_sinkronisasi', 'Nilai Perkuliahan ' . $program_studi_set[0]['NM_JENJANG'] . ' ' . $program_studi_set[0]['NM_PROGRAM_STUDI'] . ' - ' . $semester_langitan[0]['THN'] . '/' . ($semester_langitan[0]['THN'] + 1) . ' ' . $semester_langitan[0]['NM_SEMESTER']);
			$this->smarty->assign('url', site_url('sync/proses/' . $mode));
		}

		if ($mode == 'nilai_transfer') {
			$this->smarty->assign('jenis_sinkronisasi', 'Nilai Transfer Mahasiswa');
			$this->smarty->assign('url', site_url('sync/proses/' . $mode));
		}

		if ($mode == 'nilai_transfer_umaha') {
			$this->smarty->assign('jenis_sinkronisasi', 'Nilai Perkuliahan UMAHA Angkatan &lt; 2014');
			$this->smarty->assign('url', site_url('sync/proses/' . $mode));
		}

		if ($mode == 'kuliah_mahasiswa') {
			$id_smt			= $this->input->post('semester');
			$kode_prodi			= $this->input->post('kode_prodi');

			$semester_langitan = $this->langitan_model->get_semester_langitan($this->satuan_pendidikan['npsn'], $id_smt);

			$program_studi_set = $this->rdb->QueryToArray(
				"SELECT nm_jenjang, nm_program_studi FROM program_studi ps
				JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
				JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
				JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
				WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' 
				AND ps.kode_program_studi = '{$kode_prodi}'
				AND ps.STATUS_AKTIF_PRODI=1"
			);

			$this->smarty->assign('jenis_sinkronisasi', 'Aktivitas Mahasiswa - Program Studi : ' . $program_studi_set[0]['NM_JENJANG'] . ' ' . $program_studi_set[0]['NM_PROGRAM_STUDI'] . ' Semester : ' . $semester_langitan);
			$this->smarty->assign('url', site_url('sync/proses/' . $mode));
		}

		if ($mode == 'hapus_mk_kurikulum') {
			$this->smarty->assign('jenis_sinkronisasi', 'Hapus Mata Kuliah Kurikulum');
			$this->smarty->assign('url', site_url('sync/proses/' . $mode));
		}

		if ($mode == 'lulusan_do') {
			$this->smarty->assign('jenis_sinkronisasi', 'Lulusan / DO / Keluar');
			$this->smarty->assign('url', site_url('sync/proses/' . $mode));
		}

		$this->smarty->assign('mode', $mode);
		$this->smarty->display('sync/start.tpl');
	}

	/**
	 * Pemrosesan sinkronisasi
	 */
	function proses($mode)
	{
		// harus request POST 
		if ($_SERVER['REQUEST_METHOD'] != 'POST') {
			return;
		}

		if ($mode == 'mahasiswa') {
			$this->proses_mahasiswa();
		} else if ($mode == 'riwayat_pendidikan_mahasiswa') {
			$this->proses_riwayat_pendidikan_mahasiswa();
		} else if ($mode == 'mata_kuliah') {
			$this->proses_mata_kuliah();
		} else if ($mode == 'kurikulum') {
			$this->proses_kurikulum();
		} else if ($mode == 'mata_kuliah_kurikulum') {
			$this->proses_mata_kuliah_kurikulum();
		} else if ($mode == 'kelas_kuliah') {
			$this->proses_kelas_kuliah();
		} else if ($mode == 'ajar_dosen') {
			$this->proses_ajar_dosen();
		} else if ($mode == 'nilai') {
			$this->proses_nilai();
		} else if ($mode == 'nilai_per_prodi') {
			$this->proses_nilai_per_prodi();
		} else if ($mode == 'nilai_transfer') {
			$this->proses_nilai_transfer();
		} else if ($mode == 'nilai_transfer_umaha') {
			$this->proses_nilai_transfer_umaha();
		} else if ($mode == 'kuliah_mahasiswa') {
			$this->proses_kuliah_mahasiswa();
		} else if ($mode == 'hapus_mk_kurikulum') {
			$this->proses_hapus_mk_kurikulum();
		} else if ($mode == 'lulusan_do') {
			$this->proses_lulusan_do();
		} else {
			echo json_encode(array('status' => 'done', 'message' => 'Not Implemented()'));
		}
	}
}
