<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller
{
	/**
	 * @var string Token versi WS 2
	 */
	public $token;

	function __construct()
	{
		parent::__construct();

		$this->check_credentials();

		$this->load->library('feederws', [
			'url' => $this->session->userdata('ws2url'),
			'token' => $this->session->userdata('token')
		]);
	}

	function index()
	{
		// --------------------------------------------
		// Ambil data PT
		// --------------------------------------------
		$response = $this->feederws->GetProfilPT();
		if ($response['error_code'] == 100) {
			redirect('/auth/logout');
			exit();
		}
		$this->satuan_pendidikan = $response['data'][0];
		$this->smarty->assign('satuan_pendidikan', $this->satuan_pendidikan);

		// Simpan ke session
		$this->satuan_pendidikan['npsn'] = trim($this->satuan_pendidikan['kode_perguruan_tinggi']);
		$this->session->set_userdata(FEEDER_SATUAN_PENDIDIKAN, $this->satuan_pendidikan);

		// --------------------------------------------
		// Ambil data Program Studi
		// --------------------------------------------
		$response = $this->feederws->GetAllProdi($this->session->userdata('username'));
		$prodi_set = $response['data'];

		// echo json_encode($this->feederws->GetListRiwayatPendidikanMahasiswa("02d57e86-b8a0-49d1-b55e-f24b3a03d8e6"));
		// echo json_encode($this->feederws->GetBiodataMahasiswa("BENI SAPUTRA", 'SuprehatiN', null));
		// echo json_encode($this->feederws->GetMatkulKurikulum('59722098-81bf-4a7f-badb-ad3fb4f47178', 'b98ded85-3077-4916-aee0-64b0a5488ad6'));
		// echo json_encode($this->feederws->GetListMataKuliah('5ed285c5-9066-440b-8539-42ed3f8980da'));
		// echo json_encode($this->feederws->GetAllProdi('071086'));
		// $hasil = $this->feederws->GetListPerkuliahanMahasiswa("id_semester='20231' AND id_prodi='b33571b7-822b-4ec6-82ff-3026f311648e'");
		// echo json_encode($hasil);
		// die();
		// $filter = "id_semester='20231' AND id_prodi='b33571b7-822b-4ec6-82ff-3026f311648e'";

        // $jumlah_mhs = 0;
		// $listKelas = $this->feederws->GetListKelasKuliah($filter);
		
		// echo json_encode($listKelas);
		// die();
		// $peserta_kelas_set = [];
		// $listKelas = $this->feederws->GetListKelasKuliah($filter);
		// foreach ($listKelas['data'] as $kelas) {	
			// $filterPesertaKelas = "";
			// $pesertaKelas = $this->feederws->GetNilaiTransferPendidikanMahasiswa($filter);
		// 	$peserta_kelas_set = array_merge($peserta_kelas_set, $pesertaKelas['data']);
		// }
		
		// $filter = "nidn in ()";
		// echo json_encode($this->feederws->GetListPenugasanDosen($filter));
		// die();
		// $filter = "id_prodi='b33571b7-822b-4ec6-82ff-3026f311648e' AND id_periode_masuk='20231'";
		// $riwayatPendidikan = $this->feederws->GetListRiwayatPendidikanMahasiswaProdi($filter);
		// echo count($riwayatPendidikan['data']);
		// die();
		// Simpan ke session
		$this->session->set_userdata(FEEDER_SMS . '_set', $prodi_set);

		$this->smarty->display('home/index.tpl');
	}
}
