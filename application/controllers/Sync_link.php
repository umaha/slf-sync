<?php

/**
 * @property Remotedb $rdb Remote DB Sistem Langitan
 * @property string $token Token webservice
 * @property string $npsn Kode Perguruan Tinggi
 * @property array $satuan_pendidikan Row: satuan_pendidikan
 */
class Sync_link extends MY_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->check_credentials();

		// Inisialisasi Token dan Satuan Pendidikan
		$this->token = $this->session->userdata('token');
		$this->satuan_pendidikan = $this->session->userdata(FEEDER_SATUAN_PENDIDIKAN);

		// Inisialisasi URL Feeder WS 2
		$this->load->library('feederws', [
			'url' => $this->session->userdata('ws2url'),
			'token' => $this->session->userdata('token')
		]);

		// Inisialisasi Library RemoteDB
		$this->load->library('remotedb', NULL, 'rdb');
		$this->rdb->set_url($this->session->userdata('langitan'));
	}

	function dosen()
	{
		$jumlah = array();

		// Ambil jumlah dosen di feeder
		$response = ['result' => count($this->feederws->GetListDosen()['data'])];
		$jumlah['feeder'] = $response['result'];

		// Ambil jumlah dosen di langitan
		$data_set = $this->rdb->QueryToArray(
			"SELECT count(*) as jumlah FROM pengguna p
			JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = p.id_perguruan_tinggi
			WHERE join_table = 2 AND pt.npsn = '{$this->satuan_pendidikan['npsn']}'
			UNION ALL
			SELECT count(*) as jumlah FROM pengguna p
			JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = p.id_perguruan_tinggi
			WHERE join_table = 2 AND pt.npsn = '{$this->satuan_pendidikan['npsn']}' AND p.fd_id_sdm IS NOT NULL"
		);
		$jumlah['langitan'] = $data_set[0]['JUMLAH'];
		$jumlah['linked'] = $data_set[1]['JUMLAH'];
		$jumlah['insert'] = $jumlah['feeder'] - $jumlah['linked'];

		$this->smarty->assign('jumlah', $jumlah);

		$this->smarty->assign('url_sync', site_url('sync_link/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync_link/' . $this->uri->segment(2) . '.tpl');
	}

	function proses_dosen()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync_link/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_FEEDER;

		if ($mode == MODE_AMBIL_DATA_FEEDER) {
			$dosen_set = $this->feederws->GetListDosen()['data'];

			// Ambil ID_PT
			$pt_set = $this->rdb->QueryToArray(
				"SELECT ID_PERGURUAN_TINGGI FROM PERGURUAN_TINGGI 
				WHERE NPSN = '{$this->satuan_pendidikan['kode_perguruan_tinggi']}'"
			);

			// simpan ke cache
			$this->session->set_userdata('dosen_set', $dosen_set);
			$this->session->set_userdata('jumlah_' . FEEDER_DOSEN, count($dosen_set));
			$this->session->set_userdata('id_perguruan_tinggi', $pt_set[0]['ID_PERGURUAN_TINGGI']);

			$result['message'] = 'Jumlah data Dosen di Feeder yg akan diproses : ' . count($dosen_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		} else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// ambil dari cache
			$total_data = $this->session->userdata('jumlah_' . FEEDER_DOSEN);
			$dosen_set = $this->session->userdata('dosen_set');
			$id_pt = $this->session->userdata('id_perguruan_tinggi');

			// Jika masih belum di proses semua
			if ($index_proses < $total_data) {
				// Ambil per row di feeder
				$data = $dosen_set[$index_proses];

				// Ambil id_reg_ptk dari dosen
				$penugasan_set = $this->feederws->GetListPenugasanDosen("nidn='{$data['nidn']}'", 1)['data'];
				$penugasan_exist = isset($penugasan_set[0]);

				// Cek di langitan berdasarkan id_dosen yg belum ada
				$pengguna_set = $this->rdb->QueryToArray(
					"SELECT ID_PENGGUNA, FD_ID_SDM FROM pengguna 
					WHERE id_perguruan_tinggi = {$id_pt} AND username = '{$data['nidn']}'"
				);
				$pengguna_exist = isset($pengguna_set[0]);

				// number
				$n = $index_proses + 1;

				$result['message'] = "{$n}. {$data['nidn']} {$data['nama_dosen']} " .
					($pengguna_exist ? 'Ada' : 'Tidak Ada') . '. ' .
					($penugasan_exist ? 'Penugasan Ada' : 'Penugasan Tidak Ada');

				if ($pengguna_exist && $penugasan_exist) {
					$update_pengguna_result = $this->rdb->Query(
						"UPDATE PENGGUNA SET FD_ID_SDM = '{$data['id_dosen']}', FD_SYNC_ON = SYSDATE
						WHERE ID_PERGURUAN_TINGGI = {$id_pt} AND USERNAME = '{$data['nidn']}' AND 
						      JOIN_TABLE = 2 AND FD_ID_SDM IS NULL"
					);

					$update_dosen_result = $this->rdb->Query(
						"UPDATE DOSEN SET FD_ID_REG_PTK = '{$penugasan_set[0]['id_registrasi_dosen']}', FD_SYNC_ON = SYSDATE
						WHERE ID_PENGGUNA = {$pengguna_set[0]['ID_PENGGUNA']} AND FD_ID_REG_PTK IS NULL"
					);

					$result['message'] .=
						" >>> UPDATE PENGGUNA " . ($update_pengguna_result ? 'SUKSES' : 'GAGAL') . '. ' .
						"UPDATE DOSEN " . ($update_dosen_result ? 'SUKSES' : 'GAGAL');
				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			} else {
				$result['status'] = SYNC_STATUS_DONE;
				$result['message'] = 'Selesai';
			}
		}

		echo json_encode($result);
	}

	/**
	 * GET /sync_link/mahasiswa/
	 */
	function mahasiswa()
	{
		// redirect('home');

		$jumlah['feeder'] = '-';
		$jumlah['langitan'] = '-';
		$jumlah['linked'] = '-';

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		// Ambil Semua angkatan yg ada :
		$sql_angkatan_mhs_raw = file_get_contents(APPPATH . 'models/sql/mahasiswa-angkatan.sql');
		$sql_angkatan_mhs = strtr($sql_angkatan_mhs_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$angkatan_set = $this->rdb->QueryToArray($sql_angkatan_mhs);
		$this->smarty->assign('angkatan_set', $angkatan_set);

		$this->smarty->assign('url_sync', site_url('sync_link/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync_link/' . $this->uri->segment(2) . '.tpl');
	}

	function mahasiswa_data($angkatan)
	{
		// redirect('home');

		$jumlah = array();
		$filter = "id_periode_masuk='{$angkatan}1' OR id_periode_masuk='{$angkatan}2'";

		// Ambil jumlah mahasiswa di feeder
		$response = $this->feederws->GetListRiwayatPendidikanMahasiswaProdi($filter);

		// Ambil jumlah mahasiswa di Sistem Langitan & yg sudah link
		$mhs_set = $this->rdb->QueryToArray(
			"SELECT count(*) as jumlah FROM mahasiswa m
			JOIN pengguna p ON p.id_pengguna = m.id_pengguna
			JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi
			JOIN perguruan_tinggi pt on pt.id_perguruan_tinggi = p.id_perguruan_tinggi
			WHERE npsn = '{$this->satuan_pendidikan['npsn']}' 
				and m.thn_angkatan_mhs = '{$angkatan}'
			UNION ALL
			SELECT count(*) as jumlah FROM mahasiswa m
			JOIN pengguna p ON p.id_pengguna = m.id_pengguna
			JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi
			JOIN perguruan_tinggi pt on pt.id_perguruan_tinggi = p.id_perguruan_tinggi
			WHERE npsn = '{$this->satuan_pendidikan['npsn']}' 
				and m.thn_angkatan_mhs = '{$angkatan}'
				AND m.FD_ID_REG_PD IS NOT NULL 
			"
		);

		$jumlah['feeder'] = count($response['data']);
		$jumlah['langitan'] = $mhs_set[0]['JUMLAH'];
		$jumlah['linked'] = $mhs_set[1]['JUMLAH'];

		echo json_encode($jumlah);
	}

	private function proses_mahasiswa()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync_link/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		$angkatan	= $this->input->post('angkatan');

		if ($mode == MODE_AMBIL_DATA_LANGITAN) {

			$filter = "id_periode_masuk='{$angkatan}1' OR id_periode_masuk='{$angkatan}2'";

			// Ambil jumlah mahasiswa di feeder
			$mahasiswa_set = $this->feederws->GetListRiwayatPendidikanMahasiswaProdi($filter);

			// Ambil ID_PT
			$pt_set = $this->rdb->QueryToArray(
				"SELECT ID_PERGURUAN_TINGGI FROM PERGURUAN_TINGGI 
				WHERE NPSN = '{$this->satuan_pendidikan['kode_perguruan_tinggi']}'"
			);

			// simpan ke cache
			$this->session->set_userdata('mahasiswa_set', $mahasiswa_set['data']);
			$this->session->set_userdata('id_perguruan_tinggi', $pt_set[0]['ID_PERGURUAN_TINGGI']);

			$result['message'] = 'Ambil data langitan selesai. Jumlah data: ' . count($mahasiswa_set['data']);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		} else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$mahasiswa_set = $this->session->userdata('mahasiswa_set');
			$id_pt = $this->session->userdata('id_perguruan_tinggi');

			// Jika masih dalam rentang index, di proses
			if ($index_proses < count($mahasiswa_set)) {
				// Ambil per row di feeder
				$data = $mahasiswa_set[$index_proses];

				// rtrim thn_angkatan_mhs
				$thn_angkatan_mhs = $angkatan;
				$nama = str_replace("'", "''", $data['nama_mahasiswa']);

			
				if(($data['id_jenis_keluar'] == null || $data['id_jenis_keluar'] == 1 )) {
					$query =
					"SELECT m.ID_PENGGUNA, m.ID_MHS, p.FD_ID_PD, m.FD_ID_REG_PD 
				FROM mahasiswa m 
				join pengguna p on p.id_pengguna = m.id_pengguna
				join program_studi ps on ps.id_program_studi = m.id_program_studi
				WHERE p.id_perguruan_tinggi = {$id_pt} 
					AND m.nim_mhs = '{$data['nim']}' 
					AND m.thn_angkatan_mhs = '{$thn_angkatan_mhs}' 
					AND trim(upper(p.nm_pengguna)) = trim(upper('{$nama}'))
					AND ps.fd_id_sms = '{$data['id_prodi']}'";
				} else {
					$query =
					"SELECT m.ID_PENGGUNA, m.ID_MHS, p.FD_ID_PD, m.FD_ID_REG_PD 
				FROM mahasiswa m 
				join pengguna p on p.id_pengguna = m.id_pengguna
				join program_studi ps on ps.id_program_studi = m.id_program_studi
				WHERE p.id_perguruan_tinggi = {$id_pt} 
					AND (m.nim_mhs = '{$data['nim']}' OR m.nim_mhs = '{$data['nim']}D')
					AND m.thn_angkatan_mhs = '{$thn_angkatan_mhs}' 
					AND trim(upper(p.nm_pengguna)) = trim(upper('{$nama}'))
					AND ps.fd_id_sms = '{$data['id_prodi']}'";
				}

				$mhs_langitan = $this->rdb->QueryToArray($query);

				$mhs_langitan_exist = isset($mhs_langitan[0]);
				
				// Jika ada
				if ($mhs_langitan_exist) {
					// Update
					$update_pengguna_result = $this->rdb->Query(
						"UPDATE PENGGUNA 
							SET FD_ID_PD = '{$data['id_mahasiswa']}', FD_SYNC_ON = SYSDATE
							WHERE ID_PENGGUNA = {$mhs_langitan[0]['ID_PENGGUNA']}"
					);

					$update_mahasiswa_result = $this->rdb->Query(
						"UPDATE MAHASISWA 
							SET FD_ID_REG_PD = '{$data['id_registrasi_mahasiswa']}', FD_SYNC_ON = SYSDATE
							WHERE ID_MHS = {$mhs_langitan[0]['ID_MHS']}"
					);

					$result['message'] .= ($index_proses + 1) . ". " .
						" >>> UPDATE PENGGUNA " . $data['nim'] .
						($update_pengguna_result ? ' SUKSES' : 'GAGAL') . '. ' .
						"UPDATE MAHASISWA " . ($update_mahasiswa_result ? 'SUKSES' : 'GAGAL');
				} else {
					
					$cek_mhs_langitan = $this->rdb->QueryToArray(
						"SELECT * FROM MAHASISWA m 
						WHERE m.FD_ID_REG_PD = '{$data['id_registrasi_mahasiswa']}'
						AND m.nim_mhs = '{$data['nim']}'"
					);
					if (count($cek_mhs_langitan) > 0) {
						$result['message'] .= ($index_proses + 1) . ". " ."NIM ".$cek_mhs_langitan[0]['NIM_MHS']." sudah sinkron";
					}else {
						$pesan = "Data mahasiswa {$data['nim']}({$data['id_registrasi_mahasiswa']}) dengan nama {$nama}, prodi {$data['id_prodi']} tidak ada di langitan.";

						$result['message'] = ($index_proses + 1) . ". " .$query;
					}

				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			} else {
				$result['status'] = SYNC_STATUS_DONE;
				$result['message'] = 'Selesai';
			}
		}

		echo json_encode($result);
	}


	/**
	 * GET /sync_link/program_studi/
	 */
	function program_studi()
	{

		$jumlah = array(
			'feeder'	=> '-',
			'langitan'	=> '-',
			'linked'	=> '-'
		);

		// Jumlah di feeder
		$result = $this->feederws->GetAllProdi($this->satuan_pendidikan['npsn']);
		$jumlah['feeder'] = count($result['data']);

		// Jumlah di langitan
		$result = $this->rdb->QueryToArray(
			"SELECT count(*) as jumlah FROM program_studi ps
			JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
			JOIN perguruan_tinggi pt on pt.id_perguruan_tinggi = f.id_perguruan_tinggi
			WHERE npsn = '{$this->satuan_pendidikan['npsn']}'"
		);
		$jumlah['langitan'] = $result[0]['JUMLAH'];

		// Jumlah yg sudah link (punya ID_SMS)
		$result = $this->rdb->QueryToArray(
			"SELECT count(*) as jumlah FROM program_studi ps
			JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
			JOIN perguruan_tinggi pt on pt.id_perguruan_tinggi = f.id_perguruan_tinggi
			WHERE npsn = '{$this->satuan_pendidikan['npsn']}' AND fd_id_sms IS NOT NULL"
		);
		$jumlah['linked'] = $result[0]['JUMLAH'];

		$this->smarty->assign('jumlah', $jumlah);

		$this->smarty->assign('url_sync', site_url('sync_link/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync_link/program_studi.tpl');
	}

	private function proses_program_studi()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync_link/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_FEEDER;

		if ($mode == MODE_AMBIL_DATA_FEEDER) {
			$program_studi_set = $this->feederws->GetAllProdi($this->satuan_pendidikan['kode_perguruan_tinggi'])['data'];

			// Ambil ID_PT
			$pt_set = $this->rdb->QueryToArray(
				"SELECT ID_PERGURUAN_TINGGI FROM PERGURUAN_TINGGI 
				WHERE NPSN = '{$this->satuan_pendidikan['kode_perguruan_tinggi']}'"
			);

			// simpan ke cache
			$this->session->set_userdata('program_studi_set', $program_studi_set);
			$this->session->set_userdata('id_perguruan_tinggi', $pt_set[0]['ID_PERGURUAN_TINGGI']);

			$result['message'] = 'Jumlah data Program Studi di Feeder yg akan diproses : ' . count($program_studi_set);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		} else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// ambil dari cache
			$program_studi_set = $this->session->userdata('program_studi_set');
			$id_pt = $this->session->userdata('id_perguruan_tinggi');

			// Jika masih belum di proses semua
			if ($index_proses < count($program_studi_set)) {
				// Ambil per row di feeder
				$data = $program_studi_set[$index_proses];

				// Cek di langitan berdasarkan id_dosen yg belum ada
				$query = "SELECT *
					FROM PROGRAM_STUDI ps
					JOIN FAKULTAS f ON f.ID_FAKULTAS = ps.ID_FAKULTAS 
					WHERE f.ID_PERGURUAN_TINGGI = {$id_pt} 
						AND trim(upper(ps.NM_PROGRAM_STUDI)) = trim(upper('{$data['nama_program_studi']}'))
						AND ps.KODE_PROGRAM_STUDI = '{$data['kode_program_studi']}'
						AND ps.STATUS_AKTIF_PRODI = 1";
				$prodi_set = $this->rdb->QueryToArray($query);

				$prodi_exist = isset($prodi_set[0]);

				$result['message'] = "";

				if ($prodi_exist) {
					$queryUpdate = "UPDATE PROGRAM_STUDI 
						SET FD_ID_SMS = '{$data['id_prodi']}', FD_SYNC_ON = SYSDATE
						WHERE ID_PROGRAM_STUDI = {$prodi_set[0]['ID_PROGRAM_STUDI']} 
							AND FD_ID_SMS IS NULL";
					$update_pengguna_result = $this->rdb->Query($queryUpdate);

					$result['message'] .=
						" >>> UPDATE PROGRAM_STUDI " . ($update_pengguna_result ? 'SUKSES' : 'GAGAL') . '. ';
				} else {
					$result['message'] = "Data Program Studi tidak ditemukan";
				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			} else {
				$result['status'] = SYNC_STATUS_DONE;
				$result['message'] = 'Selesai';
			}
		}

		echo json_encode($result);
	}

	/**
	 * GET /sync_link/mata_kuliah/
	 */
	function mata_kuliah()
	{
		// redirect('home');

		$jumlah['feeder'] = '-';
		$jumlah['langitan'] = '-';
		$jumlah['linked'] = '-';

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		$this->smarty->assign('url_sync', site_url('sync_link/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync_link/' . $this->uri->segment(2) . '.tpl');
	}

	function mata_kuliah_data($kode_prodi)
	{

		$jumlah = array();
		$filter = "id_prodi='{$kode_prodi}'";

		// Ambil jumlah mahasiswa di feeder
		$response = $this->feederws->GetListMataKuliahFilterBy($filter);

		// Ambil jumlah mahasiswa di Sistem Langitan & yg sudah link
		$matkul_set = $this->rdb->QueryToArray(
			"SELECT count(*) as jumlah FROM mata_kuliah mk
			join program_studi ps on ps.id_program_studi = mk.id_program_studi
			where ps.fd_id_sms = '{$kode_prodi}'
			UNION ALL
			SELECT count(*) as jumlah FROM mata_kuliah mk
			join program_studi ps on ps.id_program_studi = mk.id_program_studi
			where ps.fd_id_sms = '{$kode_prodi}' AND mk.FD_ID_MK IS NOT NULL 
			"
		);

		$jumlah['feeder'] = count($response['data']);
		$jumlah['langitan'] = $matkul_set[0]['JUMLAH'];
		$jumlah['linked'] = $matkul_set[1]['JUMLAH'];

		echo json_encode($jumlah);
	}

	private function proses_mata_kuliah()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync_link/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			$kode_prodi = $this->input->post('kode_prodi');

			$filter = "id_prodi='{$kode_prodi}'";

			// Ambil jumlah mahasiswa di feeder
			$mata_kuliah_set = $this->feederws->GetListMataKuliahFilterBy($filter);

			// simpan ke cache
			$this->session->set_userdata('mata_kuliah_set', $mata_kuliah_set['data']);

			$result['message'] = 'Ambil data langitan selesai. Jumlah data: ' . count($mata_kuliah_set['data']);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		} else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$mata_kuliah_set = $this->session->userdata('mata_kuliah_set');

			// Jika masih dalam rentang index, di proses
			if ($index_proses < count($mata_kuliah_set)) {
				// Ambil per row di feeder
				$data = $mata_kuliah_set[$index_proses];

				$nama_mk = str_replace("'","''",$data['nama_mata_kuliah']);

				$query =
					"SELECT * FROM MATA_KULIAH mk 
				JOIN program_studi ps on ps.id_program_studi = mk.id_program_studi
				WHERE mk.KD_MATA_KULIAH = '{$data['kode_mata_kuliah']}'
					AND trim(upper(mk.NM_MATA_KULIAH)) = trim(upper('{$nama_mk}'))
					AND mk.KREDIT_SEMESTER = '{$data['sks_mata_kuliah']}'
					AND ps.fd_id_sms = '{$data['id_prodi']}'";

				$mata_kuliah_langitan = $this->rdb->QueryToArray($query);

				$mata_kuliah_langitan_exist = isset($mata_kuliah_langitan[0]);

				// Jika ada
				if ($mata_kuliah_langitan_exist && 
					($mata_kuliah_langitan[0]['FD_ID_MK'] == $data['id_matkul'] ||
					$mata_kuliah_langitan[0]['FD_ID_MK'] == null)) {
					// Update
					$update_mata_kuliah_result = $this->rdb->Query(
						"UPDATE MATA_KULIAH 
							SET FD_ID_MK = '{$data['id_matkul']}', 
								FD_SYNC_ON = SYSDATE,
								KREDIT_TATAP_MUKA = '{$data['sks_tatap_muka']}',
								KREDIT_PRAKTIKUM = '{$data['sks_praktek']}',
								KREDIT_PRAK_LAPANGAN = '{$data['sks_praktek_lapangan']}',
								KREDIT_SIMULASI = '{$data['sks_simulasi']}',
								STATUS_PRAKTIKUM = '{$data['ada_acara_praktek']}',
							WHERE ID_MATA_KULIAH = {$mata_kuliah_langitan[0]['ID_MATA_KULIAH']}"
					);

					$result['message'] = ($index_proses + 1) .
						" >>> UPDATE MATA KULIAH " . 
						"kode matkul: " . $data['kode_mata_kuliah'] .
						" nama matkul: " . $data['nama_mata_kuliah'] .
						" sks: " . $data['sks_mata_kuliah'] .
						($update_mata_kuliah_result ? 'SUKSES' : 'GAGAL');
				} else {
					$query = "SELECT *
						FROM PROGRAM_STUDI ps
						WHERE ps.FD_ID_SMS = '{$data['id_prodi']}'";
					$prodi_set = $this->rdb->QueryToArray($query);

					$data['ada_sap'] = isset($data['ada_sap']) ? $data['ada_sap'] : 0;
					$data['ada_silabus'] = isset($data['ada_silabus']) ? $data['ada_silabus'] : 0;
					$data['ada_bahan_ajar'] = isset($data['ada_bahan_ajar']) ? $data['ada_bahan_ajar'] : 0;
					$data['ada_acara_praktek'] = isset($data['ada_acara_praktek']) ? $data['ada_acara_praktek'] : 0;
					$data['ada_diktat'] = isset($data['ada_diktat']) ? $data['ada_diktat'] : 0;
					$data['jns_mk'] = isset($data['jns_mk']) ? $data['jns_mk'] : 'A';
					$data['nama_mata_kuliah'] = str_replace("'", "''", $data['nama_mata_kuliah']);

					$query = "INSERT INTO MATA_KULIAH
						(KD_MATA_KULIAH, NM_MATA_KULIAH, KREDIT_SEMESTER, ID_PROGRAM_STUDI, KETERANGAN, KREDIT_TATAP_MUKA, KREDIT_PRAKTIKUM,  KREDIT_PRAK_LAPANGAN, CREATED_ON, ID_JENIS_MK, ID_KELOMPOK_MK, KREDIT_SIMULASI, ADA_SAP, ADA_SILABUS, ADA_BAHAN_AJAR, STATUS_PRAKTIKUM, ADA_DIKTAT, FD_ID_MK, FD_SYNC_ON)
						VALUES
						('{$data['kode_mata_kuliah']}', '{$data['nama_mata_kuliah']}', '{$data['sks_mata_kuliah']}','{$prodi_set[0]['ID_PROGRAM_STUDI']}', 'INSERT FROM FEEDER',  '{$data['sks_tatap_muka']}', '{$data['sks_praktek']}', '{$data['sks_praktek_lapangan']}', SYSDATE, '{$data['jns_mk']}', '{$data['kel_mk']}', '{$data['sks_simulasi']}', '{$data['ada_sap']}', '{$data['ada_silabus']}', '{$data['ada_bahan_ajar']}', '{$data['ada_acara_praktek']}', '{$data['ada_diktat']}', '{$data['id_matkul']}', SYSDATE)";
					$insert_mata_kuliah_result = $this->rdb->Query($query);
					
					$result['message'] = ($index_proses + 1) .
						" >>> INSERT MATA KULIAH " . 
						"kode matkul: " . $data['kode_mata_kuliah'] . " nama matkul: " . $data['nama_mata_kuliah'] . " sks: " . $data['sks_mata_kuliah'] .
						($insert_mata_kuliah_result ? ' SUKSES' : 'GAGAL');
				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			} else {
				$result['status'] = SYNC_STATUS_DONE;
				$result['message'] = 'Selesai';
			}
		}

		echo json_encode($result);
	}

	/**
	 * GET /sync_link/kurikulum/
	 */
	function kurikulum()
	{
		// redirect('home');

		$jumlah['feeder'] = '-';
		$jumlah['langitan'] = '-';
		$jumlah['linked'] = '-';

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		$this->smarty->assign('url_sync', site_url('sync_link/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync_link/' . $this->uri->segment(2) . '.tpl');
	}

	function kurikulum_data($kode_prodi)
	{

		$jumlah = array();
		$filter = "id_prodi='{$kode_prodi}'";

		// Ambil jumlah mahasiswa di feeder
		$response = $this->feederws->GetListKurikulumFilterBy($filter);

		// Ambil jumlah mahasiswa di Sistem Langitan & yg sudah link
		$matkul_set = $this->rdb->QueryToArray(
			"SELECT count(*) as jumlah FROM kurikulum k
			join program_studi ps on ps.id_program_studi = k.id_program_studi
			where ps.fd_id_sms = '{$kode_prodi}'
			UNION ALL
			SELECT count(*) as jumlah FROM kurikulum k
			join program_studi ps on ps.id_program_studi = k.id_program_studi
			where ps.fd_id_sms = '{$kode_prodi}' AND k.FD_ID_KURIKULUM_SP IS NOT NULL 
			"
		);

		$jumlah['feeder'] = count($response['data']);
		$jumlah['langitan'] = $matkul_set[0]['JUMLAH'];
		$jumlah['linked'] = $matkul_set[1]['JUMLAH'];

		echo json_encode($jumlah);
	}

	private function proses_kurikulum()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync_link/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			$kode_prodi = $this->input->post('kode_prodi');

			$filter = "id_prodi='{$kode_prodi}'";

			// Ambil jumlah mahasiswa di feeder
			$kurikulum_set = $this->feederws->GetListKurikulumFilterBy($filter);

			// simpan ke cache
			$this->session->set_userdata('kurikulum_set', $kurikulum_set['data']);

			$result['message'] = 'Ambil data langitan selesai. Jumlah data: ' . count($kurikulum_set['data']);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		} else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$kurikulum_set = $this->session->userdata('kurikulum_set');

			// Jika masih dalam rentang index, di proses
			if ($index_proses < count($kurikulum_set)) {
				// Ambil per row di feeder
				$data = $kurikulum_set[$index_proses];

				$query =
					"SELECT * FROM KURIKULUM k 
					JOIN program_studi ps on ps.id_program_studi = k.id_program_studi
					WHERE trim(upper(k.NM_KURIKULUM)) = trim(upper('{$data['nama_kurikulum']}'))
						AND ps.fd_id_sms = '{$data['id_prodi']}'";

				$kurikulum_langitan = $this->rdb->QueryToArray($query);

				$kurikulum_langitan_exist = isset($kurikulum_langitan[0]);

				// Jika ada
				if ($kurikulum_langitan_exist) {
					// Update
					if ($kurikulum_langitan[0]['FD_ID_KURIKULUM_SP'] == null) {
						$update_pengguna_result = $this->rdb->Query(
							"UPDATE KURIKULUM 
						SET FD_ID_KURIKULUM_SP = '{$data['id_kurikulum']}',
							FD_SYNC_ON = SYSDATE
						WHERE ID_KURIKULUM = {$kurikulum_langitan[0]['ID_KURIKULUM']}"
						);

						$result['message'] .= ($index_proses + 1) .
							" >>> UPDATE KURIKULUM " . ($update_pengguna_result ? 'SUKSES' : 'GAGAL');
					}
				} else {

					$queryProdi = "SELECT *
						FROM PROGRAM_STUDI ps
						WHERE ps.FD_ID_SMS = '{$data['id_prodi']}'";
					$prodi_set = $this->rdb->QueryToArray($queryProdi);

					// Konversi format semester ke id_semester
					$semester_langitan = $this->rdb->QueryToArray(
						"SELECT * FROM semester s
						JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = s.id_perguruan_tinggi
						WHERE 
							pt.npsn = '{$this->satuan_pendidikan['npsn']}' AND 
							s.fd_id_smt = '{$data['id_semester']}'"
					);
					$id_semester = $semester_langitan[0]['ID_SEMESTER'];
					$tahun = $semester_langitan[0]['THN_AKADEMIK_SEMESTER'];


					// insert
					$query = "INSERT INTO KURIKULUM (
						NM_KURIKULUM,
						STATUS_AKTIF,
						THN_KURIKULUM,
						ID_PROGRAM_STUDI,
						KETERANGAN_KURIKULUM,
						SMT_NORMAL,
						SKS_LULUS,
						SKS_WAJIB,
						SKS_PILIHAN,
						ID_SEMESTER_MULAI,
						CREATED_ON,
						FD_SYNC_ON,
						FD_ID_KURIKULUM_SP
					) VALUES (
						'{$data['nama_kurikulum']}',
						1,
						{$tahun},
						'{$prodi_set[0]['ID_PROGRAM_STUDI']}',
						'INSERT FROM FEEDER',
						'{$data['jml_sem_normal']}',
						'{$data['jumlah_sks_lulus']}',
						'{$data['jumlah_sks_wajib']}',
						'{$data['jumlah_sks_pilihan']}',
						{$id_semester},
						SYSDATE,
						SYSDATE,
						'{$data['id_kurikulum']}'
					)";

					$insert_result = $this->rdb->Query($query);

					$result['message'] .= ($index_proses + 1) .
						" >>> INSERT KURIKULUM " . ($insert_result ? 'SUKSES' : 'GAGAL');
				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			} else {
				$result['status'] = SYNC_STATUS_DONE;
				$result['message'] = 'Selesai';
			}
		}

		echo json_encode($result);
	}

	/**
	 * GET /sync_link/kurikulum_mata_kuliah/
	 */
	function kurikulum_mata_kuliah()
	{
		// redirect('home');

		$jumlah['feeder'] = '-';
		$jumlah['langitan'] = '-';
		$jumlah['linked'] = '-';

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		$this->smarty->assign('url_sync', site_url('sync_link/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync_link/' . $this->uri->segment(2) . '.tpl');
	}

	function kurikulum_mata_kuliah_data($kode_prodi)
	{
		$jumlah = array();
		$filter = "id_prodi='{$kode_prodi}'";

		// Ambil jumlah mahasiswa di feeder
		$response = $this->feederws->GetListMatkulKurikulumFilterBy($filter);

		// Ambil jumlah mahasiswa di Sistem Langitan & yg sudah link
		$kurikulum_mata_kuliah_set = $this->rdb->QueryToArray(
			"SELECT count(*) as jumlah FROM kurikulum_mk kmk
			join program_studi ps on ps.id_program_studi = kmk.id_program_studi
			where ps.fd_id_sms = '{$kode_prodi}'
			UNION ALL
			SELECT count(*) as jumlah FROM kurikulum_mk kmk
			join program_studi ps on ps.id_program_studi = kmk.id_program_studi
			where ps.fd_id_sms = '{$kode_prodi}' 
				AND kmk.FD_ID_KURIKULUM_SP IS NOT NULL 
				AND kmk.FD_ID_MK IS NOT NULL 
			"
		);

		$jumlah['feeder'] = count($response['data']);
		$jumlah['langitan'] = $kurikulum_mata_kuliah_set[0]['JUMLAH'];
		$jumlah['linked'] = $kurikulum_mata_kuliah_set[1]['JUMLAH'];

		echo json_encode($jumlah);
	}

	private function proses_kurikulum_mata_kuliah()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync_link/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		if ($mode == MODE_AMBIL_DATA_LANGITAN) {
			$kode_prodi = $this->input->post('kode_prodi');

			$filter = "id_prodi='{$kode_prodi}'";

			// Ambil jumlah mahasiswa di feeder
			$kurikulum_mata_kuliah_set = $this->feederws->GetListMatkulKurikulumFilterBy($filter);

			// simpan ke cache
			$this->session->set_userdata('kurikulum_mata_kuliah_set', $kurikulum_mata_kuliah_set['data']);

			$result['message'] = 'Ambil data langitan selesai. Jumlah data: ' . count($kurikulum_mata_kuliah_set['data']);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		} else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$kurikulum_mata_kuliah_set = $this->session->userdata('kurikulum_mata_kuliah_set');

			// Jika masih dalam rentang index, di proses
			if ($index_proses < count($kurikulum_mata_kuliah_set)) {
				// Ambil per row di feeder
				$data = $kurikulum_mata_kuliah_set[$index_proses];

				$query =
					"SELECT * FROM KURIKULUM_MK kmk 
					JOIN program_studi ps on ps.id_program_studi = kmk.id_program_studi
					JOIN MATA_KULIAH mk on mk.id_mata_kuliah = kmk.id_mata_kuliah
					JOIN KURIKULUM k on k.id_kurikulum = kmk.id_kurikulum
					WHERE ps.fd_id_sms = '{$data['id_prodi']}' 
						AND mk.fd_id_mk = '{$data['id_matkul']}'
						AND k.fd_id_kurikulum_sp = '{$data['id_kurikulum']}'";

				$kurikulum_mata_kuliah_langitan = $this->rdb->QueryToArray($query);

				$kurikulum_mata_kuliah_langitan_exist = isset($kurikulum_mata_kuliah_langitan[0]);

				// Jika ada
				if ($kurikulum_mata_kuliah_langitan_exist) {
					// Update
					$update_kurikulum_mk_result = $this->rdb->Query(
						"UPDATE KURIKULUM_MK 
						SET FD_ID_MK = '{$data['id_matkul']}', 
							FD_ID_KURIKULUM_SP = '{$data['id_kurikulum']}',
							FD_SYNC_ON = SYSDATE
						WHERE ID_KURIKULUM_MK = {$kurikulum_mata_kuliah_langitan[0]['ID_KURIKULUM_MK']}"
					);

					$result['message'] = ($index_proses + 1) .
						" >>> UPDATE KURIKULUM MATA KULIAH " . ($update_kurikulum_mk_result ? 'SUKSES' : 'GAGAL');
				} else {

					$queryProdi = "SELECT *
						FROM PROGRAM_STUDI ps
						WHERE ps.FD_ID_SMS = '{$data['id_prodi']}'";
					$prodi_set = $this->rdb->QueryToArray($queryProdi);

					$queryMatkul = "SELECT *
						FROM MATA_KULIAH mk
						WHERE mk.FD_ID_MK = '{$data['id_matkul']}'";
					$matkul_set = $this->rdb->QueryToArray($queryMatkul);

					$queryKurikulum = "SELECT *
						FROM KURIKULUM k
						WHERE k.FD_ID_KURIKULUM_SP = '{$data['id_kurikulum']}'";
					$kurikulum_set = $this->rdb->QueryToArray($queryKurikulum);
					// insert 

					if (count($prodi_set) > 0 && count($matkul_set) > 0 && count($kurikulum_set)>0) {

					$insert_kurikulum_mk_result = $this->rdb->Query(
						"INSERT INTO KURIKULUM_MK (
							ID_MATA_KULIAH,
							ID_PROGRAM_STUDI,
							KREDIT_SEMESTER,
							KREDIT_TATAP_MUKA,
							KREDIT_PRAKTIKUM,
							KREDIT_PRAK_LAPANGAN,
							KREDIT_SIMULASI,
							TINGKAT_SEMESTER,
							STATUS_SERI,
							CREATED_ON,
							ID_KURIKULUM,
							FD_ID_MK,
							FD_ID_KURIKULUM_SP,
							STATUS_WAJIB,
							FD_SYNC_ON
						) VALUES (
							'{$matkul_set[0]['ID_MATA_KULIAH']}',
							'{$prodi_set[0]['ID_PROGRAM_STUDI']}',
							'{$data['sks_mata_kuliah']}',
							'{$data['sks_tatap_muka']}',
							'{$data['sks_praktek']}',
							'{$data['sks_praktek_lapangan']}',
							'{$data['sks_simulasi']}',
							'{$data['semester']}',
							0,
							SYSDATE,
							'{$kurikulum_set[0]['ID_KURIKULUM']}',
							'{$data['id_matkul']}',
							'{$data['id_kurikulum']}',
							'{$data['apakah_wajib']}',
							SYSDATE
						)"
					);

					$result['message'] = ($index_proses + 1) .
						" >>> INSERT KURIKULUM MATA KULIAH " . ($insert_kurikulum_mk_result ? 'SUKSES' : 'GAGAL');
					}
					else {
						$result['message'] = ($index_proses + 1) .
						" >>> INSERT KURIKULUM MATA KULIAH GAGAL";
					}
				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			} else {
				$result['status'] = SYNC_STATUS_DONE;
				$result['message'] = 'Selesai';
			}
		}

		echo json_encode($result);
	}

	/**
	 * GET /sync_link/kelas_kuliah/
	 */
	function kelas_kuliah()
	{
		// redirect('home');

		$jumlah['feeder'] = '-';
		$jumlah['langitan'] = '-';
		$jumlah['linked'] = '-';

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		$this->smarty->assign('url_sync', site_url('sync_link/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync_link/' . $this->uri->segment(2) . '.tpl');
	}

	function kelas_kuliah_data($kode_prodi)
	{
		// redirect('home');

		$jumlah = array();
		$filter = "id_prodi='{$kode_prodi}'";

		// Ambil jumlah mahasiswa di feeder
		$response = $this->feederws->GetListKelasKuliah($filter);

		// Konversi format semester ke id_semester
		$semester_langitan = $this->rdb->QueryToArray(
			"SELECT id_semester FROM semester s
			JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = s.id_perguruan_tinggi
			WHERE 
				pt.npsn = '{$this->satuan_pendidikan['npsn']}'"
		);

		// Ambil jumlah mahasiswa di Sistem Langitan & yg sudah link
		$kelas_kuliah_set = $this->rdb->QueryToArray(
			"SELECT count(*) as jumlah FROM kelas_mk km
			join program_studi ps on ps.id_program_studi = km.id_program_studi
			where ps.fd_id_sms = '{$kode_prodi}' 
			UNION ALL
			SELECT count(*) as jumlah FROM kelas_mk km
			join program_studi ps on ps.id_program_studi = km.id_program_studi
			where ps.fd_id_sms = '{$kode_prodi}'
				AND km.FD_ID_KLS IS NOT NULL 
			"
		);

		$jumlah['feeder'] = count($response['data']);
		$jumlah['langitan'] = $kelas_kuliah_set[0]['JUMLAH'];
		$jumlah['linked'] = $kelas_kuliah_set[1]['JUMLAH'];

		echo json_encode($jumlah);
	}

	private function proses_kelas_kuliah()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync_link/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		$kode_prodi = $this->input->post('kode_prodi');

		// konversi format prodi ke id_prodi
		$queryProdi = "SELECT *
			FROM PROGRAM_STUDI ps
			WHERE ps.FD_ID_SMS = '{$kode_prodi}'";
		$prodi_set = $this->rdb->QueryToArray($queryProdi);

		if ($mode == MODE_AMBIL_DATA_LANGITAN) {

			$filter = "id_prodi='{$kode_prodi}'";

			// Ambil jumlah mahasiswa di feeder
			$kelas_kuliah_set = $this->feederws->GetListKelasKuliah($filter);

			// simpan ke cache
			$this->session->set_userdata('kelas_kuliah_set', $kelas_kuliah_set['data']);

			$result['message'] = 'Ambil data langitan selesai. Jumlah data: ' . count($kelas_kuliah_set['data']);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		} else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$kelas_kuliah_set = $this->session->userdata('kelas_kuliah_set');

			// Jika masih dalam rentang index, di proses
			if ($index_proses < count($kelas_kuliah_set)) {
				// Ambil per row di feeder
				$data = $kelas_kuliah_set[$index_proses];

				// Konversi format semester ke id_semester
				$semester_langitan = $this->rdb->QueryToArray(
					"SELECT id_semester FROM semester s
					JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = s.id_perguruan_tinggi
					WHERE 
						pt.npsn = '{$this->satuan_pendidikan['npsn']}' AND 
						s.fd_id_smt = '{$data['id_semester']}'"
				);
				$id_semester = $semester_langitan[0]['ID_SEMESTER'];

				$query =
					"SELECT * FROM KELAS_MK kmk 
					JOIN program_studi ps on ps.id_program_studi = kmk.id_program_studi
					JOIN NAMA_KELAS nk on kmk.no_kelas_mk = nk.id_nama_kelas
					JOIN MATA_KULIAH mk on mk.id_mata_kuliah = kmk.id_mata_kuliah
					WHERE trim(upper(nk.NAMA_FEEDER)) = trim(upper('{$data['nama_kelas_kuliah']}'))
						AND kmk.id_semester = '{$id_semester}'
						AND mk.fd_id_mk = '{$data['id_matkul']}'
						AND ps.fd_id_sms = '{$data['id_prodi']}'";

				$kelas_kuliah_langitan = $this->rdb->QueryToArray($query);

				$kelas_kuliah_langitan_exist = isset($kelas_kuliah_langitan[0]);

				// Jika ada
				if ($kelas_kuliah_langitan_exist) {
					// Update
					$update_pengguna_result = $this->rdb->Query(
						"UPDATE KELAS_MK 
						SET FD_ID_KLS = '{$data['id_kelas_kuliah']}', 
							FD_SYNC_ON = SYSDATE
						WHERE ID_KELAS_MK = {$kelas_kuliah_langitan[0]['ID_KELAS_MK']}"
					);

					$result['message'] .= ($index_proses + 1) .
						" >>> UPDATE Kelas " . ($update_pengguna_result ? 'SUKSES' : 'GAGAL');
				} else {

					$nama_kelas = $this->rdb->QueryToArray(
						"SELECT * FROM NAMA_KELAS nk
						JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = nk.id_perguruan_tinggi
						WHERE pt.npsn = '{$this->satuan_pendidikan['npsn']}' 
						AND nk.NAMA_FEEDER = '{$data['nama_kelas_kuliah']}'"
					);

					$kurikulum_mk = $this->rdb->QueryToArray(
						"SELECT * FROM KURIKULUM_MK kmk
						WHERE kmk.FD_ID_MK = '{$data['id_matkul']}'"
					);

					if (count($kurikulum_mk) > 0 && count($nama_kelas) > 0) {
						
					// INSERT
					$insert_result = $this->rdb->Query(
						"INSERT INTO KELAS_MK (
							ID_SEMESTER,
							ID_KURIKULUM_MK,
							ID_PROGRAM_STUDI,
							NO_KELAS_MK,
							IS_PDITT,
							IS_PENGGUNA_PDITT,
							KUOTA_PDITT,
							CREATED_ON,
							ID_MATA_KULIAH,
							STATUS_SERI,
							FD_ID_KLS,
							FD_SYNC_ON
						) VALUES (
							'{$id_semester}',
							'{$kurikulum_mk[0]['ID_KURIKULUM_MK']}',
							'{$prodi_set[0]['ID_PROGRAM_STUDI']}',
							'{$nama_kelas[0]['ID_NAMA_KELAS']}',
							0,
							0,
							0,
							SYSDATE,
							'{$kurikulum_mk[0]['ID_MATA_KULIAH']}',
							0,
							'{$data['id_kelas_kuliah']}',
							SYSDATE
						)"
					);


					$result['message'] .= ($index_proses + 1) .
						" >>> INSERT Kelas " . ($insert_result ? 'SUKSES' : 'GAGAL');
					}
					else {
						$result['message'] .= ($index_proses + 1) .
						" >>> DATA Kelas TIDAK DITEMUKAN";
					}

				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			} else {
				$result['status'] = SYNC_STATUS_DONE;
				$result['message'] = 'Selesai';
			}
		}

		echo json_encode($result);
	}

	/**
	 * GET /sync_link/nilai/
	 */
	function nilai()
	{
		// redirect('home');

		$jumlah['feeder'] = '-';
		$jumlah['langitan'] = '-';
		$jumlah['linked'] = '-';

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		$this->smarty->assign('url_sync', site_url('sync_link/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync_link/' . $this->uri->segment(2) . '.tpl');
	}


	function nilai_data($kode_prodi, $fd_id_semester)
	{

		$jumlah = array();
		$filter = "id_prodi='{$kode_prodi}' AND id_semester='{$fd_id_semester}'";

		// Ambil jumlah mahasiswa di feeder
		$response = $this->feederws->GetDetailNilaiPerkuliahanKelas($filter);

		// Konversi format semester ke id_semester
		$semester_langitan = $this->rdb->QueryToArray(
			"SELECT id_semester FROM semester s
			JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = s.id_perguruan_tinggi
			WHERE 
				pt.npsn = '{$this->satuan_pendidikan['npsn']}' AND 
				s.fd_id_smt = '{$fd_id_semester}'"
		);
		$id_semester = $semester_langitan[0]['ID_SEMESTER'];

		// Ambil jumlah mahasiswa di Sistem Langitan & yg sudah link
		$nilai_set = $this->rdb->QueryToArray(
			"SELECT count(*) as jumlah FROM pengambilan_mk pmk
			join kelas_mk km on km.id_kelas_mk = pmk.id_kelas_mk
			join program_studi ps on ps.id_program_studi = km.id_program_studi
			where ps.fd_id_sms = '{$kode_prodi}'
				 AND pmk.id_semester = '{$id_semester}'
			UNION ALL
			SELECT count(*) as jumlah FROM pengambilan_mk pmk
			join kelas_mk km on km.id_kelas_mk = pmk.id_kelas_mk
			join program_studi ps on ps.id_program_studi = km.id_program_studi
			where ps.fd_id_sms = '{$kode_prodi}' 
				AND pmk.id_semester = '{$id_semester}'
				AND (pmk.FD_ID_REG_PD IS NOT NULL 
				OR pmk.FD_ID_KLS IS NOT NULL )
			"
		);

		$jumlah['feeder'] = count($response['data']);
		$jumlah['langitan'] = $nilai_set[0]['JUMLAH'];
		$jumlah['linked'] = $nilai_set[1]['JUMLAH'];

		echo json_encode($jumlah);
	}

	private function proses_nilai()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync_link/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		$kode_prodi = $this->input->post('kode_prodi');
		$fd_id_semester = $this->input->post('semester');

		// Konversi format semester ke id_semester
		$semester_langitan = $this->rdb->QueryToArray(
			"SELECT id_semester FROM semester s
			JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = s.id_perguruan_tinggi
			WHERE 
				pt.npsn = '{$this->satuan_pendidikan['npsn']}' AND 
				s.fd_id_smt = '{$fd_id_semester}'"
		);
		$id_semester = $semester_langitan[0]['ID_SEMESTER'];

		if ($mode == MODE_AMBIL_DATA_LANGITAN) {

			$filter = "id_prodi='{$kode_prodi}' AND id_semester='{$fd_id_semester}'";

			// Ambil jumlah mahasiswa di feeder
			$nilai_set = $this->feederws->GetDetailNilaiPerkuliahanKelas($filter);

			// simpan ke cache
			$this->session->set_userdata('nilai_set', $nilai_set['data']);

			$result['message'] = 'Ambil data langitan selesai. Jumlah data: ' . count($nilai_set['data']);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		} else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$nilai_set = $this->session->userdata('nilai_set');

			// Jika masih dalam rentang index, di proses
			if ($index_proses < count($nilai_set)) {
				// Ambil per row di feeder
				$data = $nilai_set[$index_proses];

				$query =
					"SELECT * FROM PENGAMBILAN_MK pmk 
					JOIN MAHASISWA m on m.id_mhs = pmk.id_mhs
					JOIN program_studi ps on ps.id_program_studi = m.id_program_studi
					JOIN KELAS_MK kmk ON kmk.id_kelas_mk = pmk.id_kelas_mk
					WHERE ps.fd_id_sms = '{$data['id_prodi']}'
						AND m.FD_ID_REG_PD = '{$data['id_registrasi_mahasiswa']}'
						AND pmk.id_semester = '{$id_semester}'
						AND kmk.FD_ID_KLS = '{$data['id_kelas_kuliah']}'";

				$nilai_langitan = $this->rdb->QueryToArray($query);

				$nilai_langitan_exist = isset($nilai_langitan[0]);

				// Jika ada
				if ($nilai_langitan_exist) {
					// Update
					$update_pengguna_result = $this->rdb->Query(
						"UPDATE PENGAMBILAN_MK 
						SET NILAI_ANGKA = {$data['nilai_angka']},
							NILAI_HURUF = '{$data['nilai_huruf']}',
							FD_ID_KLS = '{$data['id_kelas_kuliah']}', 
							FD_ID_REG_PD = '{$data['id_registrasi_mahasiswa']}',
							FD_SYNC_ON = SYSDATE
						WHERE ID_PENGAMBILAN_MK = {$nilai_langitan[0]['ID_PENGAMBILAN_MK']}"
					);

					$result['message'] .= ($index_proses + 1) .
						" >>> UPDATE Nilai NIM : " . $nilai_langitan[0]['NIM_MHS'] .
						($update_pengguna_result ? 'SUKSES' : 'GAGAL');
				} else {
					// insert 

					$mahasiswa = $this->rdb->QueryToArray(
						"SELECT * FROM MAHASISWA m
						WHERE m.FD_ID_REG_PD = '{$data['id_registrasi_mahasiswa']}'"
					);

					$kelas_kuliah = $this->rdb->QueryToArray(
						"SELECT * FROM KELAS_MK kmk
						WHERE kmk.FD_ID_KLS = '{$data['id_kelas_kuliah']}'"
					);

					$insert_nilai_result = $this->rdb->Query(
						"INSERT INTO PENGAMBILAN_MK 
						(ID_KELAS_MK,	
							ID_MHS, 
							ID_SEMESTER, 
							STATUS_APV_PENGAMBILAN_MK,
							STATUS_PENGAMBILAN_MK,
							FLAGNILAI,
							STATUS_TAMPIL,
							NILAI_ANGKA, 
							NILAI_HURUF, 
							FD_ID_KLS, 
							FD_ID_REG_PD, 
							FD_SYNC_ON)
						VALUES (
							'{$kelas_kuliah[0]['ID_KELAS_MK']}',
							'{$mahasiswa[0]['ID_MHS']}',
							'{$id_semester}',
							1,
							1,
							1,
							1,
							{$data['nilai_angka']},
							'{$data['nilai_huruf']}',
							'{$data['id_kelas_kuliah']}',
							'{$data['id_registrasi_mahasiswa']}',
							SYSDATE
						)"
					);

					$result['message'] .= ($index_proses + 1) .
						" >>> INSERT Nilai, Nim : " . $mahasiswa[0]['NIM_MHS'] .
						" Kelas :" . $kelas_kuliah[0]['ID_KELAS_MK'] .
						($insert_nilai_result ? 'SUKSES' : 'GAGAL');
				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			} else {
				$result['status'] = SYNC_STATUS_DONE;
				$result['message'] = 'Selesai';
			}
		}

		echo json_encode($result);
	}


	/**
	 * GET /sync_link/kuliah_mahasiswa/
	 */
	function kuliah_mahasiswa()
	{
		// redirect('home');

		$jumlah['feeder'] = '-';
		$jumlah['langitan'] = '-';
		$jumlah['linked'] = '-';

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		$this->smarty->assign('url_sync', site_url('sync_link/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync_link/' . $this->uri->segment(2) . '.tpl');
	}

	function kuliah_mahasiswa_data($kode_prodi)
	{

		$jumlah = array();
		$filter = "id_prodi='{$kode_prodi}'";

		// Ambil jumlah mahasiswa di feeder
		$response = $this->feederws->GetListPerkuliahanMahasiswa($filter);

		// Konversi format semester ke id_semester
		$semester_langitan = $this->rdb->QueryToArray(
			"SELECT id_semester FROM semester s
			JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = s.id_perguruan_tinggi
			WHERE 
				pt.npsn = '{$this->satuan_pendidikan['npsn']}'"
		);
		$id_semester = $semester_langitan[0]['ID_SEMESTER'];

		// Ambil jumlah mahasiswa di Sistem Langitan & yg sudah link
		$kuliah_mahasiswa_set = $this->rdb->QueryToArray(
			"SELECT count(*) as jumlah FROM mahasiswa_status ms
			join mahasiswa m on m.id_mhs = ms.id_mhs
			join program_studi ps on ps.id_program_studi = m.id_program_studi
			where ps.fd_id_sms = '{$kode_prodi}'
			UNION ALL
			SELECT count(*) as jumlah FROM mahasiswa_status ms
			join mahasiswa m on m.id_mhs = ms.id_mhs
			join program_studi ps on ps.id_program_studi = m.id_program_studi
			where ps.fd_id_sms = '{$kode_prodi}' AND ms.FD_ID_REG_PD IS NOT NULL "
		);

		$jumlah['feeder'] = count($response['data']);
		$jumlah['langitan'] = $kuliah_mahasiswa_set[0]['JUMLAH'];
		$jumlah['linked'] = $kuliah_mahasiswa_set[1]['JUMLAH'];

		echo json_encode($jumlah);
	}

	private function proses_kuliah_mahasiswa()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync_link/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		$kode_prodi = $this->input->post('kode_prodi');


		if ($mode == MODE_AMBIL_DATA_LANGITAN) {

			$filter = "id_prodi='{$kode_prodi}'";

			// Ambil jumlah mahasiswa di feeder
			$kuliah_mahasiswa_set = $this->feederws->GetListPerkuliahanMahasiswa($filter);

			// simpan ke cache
			$this->session->set_userdata('kuliah_mahasiswa_set', $kuliah_mahasiswa_set['data']);

			$result['message'] = 'Ambil data langitan selesai. Jumlah data: ' . count($kuliah_mahasiswa_set['data']);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		} else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$kuliah_mahasiswa_set = $this->session->userdata('kuliah_mahasiswa_set');

			// Jika masih dalam rentang index, di proses
			if ($index_proses < count($kuliah_mahasiswa_set)) {
				// Ambil per row di feeder
				$data = $kuliah_mahasiswa_set[$index_proses];

				// Konversi format semester ke id_semester
				$semester_langitan = $this->rdb->QueryToArray(
					"SELECT id_semester FROM semester s
					JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = s.id_perguruan_tinggi
					WHERE 
						pt.npsn = '{$this->satuan_pendidikan['npsn']}' AND 
						s.fd_id_smt = '{$data['id_semester']}'"
				);
				$id_semester = $semester_langitan[0]['ID_SEMESTER'];

				$query =
					"SELECT * FROM MAHASISWA_STATUS ms
					JOIN mahasiswa m on m.id_mhs = ms.id_mhs 
					JOIN program_studi ps on ps.id_program_studi = m.id_program_studi
					WHERE ps.fd_id_sms = '{$data['id_prodi']}'
						AND m.FD_ID_REG_PD = '{$data['id_registrasi_mahasiswa']}'
						AND ms.id_semester = '{$id_semester}'";

				$kuliah_mahasiswa_langitan = $this->rdb->QueryToArray($query);

				$kuliah_mahasiswa_langitan_exist = isset($kuliah_mahasiswa_langitan[0]);

				$status_mhs = $this->rdb->QueryToArray(
					"SELECT * FROM STATUS_PENGGUNA sp 
							WHERE sp.ID_ROLE = 3 
							AND sp.FD_ID_STAT_MHS = '{$data['id_status_mahasiswa']}'"
				);
				// Jika ada
				if ($kuliah_mahasiswa_langitan_exist) {

					// Update
					$update_pengguna_result = $this->rdb->Query(
						"UPDATE MAHASISWA_STATUS 
						SET IPS = '{$data['ips']}',
							IPK = '{$data['ipk']}',
							SKS_TOTAL = '{$data['sks_total']}',
							SKS_SEMESTER = '{$data['sks_semester']}',
							ID_STATUS_PENGGUNA = '{$status_mhs[0]['ID_STATUS_PENGGUNA']}',
							FD_ID_REG_PD = '{$data['id_registrasi_mahasiswa']}', 
							FD_ID_SMT = '{$data['id_semester']}',
							FD_SYNC_ON = SYSDATE
						WHERE ID_MHS_STATUS = {$kuliah_mahasiswa_langitan[0]['ID_MHS_STATUS']}"
					);

					$result['message'] .= ($index_proses + 1) .
						" >>> UPDATE KULIAH MHS " . $data['nim'] .
						" Semester : " . $data['id_semester'] .
						($update_pengguna_result ? 'SUKSES' : 'GAGAL');
				} else {
					// insert 

					$mahasiswa = $this->rdb->QueryToArray(
						"SELECT * FROM MAHASISWA m
						WHERE m.FD_ID_REG_PD = '{$data['id_registrasi_mahasiswa']}'"
					);

					if (count($mahasiswa)>0) {

					$query = "INSERT INTO MAHASISWA_STATUS 
						(ID_MHS, IPS, IPK, SKS_TOTAL, SKS_SEMESTER, ID_SEMESTER, ID_STATUS_PENGGUNA, FD_ID_REG_PD, FD_ID_SMT, FD_SYNC_ON)
						VALUES (
							'{$mahasiswa[0]['ID_MHS']}',
							'{$data['ips']}',
							'{$data['ipk']}',
							'{$data['sks_total']}',
							'{$data['sks_semester']}',
							'{$id_semester}',
							'{$status_mhs[0]['ID_STATUS_PENGGUNA']}',
							'{$data['id_registrasi_mahasiswa']}',
							'{$data['id_semester']}',
							SYSDATE
						)";

					$insert_pengguna_result = $this->rdb->Query($query);

					$result['message'] .= ($index_proses + 1) .
						" >>> INSERT KULIAH MHS " . $mahasiswa[0]['ID_MHS'] .
						" Semester : " . $data['id_semester'] .
						($insert_pengguna_result ? ' SUKSES' : 'GAGAL');
					}else{
						$result['message'] .= ($index_proses + 1) .
						" >>> INSERT KULIAH MHS " . $data['nim'] .
						" Semester : " . $data['id_semester'] . ('GAGAL');
					}
				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			} else {
				$result['status'] = SYNC_STATUS_DONE;
				$result['message'] = 'Selesai';
			}
		}

		echo json_encode($result);
	}

	/**
	 * GET /sync_link/lulus/
	 */
	function lulus()
	{
		// redirect('home');

		$jumlah['feeder'] = '-';
		$jumlah['langitan'] = '-';
		$jumlah['linked'] = '-';

		$this->smarty->assign('jumlah', $jumlah);

		// Ambil program studi
		$sql_program_studi_raw = file_get_contents(APPPATH . 'models/sql/program-studi.sql');
		$sql_program_studi = strtr($sql_program_studi_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$program_studi_set = $this->rdb->QueryToArray($sql_program_studi);
		$this->smarty->assign('program_studi_set', $program_studi_set);

		// Ambil Semua angkatan yg ada :
		$sql_angkatan_mhs_raw = file_get_contents(APPPATH . 'models/sql/mahasiswa-angkatan.sql');
		$sql_angkatan_mhs = strtr($sql_angkatan_mhs_raw, array('@npsn' => $this->satuan_pendidikan['npsn']));
		$angkatan_set = $this->rdb->QueryToArray($sql_angkatan_mhs);
		$this->smarty->assign('angkatan_set', $angkatan_set);

		$this->smarty->assign('url_sync', site_url('sync_link/start/' . $this->uri->segment(2)));
		$this->smarty->display('sync_link/' . $this->uri->segment(2) . '.tpl');
	}

	function lulus_data($angkatan)
	{
		// redirect('home');

		$jumlah = array();
		$filter = "angkatan='{$angkatan}'";

		// Ambil jumlah mahasiswa di feeder
		$response = $this->feederws->GetListMahasiswaLulusDO($filter);

		// Ambil jumlah mahasiswa di Sistem Langitan & yg sudah link
		// $lulus_set = $this->rdb->QueryToArray(
		// 	"SELECT count(*) as jumlah FROM mata_kuliah mk
		// 	join program_studi ps on ps.id_program_studi = mk.id_program_studi
		// 	where ps.fd_id_sms = '{$kode_prodi}'
		// 	UNION ALL
		// 	SELECT count(*) as jumlah FROM mata_kuliah mk
		// 	join program_studi ps on ps.id_program_studi = mk.id_program_studi
		// 	where ps.fd_id_sms = '{$kode_prodi}' AND m.FD_ID_MK IS NOT NULL 
		// 	");

		$jumlah['feeder'] = count($response['data']);
		$jumlah['langitan'] = '-';
		$jumlah['linked'] = '-';

		echo json_encode($jumlah);
	}

	private function proses_lulus()
	{
		$result = array('status' => '', 'time' => '', 'message' => '', 'nextUrl' => site_url('sync_link/proses/' . $this->uri->segment(3)), 'params'	=> '');

		$mode	= isset($_POST['mode']) ? $_POST['mode'] : MODE_AMBIL_DATA_LANGITAN;

		$angkatan = $this->input->post('angkatan');

		if ($mode == MODE_AMBIL_DATA_LANGITAN) {

			$filter = "angkatan='{$angkatan}'";

			// Ambil jumlah mahasiswa di feeder
			$lulus_set = $this->feederws->GetListMahasiswaLulusDO($filter);

			// simpan ke cache
			$this->session->set_userdata('lulus_set', $lulus_set['data']);

			$result['message'] = 'Ambil data langitan selesai. Jumlah data: ' . count($lulus_set['data']);
			$result['status'] = SYNC_STATUS_PROSES;

			// ganti parameter
			$_POST['mode'] = MODE_SYNC;
			$result['params'] = http_build_query($_POST);
		} else if ($mode == MODE_SYNC) {
			$index_proses = isset($_POST['index_proses']) ? $_POST['index_proses'] : 0;

			// Ambil dari cache
			$lulus_set = $this->session->userdata('lulus_set');

			// Jika masih dalam rentang index, di proses
			if ($index_proses < count($lulus_set)) {
				// Ambil per row di feeder
				$data = $lulus_set[$index_proses];

				// Konversi format semester ke id_semester
				$semester_langitan = $this->rdb->QueryToArray(
					"SELECT id_semester FROM semester s
						JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = s.id_perguruan_tinggi
						WHERE 
							pt.npsn = '{$this->satuan_pendidikan['npsn']}' AND 
							s.fd_id_smt = '{$data['id_periode_keluar']}'"
				);
				$id_semester = $semester_langitan[0]['ID_SEMESTER'];

				$query =
					"SELECT * FROM ADMISI a
					JOIN MAHASISWA m ON m.ID_MHS = a.ID_MHS 
					JOIN program_studi ps on ps.id_program_studi = m.id_program_studi
				    JOIN status_pengguna sp ON sp.id_status_pengguna = a.status_akd_mhs
					WHERE  ps.fd_id_sms = '{$data['id_prodi']}'
						AND sp.status_keluar = 1
						AND a.ID_SEMESTER = '{$id_semester}'
						AND m.FD_ID_REG_PD = '{$data['id_registrasi_mahasiswa']}'";

				$lulus_langitan = $this->rdb->QueryToArray($query);

				$lulus_langitan_exist = isset($lulus_langitan[0]);

				$status_pengguna = $this->rdb->QueryToArray(
					"SELECT * FROM STATUS_PENGGUNA sp 
					WHERE sp.ID_ROLE = 3 
					AND sp.FD_ID_JNS_KELUAR = '{$data['id_jenis_keluar']}'"
				);
				$id_status_pengguna = $status_pengguna[0]['ID_STATUS_PENGGUNA'];

				// Jika ada
				if ($lulus_langitan_exist) {

					// Update
					$update_lulusan_result = $this->rdb->Query(
						"UPDATE ADMISI 
						SET FD_SYNC_ON = SYSDATE,
							status_akd_mhs = '{$id_status_pengguna}'
						WHERE ID_ADMISI = {$lulus_langitan[0]['ID_ADMISI']}"
					);

					$update_status_mhs_result = $this->rdb->Query(
						"UPDATE MAHASISWA 
						SET STATUS_AKADEMIK_MHS = '{$id_status_pengguna}'
						WHERE FD_ID_REG_PD = '{$data['id_registrasi_mahasiswa']}'"
					);

					$result['message'] .= ($index_proses + 1) .
						" >>> UPDATE ".$lulus_langitan[0]['NIM_MHS']." Lulusan / DO " . ($update_lulusan_result ? 'SUKSES' : 'GAGAL').
						" >>> UPDATE STATUS MAHASISWA " . ($update_status_mhs_result ? 'SUKSES' : 'GAGAL');
				} else {
					$mahasiswa = $this->rdb->QueryToArray(
						"SELECT * FROM MAHASISWA WHERE FD_ID_REG_PD = '{$data['id_registrasi_mahasiswa']}'"
					);

					if (count($mahasiswa[0])) {
						
					// insert 
					$query = "INSERT INTO ADMISI 
							(ID_MHS, 
							STATUS_AKD_MHS, 
							STATUS_APV, 
							ID_SEMESTER,
							TGL_KELUAR, 
							CREATED_BY, 
							CREATED_ON)
						VALUES (
							'{$mahasiswa[0]['ID_MHS']}',
							'{$id_status_pengguna}',
							1,
							$id_semester,
							TO_DATE('{$data['tanggal_keluar']}', 'DD-MM-YYYY'),
							60,
							SYSDATE
						)";
					$insert_result = $this->rdb->Query($query);

					$update_status_mhs_result = $this->rdb->Query(
						"UPDATE MAHASISWA 
						SET STATUS_AKADEMIK_MHS = '{$id_status_pengguna}'
						WHERE FD_ID_REG_PD = '{$data['id_registrasi_mahasiswa']}'"
					);

					$result['message'] .= ($index_proses + 1) .
						" >>> INSERT Lulusan / DO " . $mahasiswa[0]['NIM_MHS']
						. ($insert_result ? ' SUKSES' : 'GAGAL')
						. " >>> UPDATE STATUS MAHASISWA " . ($update_status_mhs_result ? 'SUKSES' : 'GAGAL');
					}
					else {
						$result['message'] .= ($index_proses + 1) . " >>> DATA ".$data['id_registrasi_mahasiswa']." TIDAK DITEMUKAN";
					}



				}

				$result['status'] = SYNC_STATUS_PROSES;

				// ganti parameter
				$_POST['index_proses'] = $index_proses + 1;
				$result['params'] = http_build_query($_POST);
			} else {
				$result['status'] = SYNC_STATUS_DONE;
				$result['message'] = 'Selesai';
			}
		}

		echo json_encode($result);
	}

	function start($mode)
	{
		if ($mode == 'dosen') {
			$this->smarty->assign('jenis_sinkronisasi', 'Import Dosen');
			$this->smarty->assign('url', site_url('sync_link/proses/' . $mode));
		}

		if ($mode == 'mahasiswa') {
			$this->smarty->assign('jenis_sinkronisasi', 'Import Mahasiswa');
			$this->smarty->assign('url', site_url('sync_link/proses/' . $mode));
		}

		if ($mode == 'mata_kuliah') {
			$this->smarty->assign('jenis_sinkronisasi', 'Import Mata Kuliah');
			$this->smarty->assign('url', site_url('sync_link/proses/' . $mode));
		}

		if ($mode == 'kurikulum') {
			$this->smarty->assign('jenis_sinkronisasi', 'Import Kurikulum');
			$this->smarty->assign('url', site_url('sync_link/proses/' . $mode));
		}

		if ($mode == 'kurikulum_mata_kuliah') {
			$this->smarty->assign('jenis_sinkronisasi', 'Import Kurikulum Mata Kuliah');
			$this->smarty->assign('url', site_url('sync_link/proses/' . $mode));
		}

		if ($mode == 'kelas_kuliah') {
			$this->smarty->assign('jenis_sinkronisasi', 'Import Kelas Kuliah');
			$this->smarty->assign('url', site_url('sync_link/proses/' . $mode));
		}

		if ($mode == 'nilai') {
			$this->smarty->assign('jenis_sinkronisasi', 'Import Nilai');
			$this->smarty->assign('url', site_url('sync_link/proses/' . $mode));
		}

		if ($mode == 'kuliah_mahasiswa') {
			$this->smarty->assign('jenis_sinkronisasi', 'Import Kuliah Mahasiswa');
			$this->smarty->assign('url', site_url('sync_link/proses/' . $mode));
		}

		if ($mode == 'program_studi') {
			$this->smarty->assign('jenis_sinkronisasi', 'Import Program Studi');
			$this->smarty->assign('url', site_url('sync_link/proses/' . $mode));
		}

		// Internal UMAHA
		if ($mode == 'lulus') {
			$this->smarty->assign('jenis_sinkronisasi', 'Update lulusan ke Sistem Langitan');
			$this->smarty->assign('url', site_url('sync_link/proses/' . $mode));
		}

		$this->smarty->display('sync/start.tpl');
	}

	function proses($mode)
	{
		// harus request POST 
		if ($_SERVER['REQUEST_METHOD'] != 'POST') {
			return;
		}

		if ($mode == 'dosen') {
			$this->proses_dosen();
		} else if ($mode == 'mahasiswa') {
			$this->proses_mahasiswa();
		} else if ($mode == 'program_studi') {
			$this->proses_program_studi();
		} else if ($mode == 'mata_kuliah') {
			$this->proses_mata_kuliah();
		} else if ($mode == 'kurikulum') {
			$this->proses_kurikulum();
		} else if ($mode == 'kurikulum_mata_kuliah') {
			$this->proses_kurikulum_mata_kuliah();
		} else if ($mode == 'kelas_kuliah') {
			$this->proses_kelas_kuliah();
		} else if ($mode == 'nilai') {
			$this->proses_nilai();
		} else if ($mode == 'kuliah_mahasiswa') {
			$this->proses_kuliah_mahasiswa();
		} else if ($mode == 'lulus') {
			$this->proses_lulus();
		} else {
			echo json_encode(array('status' => 'done', 'message' => 'Not Implemented()'));
		}
	}
}
