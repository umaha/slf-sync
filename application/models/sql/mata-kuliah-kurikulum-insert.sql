SELECT
    kmk.id_kurikulum_mk,
    k.fd_id_kurikulum_sp as id_kurikulum,
    mk.fd_id_mk as id_matkul,
    mk.nm_mata_kuliah,
    kmk.tingkat_semester as semester,
    nvl(kmk.kredit_semester, 0) as sks_mata_kuliah,
    nvl(kmk.kredit_praktikum, 0) as sks_praktek,
    nvl(kmk.kredit_prak_lapangan, 0) as sks_praktek_lapangan,
    nvl(kmk.kredit_simulasi, 0) as sks_simulasi,
    nvl(kmk.status_wajib, 0) as apakah_wajib
FROM kurikulum_mk kmk
    JOIN kurikulum k ON k.id_kurikulum = kmk.id_kurikulum
    JOIN mata_kuliah mk ON mk.id_mata_kuliah = kmk.id_mata_kuliah
    JOIN program_studi ps ON ps.id_program_studi = kmk.id_program_studi
    JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
    JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
WHERE
    pt.npsn = '@npsn'
    AND kmk.id_kurikulum = '@id_kurikulum'
    AND ps.kode_program_studi = '@kode_prodi'
    AND ps.status_aktif_prodi = 1
    AND (
        kmk.fd_id_kurikulum_sp IS NULL
        AND kmk.fd_id_mk IS NULL
    )