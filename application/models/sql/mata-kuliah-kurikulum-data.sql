/* Jumlah Semua Data */

SELECT COUNT(*) as jumlah
FROM kurikulum_mk kmk
    JOIN mata_kuliah mk ON mk.id_mata_kuliah = kmk.id_mata_kuliah
    JOIN program_studi ps ON ps.id_program_studi = kmk.id_program_studi
    JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
    JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
WHERE
    pt.npsn = '@npsn'
    AND kmk.id_kurikulum = '@id_kurikulum'
    AND ps.status_aktif_prodi = 1
UNION ALL
/* Jumlah sudah Link */
SELECT COUNT(*) as jumlah
FROM kurikulum_mk kmk
    JOIN mata_kuliah mk ON mk.id_mata_kuliah = kmk.id_mata_kuliah
    JOIN program_studi ps ON ps.id_program_studi = kmk.id_program_studi
    JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
    JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
WHERE
    pt.npsn = '@npsn'
    AND kmk.id_kurikulum = '@id_kurikulum'
    AND (
        kmk.fd_id_kurikulum_sp IS NOT NULL
        AND kmk.fd_id_mk IS NOT NULL
    )
    AND ps.status_aktif_prodi = 1
UNION ALL
/* Jumlah bakal update */
SELECT COUNT(*) as jumlah
FROM kurikulum_mk kmk
    JOIN mata_kuliah mk ON mk.id_mata_kuliah = kmk.id_mata_kuliah
    JOIN program_studi ps ON ps.id_program_studi = kmk.id_program_studi
    JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
    JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
WHERE
    pt.npsn = '@npsn'
    AND kmk.id_kurikulum = '@id_kurikulum'
    AND (
        kmk.fd_id_kurikulum_sp IS NOT NULL
        AND kmk.fd_id_mk IS NOT NULL
    )
    AND kmk.fd_sync_on < kmk.updated_on
    AND ps.status_aktif_prodi = 1
UNION ALL
/* Jumlah bakal Insert */
SELECT COUNT(*) as jumlah
FROM kurikulum_mk kmk
    JOIN mata_kuliah mk ON mk.id_mata_kuliah = kmk.id_mata_kuliah
    JOIN program_studi ps ON ps.id_program_studi = kmk.id_program_studi
    JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
    JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
WHERE
    pt.npsn = '@npsn'
    AND kmk.id_kurikulum = '@id_kurikulum'
    AND (
        kmk.fd_id_kurikulum_sp IS NULL
        AND kmk.fd_id_mk IS NULL
    )
    AND ps.status_aktif_prodi = 1