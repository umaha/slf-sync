/**
 * Author:  Fathoni <m.fathoni@mail.com>
 * Created: Apr 29, 2018
 */

/* jumlah semua */

select count(*) as jumlah
from
    pengambilan_mk_konversi pmk
    join mahasiswa m on m.id_mhs = pmk.id_mhs
    join pengguna p on p.id_pengguna = m.id_pengguna
    join perguruan_tinggi pt on pt.id_perguruan_tinggi = p.id_perguruan_tinggi
    left join mata_kuliah mk on mk.id_mata_kuliah = pmk.id_mata_kuliah
    join standar_nilai sn on sn.nm_standar_nilai = pmk.nilai_huruf_diakui
    join admisi a on a.id_mhs = m.id_mhs
    and a.id_jalur is not null
    join semester s on a.id_semester = s.id_semester
    left join perguruan_tinggi pt_asal on pt_asal.npsn = a.kode_pt_asal
where
    pt.npsn = '@npsn'
    and a.id_semester = '@smt'
    and m.fd_id_reg_pd is not null
union all
/* jumlah link */
select count(*)
from
    pengambilan_mk_konversi pmk
    join mahasiswa m on m.id_mhs = pmk.id_mhs
    join pengguna p on p.id_pengguna = m.id_pengguna
    join perguruan_tinggi pt on pt.id_perguruan_tinggi = p.id_perguruan_tinggi
    left join mata_kuliah mk on mk.id_mata_kuliah = pmk.id_mata_kuliah
    join standar_nilai sn on sn.nm_standar_nilai = pmk.nilai_huruf_diakui
    join admisi a on a.id_mhs = m.id_mhs
    and a.id_jalur is not null
    join semester s on a.id_semester = s.id_semester
    left join perguruan_tinggi pt_asal on pt_asal.npsn = a.kode_pt_asal
where
    pt.npsn = '@npsn'
    and a.id_semester = '@smt'
    and m.fd_id_reg_pd is not null
    and pmk.fd_id_ekuivalensi is not null
union all
/* jumlah update */
select count(*)
from
    pengambilan_mk_konversi pmk
    join mahasiswa m on m.id_mhs = pmk.id_mhs
    join pengguna p on p.id_pengguna = m.id_pengguna
    join perguruan_tinggi pt on pt.id_perguruan_tinggi = p.id_perguruan_tinggi
    left join mata_kuliah mk on mk.id_mata_kuliah = pmk.id_mata_kuliah
    join standar_nilai sn on sn.nm_standar_nilai = pmk.nilai_huruf_diakui
    join admisi a on a.id_mhs = m.id_mhs
    and a.id_jalur is not null
    join semester s on a.id_semester = s.id_semester
    left join perguruan_tinggi pt_asal on pt_asal.npsn = a.kode_pt_asal
where
    pt.npsn = '@npsn'
    and a.id_semester = '@smt'
    and m.fd_id_reg_pd is not null
    and pmk.fd_id_ekuivalensi is not null
    and pmk.updated_on > pmk.fd_sync_on
union all
/* jumlah bakal insert */
select count(*)
from
    pengambilan_mk_konversi pmk
    join mahasiswa m on m.id_mhs = pmk.id_mhs
    join pengguna p on p.id_pengguna = m.id_pengguna
    join perguruan_tinggi pt on pt.id_perguruan_tinggi = p.id_perguruan_tinggi
    left join mata_kuliah mk on mk.id_mata_kuliah = pmk.id_mata_kuliah
    join standar_nilai sn on sn.nm_standar_nilai = pmk.nilai_huruf_diakui
    join admisi a on a.id_mhs = m.id_mhs
    and a.id_jalur is not null
    join semester s on a.id_semester = s.id_semester
    left join perguruan_tinggi pt_asal on pt_asal.npsn = a.kode_pt_asal
where
    pt.npsn = '@npsn'
    and a.id_semester = '@smt'
    and m.fd_id_reg_pd is not null
    and pmk.fd_id_ekuivalensi is NULL
union all
/* jumlah bakal delete */
select count(*)
from
    pengambilan_mk_konv_del pmkd
    join mahasiswa m on m.id_mhs = pmkd.id_mhs
    join pengguna p on p.id_pengguna = m.id_pengguna
    join perguruan_tinggi pt on pt.id_perguruan_tinggi = p.id_perguruan_tinggi
    left join mata_kuliah mk on mk.id_mata_kuliah = pmkd.id_mata_kuliah
    join standar_nilai sn on sn.nm_standar_nilai = pmkd.nilai_huruf_diakui
    join admisi a on a.id_mhs = m.id_mhs
    and a.id_jalur is not null
    join semester s on a.id_semester = s.id_semester
    left join perguruan_tinggi pt_asal on pt_asal.npsn = a.kode_pt_asal
where
    pt.npsn = '@npsn'
    and a.id_semester = '@smt'
    and pmkd.fd_id_ekuivalensi is not null
    and pmkd.fd_sync_on is not null