SELECT
    ms.id_mhs_status,
    m.nim_mhs,
    '@id_smt' as id_semester,
    m.fd_id_reg_pd as id_registrasi_mahasiswa,
    sp.fd_id_stat_mhs as id_status_mahasiswa,
    ips,
    sks_semester,
    ipk,
    sks_total as total_sks,
    case
        when substr(s.fd_id_smt, -1) = '3' then 240000
        /* Semester Pendek Fix Rp 240.0000 */
        else CASE sp.fd_id_stat_mhs
            WHEN 'C' THEN 0
            ELSE coalesce(
                tm.total_besar_biaya,
                ms.biaya_semester
            )
        END
    END as biaya_kuliah_smt,
    sb.fd_id_pembiayaan as id_pembiayaan
    /* Ambil dari biaya_semester, jika tidak ada tagihan */
FROM
    mahasiswa_status ms
    JOIN mahasiswa m ON m.id_mhs = ms.id_mhs
    JOIN sumber_biaya sb ON m.sumber_biaya = sb.id_sumber_biaya
    JOIN program_studi ps ON m.id_program_studi = ps.id_program_studi
    LEFT JOIN status_pengguna sp ON sp.id_status_pengguna = ms.id_status_pengguna
    JOIN semester s ON s.id_semester = ms.id_semester
    JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = s.id_perguruan_tinggi
    LEFT JOIN tagihan_mhs tm ON tm.id_mhs = m.id_mhs
    AND tm.id_semester = s.id_semester
WHERE
    pt.npsn = '@npsn'
    AND s.thn_akademik_semester || decode (
        nm_semester,
        'Ganjil',
        1,
        'Genap',
        2,
        'Pendek',
        3
    ) = '@id_smt'
    AND ps.kode_program_studi = '@kode_prodi'
    AND sp.fd_id_stat_mhs IS NOT NULL
    AND
    /* Pembatasan Status Aktivitas Kuliah */
    ms.fd_id_smt IS NOT NULL
    AND ps.status_aktif_prodi = 1
    AND ms.fd_id_reg_pd IS NOT NULL
    AND ms.fd_sync_on < ms.updated_on