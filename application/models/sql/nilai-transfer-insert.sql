/**
 * Author:  Fathoni <m.fathoni@mail.com>
 * Created: Mar 4, 2019
 */

select
    m.nim_mhs || ' ' || p.nm_pengguna as mhs,
    pmk.id_pengambilan_mk_konversi,
    m.fd_id_reg_pd as id_registrasi_mahasiswa,
    mk.fd_id_mk as id_matkul,
    pmk.kode_mk_asal as kode_mata_kuliah_asal,
    pmk.nama_mk_asal as nama_mata_kuliah_asal,
    pmk.sks_asal as sks_mata_kuliah_asal,
    pmk.sks_diakui as sks_mata_kuliah_diakui,
    pmk.nilai_huruf_asal,
    pmk.nilai_huruf_diakui,
    pmk.is_migrasi_hukum,
    nvl(sn.nilai_standar_nilai, 0) as nilai_angka_diakui,
    s.fd_id_smt as id_semester,
    pt_asal.fd_id_sp as id_sp
from
    pengambilan_mk_konversi pmk
    join mahasiswa m on m.id_mhs = pmk.id_mhs
    join pengguna p on p.id_pengguna = m.id_pengguna
    join perguruan_tinggi pt on pt.id_perguruan_tinggi = p.id_perguruan_tinggi
    left join mata_kuliah mk on mk.id_mata_kuliah = pmk.id_mata_kuliah
    join standar_nilai sn on sn.nm_standar_nilai = pmk.nilai_huruf_diakui
    join admisi a on a.id_mhs = m.id_mhs
    and a.id_jalur is not null
    join semester s on a.id_semester = s.id_semester
    left join perguruan_tinggi pt_asal on pt_asal.npsn = a.kode_pt_asal
where
    pt.npsn = '@npsn'
    and m.fd_id_reg_pd is not null
    and a.id_semester = '@smt'
    and pmk.fd_id_ekuivalensi is NULL