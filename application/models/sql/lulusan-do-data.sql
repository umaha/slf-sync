/**
 * Author:  Fathoni <m.fathoni@mail.com>
 * Created: Apr 5, 2018
 */

/* Jumlah Semua Data */

SELECT COUNT(*) AS jumlah
FROM admisi a
    JOIN mahasiswa m ON m.id_mhs = a.id_mhs
    JOIN pengguna p ON p.id_pengguna = m.id_pengguna
    JOIN status_pengguna sp ON sp.id_status_pengguna = a.status_akd_mhs
    JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi
    JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
    JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
WHERE
    P.id_perguruan_tinggi = 1
    AND sp.status_keluar = 1
    AND ps.kode_program_studi = '@kode_prodi'
    AND ps.status_aktif_prodi = 1
UNION ALL
/* Jumlah Sudah Link */
SELECT COUNT(*) AS jumlah
FROM admisi A
    JOIN mahasiswa M ON M.id_mhs = A.id_mhs
    JOIN pengguna P ON P.id_pengguna = M.id_pengguna
    JOIN status_pengguna sp ON sp.id_status_pengguna = A.status_akd_mhs
    JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi
    JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
    JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
WHERE
    P.id_perguruan_tinggi = 1
    AND sp.status_keluar = 1
    AND a.fd_sync_on IS NOT NULL
    AND ps.kode_program_studi = '@kode_prodi'
    AND ps.status_aktif_prodi = 1
UNION ALL
/* Jumlah Bakal Update */
SELECT COUNT(*) AS jumlah
FROM admisi A
    JOIN mahasiswa M ON M.id_mhs = A.id_mhs
    JOIN pengguna P ON P.id_pengguna = M.id_pengguna
    JOIN status_pengguna sp ON sp.id_status_pengguna = A.status_akd_mhs
    JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi
    JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
    JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
WHERE
    P.id_perguruan_tinggi = 1
    AND sp.status_keluar = 1
    AND ps.kode_program_studi = '@kode_prodi'
    AND ps.status_aktif_prodi = 1
    AND a.fd_sync_on IS NOT NULL
    AND (a.updated_on > a.fd_sync_on)
UNION ALL
/* Jumlah Bakal Insert */
SELECT COUNT(*) AS jumlah
FROM admisi A
    JOIN mahasiswa M ON M.id_mhs = A.id_mhs
    JOIN pengguna P ON P.id_pengguna = M.id_pengguna
    JOIN status_pengguna sp ON sp.id_status_pengguna = A.status_akd_mhs
    JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi
    JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
    JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
WHERE
    P.id_perguruan_tinggi = 1
    AND sp.status_keluar = 1
    AND ps.kode_program_studi = '@kode_prodi'
    AND ps.status_aktif_prodi = 1
    AND a.fd_sync_on IS NULL