/**
 * Author:  Fathoni
 * Created: Nov 1, 2016
 */

SELECT
    m.id_mhs,
    p.id_pengguna,
    p.fd_id_pd_lama,
    /* Informasi Mahasiswa */
    P.nm_pengguna AS nama_mahasiswa,
    decode(
        P.kelamin_pengguna,
        1,
        'L',
        2,
        'P'
    ) AS jenis_kelamin,
    COALESCE(m.nisn, cms.nisn) as nisn,
    NVL(
        m.nik_mhs,
        CASE
            WHEN LENGTH(cmb.nik_c_mhs) <= 16 THEN cmb.nik_c_mhs
        END
    ) AS nik,
    NVL( (
            SELECT nm_kota
            FROM kota
            WHERE
                kota.id_kota = m.LAHIR_KOTA_MHS
        ),
        'Belum Terekam'
    ) as tempat_lahir,
    NVL(
        to_char(
            tgl_lahir_pengguna,
            'YYYY-MM-DD'
        ),
        '1900-01-01'
    ) as tanggal_lahir,
    NVL( (
            select id_feeder
            from agama
            where
                agama.id_agama = p.id_agama
        ),
        1
    ) as id_agama,
    /* default Islam */
    0 AS id_kk,
    pt.fd_id_sp as id_sp,
    /* Info tempat tinggal */
    SUBSTR(
        COALESCE(alamat_asal_mhs, alamat_mhs),
        1,
        80
    ) as jalan,
    COALESCE(
        m.alamat_rt_mhs,
        m.alamat_rt_asal_mhs,
        cmb.alamat_rt
    ) AS rt,
    COALESCE(
        m.alamat_rw_mhs,
        m.alamat_rw_asal_mhs,
        cmb.alamat_rw
    ) AS rw,
    SUBSTR(
        COALESCE(
            m.alamat_dusun_mhs,
            m.alamat_dusun_asal_mhs,
            cmb.alamat_dusun
        ),
        1,
        60
    ) AS dusun,
    NVL(
        SUBSTR(
            COALESCE(
                m.alamat_asal_mhs,
                m.alamat_mhs
            ),
            1,
            60
        ),
        'Belum Terekam'
    ) as kelurahan,
    '000000' as id_wilayah,
    COALESCE(
        m.alamat_kodepos,
        m.alamat_kodepos_asal
    ) AS kode_pos,
    NULL AS id_jns_tinggal,
    NULL AS id_alat_transport,
    NULL AS telepon,
    substr(m.mobile_mhs, 1, 20) AS handphone,
    COALESCE(
        P.email_alternate,
        cmb.email,
        p.email_pengguna
    ) AS email,
    /* Other Info */
    0 as penerima_kps,
    m.nomor_kps as no_kps,
    'A' as stat_pd,
    /* Informasi Ayah */
    m.nm_ayah_mhs as nama_ayah,
    to_char(
        m.tgl_lahir_ayah_mhs,
        'YYYY-MM-DD'
    ) as tanggal_lahir_ayah,
    m.nik_ayah_mhs as nik_ayah,
    null as id_jenjang_pendidikan_ayah,
    null as id_pekerjaan_ayah,
    null as id_penghasilan_ayah,
    NVL(null, 0) as id_kebutuhan_khusus_ayah,
    /* Informasi Ibu */
    NVL(nm_ibu_mhs, 'Belum Terekam') as nama_ibu_kandung,
    to_char(
        m.tgl_lahir_ibu_mhs,
        'YYYY-MM-DD'
    ) as tanggal_lahir_ibu,
    m.nik_ibu_mhs as nik_ibu,
    null as id_jenjang_pendidikan_ibu,
    null as id_pekerjaan_ibu,
    null as id_penghasilan_ibu,
    NVL(null, 0) as id_kebutuhan_khusus_ibu,
    /* Informasi Wali */
    m.nm_wali_mhs as nm_wali,
    to_char(
        m.tgl_lahir_wali_mhs,
        'YYYY-MM-DD'
    ) as tanggal_lahir_wali,
    null as id_jenjang_pendidikan_wali,
    null as id_pekerjaan_wali,
    null as id_penghasilan_wali,
    'ID' AS kewarganegaraan
FROM mahasiswa m
    JOIN pengguna p ON p.id_pengguna = m.id_pengguna
    JOIN program_studi ps ON ps.id_program_studi = m.id_program_studi
    JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = p.id_perguruan_tinggi
    LEFT JOIN calon_mahasiswa_baru cmb ON cmb.id_c_mhs = M.id_c_mhs
    LEFT JOIN calon_mahasiswa_sekolah cms ON cms.id_c_mhs = cmb.id_c_mhs
WHERE
    pt.npsn = '@npsn'
    AND ps.kode_program_studi = '@kode_prodi'
    AND ps.status_aktif_prodi = 1
    AND m.thn_angkatan_mhs = '@angkatan'
    AND p.fd_id_pd IS NULL
ORDER BY m.nim_mhs ASC