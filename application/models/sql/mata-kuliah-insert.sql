/**
 * Author:  Fathoni
 * Created: Feb 13, 2017
 */

SELECT
    mk.id_mata_kuliah,
    ps.fd_id_sms AS id_prodi,
    j.id_jenjang_pendidikan_feeder AS id_jenj_didik,
    kd_mata_kuliah AS kode_mata_kuliah,
    nm_mata_kuliah AS nama_mata_kuliah,
    id_jenis_mk AS id_jenis_mata_kuliah,
    id_kelompok_mk AS id_kelompok_mata_kuliah,
    kredit_semester AS sks_mata_kuliah,
    kredit_tatap_muka AS sks_tatap_muka,
    kredit_praktikum AS sks_praktek,
    kredit_prak_lapangan AS sks_praktek_lapangan,
    kredit_simulasi AS sks_simulasi,
    mk.metode_pelaksanaan AS metode_kuliah,
    mk.ada_sap AS ada_sap,
    mk.ada_silabus AS ada_silabus,
    mk.ada_bahan_ajar AS ada_bahan_ajar,
    mk.status_praktikum AS ada_acara_praktek,
    mk.ada_diktat AS ada_diktat,
    to_char(
        mk.tgl_mulai_efektif,
        'YYYY-MM-DD'
    ) AS tanggal_mulai_efektif,
    to_char(
        mk.tgl_akhir_efektif,
        'YYYY-MM-DD'
    ) as tanggal_akhir_efektif
FROM mata_kuliah mk
    JOIN program_studi ps ON ps.id_program_studi = mk.id_program_studi
    JOIN jenjang j ON j.id_jenjang = ps.id_jenjang
    JOIN fakultas f ON f.id_fakultas = ps.id_fakultas
    JOIN perguruan_tinggi pt ON pt.id_perguruan_tinggi = f.id_perguruan_tinggi
WHERE
    pt.npsn = '@npsn'
    AND ps.kode_program_studi = '@kode_prodi'
    AND ps.status_aktif_prodi = 1
    AND mk.fd_id_mk IS NULL