{extends file='home_layout.tpl'}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Sinkronisasi Link Lulusan / DO</h2>
			</div>
			
			<table class="table table-bordered table-condensed" style="width: auto">
				<thead>
					<tr>
						<th>Lulusan / DO</th>
						<th>Feeder</th>
						<th>Sistem Langitan</th>
						<th>Link</th>
					</tr>
				</thead>
				<tbody>
					<tr class="jumlahdata">
						<td>Jumlah Data</td>
						<td class="text-center">{$jumlah.feeder}</td>
						<td class="text-center">{$jumlah.langitan}</td>
						<td class="text-center">{$jumlah.linked}</td>
					</tr>
				</tbody>
			</table>

		<form class="form-horizontal" action="{$url_sync}" method="post">
			<fieldset>

				<!-- Form Name -->
				<legend>Filter Data</legend>

                <!-- Select Basic -->
				<div class="form-group">
                <label class="col-md-2 control-label" for="selectbasic">Angkatan</label>
                <div class="col-md-4">
                    <select name="angkatan" class="form-control" required>
                        <option value=""></option>
                        {foreach $angkatan_set as $a}
                            <option value="{$a.THN_ANGKATAN_MHS}">{$a.THN_ANGKATAN_MHS}</option>
                        {/foreach}
                    </select>
                </div>
            </div>

				<!-- Button -->
				<div class="form-group">
					<label class="col-md-4 control-label" for="singlebutton"></label>
					<div class="col-md-4">
						<input type="submit" class="btn btn-primary" value="Proses Sinkronisasi" />
					</div>
				</div>
				
			</fieldset>
		</form>
		</div>
	</div>
{/block}
{block name='footer-script'}
	<script type="text/javascript">
		$(function(){
			
			$('input[type=submit]').prop('disabled', true);
			
			var refreshTable = function(angkatan) {
				var url = '{site_url('sync_link/lulus_data')}/'+angkatan;
				
				if (angkatan)
				{
					$.ajax({
						dataType: "json",
						url: url,
						beforeSend: function (xhr) {
							$('input[type=submit]').prop('disabled', true);
							$('body').css('cursor', 'progress');
						},
						success: function (data) {
                            $('tr.jumlahdata td:eq(1)').text(data.feeder);
                            $('tr.jumlahdata td:eq(2)').text(data.langitan);
                            $('tr.jumlahdata td:eq(3)').text(data.linked);
							$("input[type=submit]").prop('disabled', false);
							$('body').css('cursor', 'default');
						}
					});
				}
			};
            $('select[name=angkatan]').on('change', function(){
				refreshTable($('select[name=angkatan]').val());
			});
		});
	</script>
{/block}