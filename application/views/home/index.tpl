{extends file='home_layout.tpl'}
{block name='head'}
	<meta http-equiv="refresh" content="30">
{/block}
{block name='body-content'}
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h2>Selamat Datang di Sistem Langitan-Feeder Sync</h2>
			</div>

			<table class="table table-bordered table-condensed" style="width: auto">
				<thead>
					<tr>
						<th>Keterangan</th>
						<th>Nilai</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Web Service URL</td>
						<td>{$ci->session->userdata('ws2url')}</td>
					</tr>
					<tr>
						<td>Token</td>
						<td><textarea cols="100" rows="5">{$ci->session->userdata('token')}</textarea></td>
					</tr>
					<tr>
						<td colspan=2></td>
					</tr>
					<tr>
						<td>ID</td>
						<td>{$satuan_pendidikan.id_perguruan_tinggi}</td>
					</tr>
					<tr>
						<td>Kode PT</td>
						<td>{$satuan_pendidikan.kode_perguruan_tinggi}</td>
					</tr>
					<tr>
						<td>Nama PT</td>
						<td>{$satuan_pendidikan.nama_perguruan_tinggi}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
{/block}